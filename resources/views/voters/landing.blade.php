@extends('layout')

@section('title', 'Voting Page')

@section('content')
    @if(Auth::user()->admin)
        <a class="btn" href="{{route('admin.home')}}">Go to Admin Panel</a>

    @endif
    <form action="{{url('vote')}}" method="POST" class="form-horizontal" onsubmit="return confirm('Are you sure you want to submit?')">
        {{csrf_field()}}
        <section class="panel">
            <header class="panel-heading">
                Welcome {{Auth::user()->name}}
                <h2 class="panel-title"> Select your preferred candidate below</h2>

            </header>
            <div class="panel-body">

                @if(!Auth::user()->voted)
                    @foreach($posts as $post)

                        @if(count($post->candidates) > 0)
                            @if($post->type == 'normal')

                                <div class="post" id="post{{$post->id}}" data-index="{{$post->id}}">
                                    <div class="panel">
                                        <div class="panel-heading text-center h4">{{ucfirst($post->title)}}</div>
                                    </div>
                                    <div style="margin: 0 20%;" class="form-group">
                                        {{--                            <span class="col-sm-3 control-label"> {{ucfirst($post->title)}}: </span>--}}
                                        <div class="">
                                            @foreach($post->candidates as $candidate )
                                                <div class="radio">
                                                    <img src="{{asset('storage/'.$candidate->image_link)}}" width="120"
                                                         height="120">
                                                    <label>
                                                        <input type="radio" name="can[{{ $post->id }}][]"
                                                               id="post_{{$post->id}}" value="{{$candidate->id}}">
                                                        <span style="font-size:24px;"><strong>{{$candidate->name}}</strong><br><br>{{$candidate->matric_no }}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                            <div class="radio">
                                                <img src="" height="120" width="120">
                                                <label>
                                                    <input type="radio" name="can[{{$post->id}}][]">
                                                    <span style="font-size:24px;"><strong>No Option</strong></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @else

                                <div class="post" id="post{{$post->id}}" data-index="{{$post->id}}">
                                    <div class="panel">
                                        <div class="panel-heading text-center h4">{{ucfirst($post->title)}}</div>
                                    </div>
                                    <div style="margin: 0 20%;" class="form-group">
                                        {{--                            <span class="col-sm-3 control-label"> {{ucfirst($post->title)}}: </span>--}}
                                        <div class="">
                                            @foreach($post->candidates as $candidate )
{{--                                                {{Auth::user()->department == $candidate->depart}}--}}
                                            @if(Auth::user()->department == $candidate->department)
                                                    <div class="checkbox">
                                                    <img src="{{asset('storage/'.$candidate->image_link)}}" width="120"
                                                         height="120">
                                                    <label>
                                                        <input type="checkbox" name="can[{{ $post->id }}][]"
                                                               id="post_{{$post->id}}" value="{{$candidate->id}}">
                                                        <span style="font-size:24px;"><strong>{{$candidate->name}}</strong></span>
                                                    </label>
                                                </div>
                                                @endif
                                            @endforeach
                                                {{--<div class="checkbox">--}}
                                                    {{--<img src="" height="120" width="120">--}}
                                                    {{--<label>--}}
                                                        {{--<input type="checkbox" name="can[{{$post->id}}][]">--}}
                                                        {{--<span style="font-size:24px;"><strong>Skip</strong></span>--}}
                                                    {{--</label>--}}
                                                {{--</div>--}}
                                        </div>
                                    </div>
                                </div>

                            @endif

                        @endif

                    @endforeach

                    <div class="text-center">
                        <button type="button" id="prv" onclick="previousPost()" class="btn btn-primary">Previous
                        </button>
                        <button type="button" id="nxt" onclick="nextPost()" class="btn btn-primary">Next</button>
                    </div>

                @else

                    You have completed the voting exercise. Please proceed to <a href="{{route('logout')}}">logout</a>

                @endif
            </div>

            <footer class="panel-footer clearfix">
                @if(!Auth::user()->voted)
                    <button onclick="submitting()" id="submitBtn" type="submit" class="btn btn-success btn-lg pull-right">Submit</button>
                @endif

            </footer>
        </section>
    </form>

@endsection

@section('scripts')
    <script>
        var counter = 0;
        $(document).ready(
            function () {
                $('.post').addClass('hidden');
                var a = $('.post')[0]
                $(a).removeClass('hidden')
                $('#prv').hide()

                $('input[type=checkbox]').on('change', function (e) {
                    if ($('input[type=checkbox]:checked').length > 3) {
                        $(this).prop('checked', false);
                        alert("YOU CAN ONLY CHOOSE 3 STUDENT REPRESENTATIVES");
                    }
                });
            }
        )

        function previousPost() {
            console.log(counter)
            if (counter < 1) return;
            $('.post').addClass('hidden')
            counter--;
            var a = $('.post')[counter]
            $(a).removeClass('hidden')

            if (counter === 0) {
                $('#prv').hide()
            } else {
                $('#prv').show()
            }
            if (counter + 1 === $('.post').length) {
                $('#nxt').hide()
            } else {
                $('#nxt').show()
            }
        }

        function nextPost() {
            console.log(counter)
            var a = $('.post')
            if (counter + 1 >= a.length) {
                return;
            }
            a.addClass('hidden')
            counter++;
            var b = a[counter]
            $(b).removeClass('hidden')

            if (counter === 0) {
                $('#prv').hide()
            } else {
                $('#prv').show()
            }
            if (counter + 1 === a.length) {
                $('#nxt').hide()
            } else {
                $('#nxt').show()
            }
        }

        function submitting(){
            var btn = document.getElementById('submitBtn');
            btn.setAttribute('disabled', true);
            return true;
        }
    </script>
@endsection
