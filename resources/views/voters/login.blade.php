@extends('layout')

@section('title','Voters Login')

@section('content')

    <!-- start: page -->

    <section class="body-sign">
        <div class="center-sign">
            <a href="/" class="logo pull-left">
                <h2>Voter</h2>
            </a>

            <div class="panel panel-sign">
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Log In</h2>
                </div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <p class="alert alert-success alert-dismissible"> {{Session::get('message')}}</p>
                    @endif

                    @if(count($errors)>0)
                        <ul>
                            @foreach($errors->all() as $error)
                                <li class="alert alert-warning alert-dismissible "><span class="fa fa-exclamation- circle"> </span> {{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                    <form action="" method="post">

                        {{csrf_field()}}
                        <div class="form-group mb-lg">
                            <label>Matric Number</label>
                            <div class="input-group input-group-icon">
                                <input name="matric" type="text" class="form-control input-lg" />
                                <span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
                            </div>
                        </div>

                        <div class="form-group mb-lg">
                            <div class="clearfix">
                                <label class="pull-left">PIN</label>

                            </div>
                            <div class="input-group input-group-icon">
                                <input name="pwd" type="text" class="form-control input-lg" />
                                <span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
{{--                                <div class="checkbox-custom checkbox-default">--}}
{{--                                    <input id="RememberMe" name="rememberme" type="checkbox"/>--}}
{{--                                    <label for="RememberMe">Remember Me</label>--}}
{{--                                </div>--}}
                            </div>
                            <div class="col-sm-4 text-right">
                                <button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
                                <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
            <br><br><br><br><br><br><br><br>
        </div>
    </section>
    <!-- end: page -->

@endsection
