@extends('layout')

@section('title','Admin Landing')

@section('styles')
    <link rel="stylesheet" href="assets/vendor/select2/css/select2.css" />
    <link rel="stylesheet" href="assets/vendor/select2-bootstrap-theme/select2-bootstrap.css" />
    <link rel="stylesheet" href="assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
@endsection

@section('content')

    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
            </div>

            <h2 class="panel-title">Candidate List</h2>
        </header>
        <div class="panel-body">
            <h3>Vote Ratio: {{$voted}}/{{$voters}}</h3>
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                <tr>
                    <th>Candidate's name</th>
                    <th>Actual Votes</th>
                    {{--<th>Votes</th>--}}
                    <th>Post </th>
                    <th>Department</th>
                    <th>Image</th>
                    <th class="hidden-phone">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($candidates as $candidate)

                    <tr class="gradeX">
                        <td>{{$candidate->name}}</td>
                        <td>{{$candidate->actual_votes->count()}}</td>
                        {{--<td>{{$candidate->votes}}</td>--}}
                        <td>{{$candidate->post->title}}</td>
                        <td>{{$candidate->department}}</td>
                        <td><img width="120" height="120" src="{{asset('storage/'.$candidate->image_link)}}"></td>
                        {{--                                <td><img src="{{storage_path($candidate->image_link)}}"></td>--}}
                        <td class="center">
                            <a href="{{url('/admonish/result/'.$candidate->id)}}" class="btn btn-success">View</a>
                        </td>
                        {{--<td class="center">--}}
                            {{--<form action="{{url('/admin/delete/'.$candidate->id)}}" method="POST">--}}
                                {{--{{method_field('DELETE')}} <button class="btn btn-danger"> Delete </button>--}}
                                {{--{{csrf_field()}}--}}
                            {{--</form>--}}
                        {{--</td>--}}
                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>
    </section>

@endsection
