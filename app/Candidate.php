<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $guarded = ['id'];


    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function actual_votes(){
        return $this->hasMany(Vote::class);
    }
}
