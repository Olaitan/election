<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Post;
use App\User;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function home(){
        $posts =  Post::with('candidates')->orderBy('weight', 'desc')->get();
        return view('voters.landing')->withPosts($posts);
    }

    public function vote(Request $request){
//        dd($request->all());
        foreach( $request->can as $post){
            foreach($post as $p){
                    if(is_numeric($p)){
                    $candidate = Candidate::find($p);
                    $candidate->votes +=1;
                    $candidate->save();

                    $vote = Vote::query()->where('user_id', Auth::id())->where('candidate_id', $candidate->id)->first();
                    if($vote){
                        continue;
                    }
                    $v = new Vote();
                    $v->candidate_id = $candidate->id;
                    $v->user_id = Auth::id();
                    $v->save();
                }
            }
        }
        $user = User::find(Auth::id());
        $user->voted = true;
        $user->save();
        return redirect('/');
    }
}
