<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function home()
    {
        if(!Auth::user()->admin){
            return redirect('/');
        }
        $d = User::query()->select('department')->groupBy('department')->get();
//        dd($d[0]->department);
        $candidates = Candidate::with('post')->get();

        return view('admin.index')->withPosts(Post::all())->withCandidates($candidates)->withDepartments($d);
    }

    public function result(){

        if(!Auth::user()->admin){
            return redirect('/');
        }
//        $d = User::query()->select('department')->groupBy('department')->get();
//        dd($d[0]->department);
        $u = User::query()->where('admin', 0)->count();
        $vu = User::query()->where('admin', 0)->where('voted', 1)->count();
        $candidates = Candidate::with('post')->orderBy('post_id')->get();

        return view('admin.result')->withCandidates($candidates)->withVoters($u)->withVoted($vu);

    }

    public function resultSingle($id){

        if(!Auth::user()->admin){
            return redirect('/');
        }
//        $d = User::query()->select('department')->groupBy('department')->get();
//        dd($d[0]->department);
        $u = User::query()->where('admin', 0)->count();
        $vu = User::query()->where('admin', 0)->where('voted', 1)->count();
        $candidate = Candidate::query()->find($id);

//        $candidates = Candidate::with('post')->orderBy('post_id')->get();

        return view('admin.result_show', [
            'candidate' => $candidate
        ]);

    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required', 'type' => 'required|in:normal,src']);
        if ($validator->passes()) {
            $post = new Post();
            $post->title = $request->name;
            $post->type = $request->type;
            $post->save();

            Session::flash('message', 'Post saved successfully');

            return back();
        } else {
            return back()->withErrors($validator->errors()->all());
        }
    }

    public function addCandidate(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), ['name' => 'required', 'matric_no' => 'required|unique:candidates', 'post' => 'required', 'image' => 'required|image|mimes:jpeg,jpg,gif,png', 'department' => 'required']);
        if ($validator->passes()) {
            $candidate = new Candidate();
            $candidate->name = $request->name;
            $candidate->matric_no = $request->matric_no;
            $candidate->nick_name = $request->nick_name;
            $candidate->post_id = $request->post;
            $candidate->department = $request->department;

            $image = $request->file('image');
            $link = $image->storeAs('images', str_random('10') . '.' . $image->getClientOriginalExtension());
            $candidate->image_link = $link;

            $candidate->save();

            Session::flash('message', 'Candidate saved succesfully');

            return back();
        } else {
            return back()->withErrors($validator->errors()->all());
        }
    }

    public function candidateDelete($id)
    {
        Candidate::destroy($id);
        Session::flash('message', 'candidate delete successful');
        return back();
    }
}
