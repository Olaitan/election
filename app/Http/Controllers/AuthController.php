<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    public function login(Request $request){
        $rules = [
            'matric'=>'required',
            'pwd'=>'required'
        ];
        $this->validate($request, $rules);
        $user = User::query()->where('matric_no', $request->matric)->where('generated_password', $request->pwd)->first();

        if($user){
            if(Auth::loginUsingId($user->id))

                return Auth::user()->admin ? redirect()->route('admin.home') : redirect('/');
            else
                return back()->withErrors(['Invalid Credentials']);
        }else{
            return back()->withErrors(['Invalid Credential']);
        }
    }
}
