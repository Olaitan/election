<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public function users() {
        return $this->belongsToMany(User::class, 'votes', 'user_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
