<?php

namespace App\Console\Commands;

use App\Candidate;
use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class LoadSRC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:src';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Candidate::query()->count() > 2) {
            $this->line('Already Loaded');
            return;
        }

        $excos = Storage::files('src');
        foreach ($excos as $exco) {
            preg_match("~/([\w\. ]+) - ([\w ]+)~", $exco, $m);
            $name = $m[1];
            $position = $this->trim($m[2]);

            $position = Post::query()->firstOrCreate([
                'title' => $position,
                'type'=> 'src'
            ]);

            Candidate::query()->create([
                'name' => $name,
                'post_id' => $position->id,
                'image_link' => $exco
            ]);
        }
        $this->line('Added ' . count($excos) . ' srcs');
    }
    public function trim($text) {
        $text = str_replace('SRC', 'STUDENT REPRESENTATIVE COUNCIL', $text);

        return trim($text);
    }
}
