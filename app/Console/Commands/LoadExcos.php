<?php

namespace App\Console\Commands;

use App\Candidate;
use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class LoadExcos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:excos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Candidate::query()->count() > 2) {
            $this->line('Already Loaded');
            return;
        }

        $excos = Storage::files('excos');
        foreach ($excos as $exco) {
            preg_match("~/([\w\. ]+) - ([\w ]+)~", $exco, $m);
            $name = $m[1];
            $position = $this->trim($m[2]);

            $position = Post::query()->firstOrCreate([
                'title' => $position
            ]);

            Candidate::query()->create([
                'name' => $name,
                'post_id' => $position->id,
                'image_link' => $exco
            ]);
        }
        $this->line('Added ' . count($excos) . ' excos');

        //SRC lines
        $excos = Storage::files('src');
        foreach ($excos as $exco) {
            preg_match("~/([\w\. ]+) - ([\w ]+)~", $exco, $m);
            $name = $m[1];
            $position = $this->trim($m[2]);

            $position = Post::query()->firstOrCreate([
                'title' => $position,
                'type'=> 'src'
            ]);

            Candidate::query()->create([
                'name' => $name,
                'post_id' => $position->id,
                'image_link' => $exco
            ]);
        }
        $p = Post::query()->where('title', 'ASSISTANT GENERAL SECRETARY')->first();
        Candidate::query()->create([
            'name' => 'BABATUNDE MFR',
            'post_id' => $p->id,
            'image_link' => '/'
        ]);


        $this->line('Added ' . count($excos) . ' srcs');
            //BABATUNDE MFR
        $srcs = [
            [
                'name'=>'DE KING FATHER',
                'dep'=>'FINANCE'
            ],
            [
                'name'=>'EMERALD',
                'dep'=>'FINANCE'
            ],
            [
                'name'=>'SNAZZY STAR',
                'dep'=>'PUBLIC ADMINISTRATION'
            ],
            [
                'name'=>'EMPEROR',
                'dep'=>'PUBLIC ADMINISTRATION'
            ],
            [
                'name'=>'CROWN BOLAZ',
                'dep'=>'MARKETING'
            ],
            [
                'name'=>'HON ADIGUN',
                'dep'=>'MARKETING'
            ],
            [
                'name'=>'MHIZ 4TUNE',
                'dep'=>'MARKETING'
            ],
            [
                'name'=>'TOP A',
                'dep'=>'BUSINESS ADMINISTRATION'
            ],


        ];
        foreach($srcs as $src){
            $position = Post::query()->firstOrCreate([
                'title' => 'STUDENT REPRESENTATIVE COUNCIL',
                'type'=> 'src'
            ]);
            Candidate::query()->create([
                'name' => $src['name'],
                'post_id' => $position->id,
                'image_link' => '/',
                'department'=>$src['dep']
            ]);
        }
        $this->line('Added ' . count($srcs) . ' srcs');

    }

    public function trim($text) {
        $text = str_replace('AGS', 'ASSISTANT GEN SEC', $text);
        $text = str_replace('GEN', 'GENERAL', $text);
        $text = str_replace('FIN', 'FINANCIAL', $text);
        $text = str_replace('VP', 'VICE PRESIDENT', $text);
        $text = str_replace('PRO', 'PUBLIC RELATIONS OFFICER', $text);

        $text = str_replace('SRC', 'STUDENT REPRESENTATIVE COUNCIL', $text);

        $text = str_replace('SEC', 'SECRETARY', $text);
        return trim($text);
    }
}
