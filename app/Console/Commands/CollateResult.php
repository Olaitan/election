<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class CollateResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collate:result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = Post::query()->with('candidates.actual_votes')->orderBy('weight', 'desc')->get();
        $excel = Excel::create('voting');
        $data = [];

        foreach ($posts as $post) {
            $title = $post->title;
//            $excel = Excel::create($title);

            foreach ($post->candidates as $candidate) {
                $name = $candidate->name;
                $votes = 0;
                $voters = [];
                $invalid = 0;
                foreach ($candidate->actual_votes as $vote) {
                    $user = $vote->user;
                    if (!isset($voters[$user->matric_no])) {
                        $voters[$user->matric_no] = [
                            'name' => $user->name,
                            'Matric Number' => $user->matric_no,
                            'Level' => $user->level,
                            'Department' => $user->department,
                            'Submit Clicked' => 1,
                            'Time Voted' => $vote->created_at->toTimeString()
                        ];
                        $votes++;
                    } else {
                        $voters[$user->matric_no]['Time Voted'] = $voters[$user->matric_no]['Time Voted'] . ", " . $vote->created_at->toTimeString();
                        $voters[$user->matric_no]['Submit Clicked']++;
                        $invalid++;
                    }
                }

                $data[] = [
                    'Position' => $title,
                    'Name' => $name,
                    'Actual Votes' => $votes,
                    'Void Votes' => $invalid
                ];

                $voters = array_values($voters);
            }

//            $excel->save('xls', storage_path('app'));
        }
        $excel->sheet("first", function ($sheet) use ($data) {
            $sheet->fromArray($data);
        });
        $excel->save('xls', storage_path('app'));
        $this->line('Done with all');
    }
}
