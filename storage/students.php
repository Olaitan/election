<?php
return [
[
"matric_no"=>"18/66MA001",
"name"=>"ABDULJEELIL, Ridwan Olamilekan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA002",
"name"=>"ABDULLATEEF, Hikmah",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA003",
"name"=>"ABDULRAHEEM, Kamal- Deen",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA004",
"name"=>"ABDULRAHMAN, Jibola Hikmat",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA005",
"name"=>"ABDUSSALAM, Aisha Opeyemi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA006",
"name"=>"ABUBAKAR, Aisha",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA007",
"name"=>"ABUBAKAR, Quamdeen Olamilekan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA008",
"name"=>"ADEBAYO, Adekunle Jubril",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA009",
"name"=>"ADEBAYO, Precious Ayomide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA010",
"name"=>"ADEBISI, Bolanle Titilayo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA011",
"name"=>"ADEBOWALE, Khadijat Olamide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA012",
"name"=>"ADEBOYE, Adesola Helen",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA013",
"name"=>"ADEFILA, Taiwo Janet",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA014",
"name"=>"ADEKUNLE, Oluwapelumi David",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA015",
"name"=>"ADELANI, Susan Oyindamola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA016",
"name"=>"ADELANI, Toheeb Adekunle",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA017",
"name"=>"ADELEKE, Damilola Oluwasemilore",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA018",
"name"=>"ADELEKE, Iyanuoluwa Irewole",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA019",
"name"=>"ADENIYI, Oluwasegun Joshua",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA020",
"name"=>"ADEOGUN, Tunmise Esther",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA021",
"name"=>"ADEOTI, Fathia Oyindolapo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA022",
"name"=>"ADEOYE, Faith Oluwatosin",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA023",
"name"=>"ADERINTO, Oluwalosolaeyi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA024",
"name"=>"ADEWUMI, Sarah Ifeoluwa",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA025",
"name"=>"ADEWUSI, Folarin Ridwan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA026",
"name"=>"ADEYEMI, Azeez Olayiwola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA027",
"name"=>"ADEYEMI, Fawaz Olanrewaju",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA028",
"name"=>"ADEYEMO, Oluwafemi John",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA029",
"name"=>"ADEYEMO, Shukurah Opeyemi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA030",
"name"=>"ADEYINKA, Angel Naomi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA031",
"name"=>"ADIMULA, Michael Ayobolanle",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA032",
"name"=>"AFOLAYAN, Muhammed Ololade",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA033",
"name"=>"AFOLAYAN, Opeyemi Deborah",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA034",
"name"=>"AGBAJE, Aminat Oluwadamilola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA035",
"name"=>"AGBOOLA, Esther Oluwatoyosi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA036",
"name"=>"AHMADU, Ayomide Triumph",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA037",
"name"=>"AHMED, Misturah Omooroh",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA038",
"name"=>"AINA, Divine-grace Ayomitade",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA039",
"name"=>"AJAYI, Theophilus Vim",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA040",
"name"=>"AJIBOLA, Zainab Abimbola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA041",
"name"=>"AJISEGIRI, Sulaimon Opeyemi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA042",
"name"=>"AJITONI, Toluwase Stephen",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA043",
"name"=>"AKANDE, Mustapha Abolade",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA044",
"name"=>"AKANDE, Olasoji Daniel",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA045",
"name"=>"AKANDE, Toluwalope Ifeoluwa",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA046",
"name"=>"AKINSIPE, Toluwalase Ayomide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA047",
"name"=>"AKOGUN, Oluwaseyi Christopher",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA048",
"name"=>"ALABI, Lukman Temitope",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA049",
"name"=>"ALABI, Mercy Oluwadara",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA050",
"name"=>"ALADE, Kaothar Omolabake",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA051",
"name"=>"ALAMUTU, Taiwo Rasheed",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA052",
"name"=>"ALAO, Esther Tolulope",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA053",
"name"=>"AMEH, Esther Praise",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA054",
"name"=>"AMOO, Ismail Toluwanimi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA055",
"name"=>"ANAMANYA, Vincent Chetachukwu",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA056",
"name"=>"ANOFI, Ramota Bolanle",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA057",
"name"=>"ANOKWURU, Victory Patrick",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA058",
"name"=>"ARASE, Uyioghsa Ayomide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA059",
"name"=>"ARIGBANLA, Abeeb Olayinka",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA060",
"name"=>"ATUNLUTE, Demilade Elizabeth",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA061",
"name"=>"BAKARE, Idayat Adeola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA062",
"name"=>"BAMIDELE, Boluwatife Isaac",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA063",
"name"=>"BAYOH, Amera",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA064",
"name"=>"BELLO, Abdullahi Olamilekan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA065",
"name"=>"BERETE, Fode Bangaly",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA066",
"name"=>"BISIRIYU, Naheemat Ololade",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA067",
"name"=>"CONDE, Aisha Hassan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA068",
"name"=>"DABO, Mohammed Lamine",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA069",
"name"=>"DADA, Precious Oyindamola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA070",
"name"=>"DAGAH, Valentino Dantong",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA071",
"name"=>"DARAMOLA, Olajumoke Mercy",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA072",
"name"=>"DAUDA, Qudus Omeiza",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA073",
"name"=>"DUROWADE, Oke Pelumi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA074",
"name"=>"EDWARD, Joshua Korede",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA075",
"name"=>"EGBEYEMI, Fawaz Eyijolalo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA076",
"name"=>"FADODUN, Esther Ayomide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA077",
"name"=>"FASHANU, Oluwatosin Mary",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA078",
"name"=>"FASINA, Gideon Oluwadamilare",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA079",
"name"=>"FOLORUNSHO, Olugbenga Daniel",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA080",
"name"=>"HAMZAT, Barakat Olatomiwa",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA081",
"name"=>"HAMZAT, Fatimah Ololade",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA082",
"name"=>"IBRAHIM, Babatunde Qurwiyu",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA083",
"name"=>"IBRAHIM, Balqees Ayo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA084",
"name"=>"IBRAHIM, Mariam Oyindamola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA085",
"name"=>"IKOR, Simon Otsai",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA086",
"name"=>"ISAU, Zulikifil Olanrewaju",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA087",
"name"=>"ISHOLA, Quadri Abiola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA088",
"name"=>"ISMAIL, Abdulafeez Olalekan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA089",
"name"=>"IWASOKUN, Oluwasetemi Samuel",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA090",
"name"=>"IYIOLA, Kabirat Oladoyin",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA091",
"name"=>"JIBUIKE, Comfort Oluwabunmi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA092",
"name"=>"JIMOH, Anthonia Oriyomi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA093",
"name"=>"JOEL, Aaron Opemipo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA094",
"name"=>"JOHNSON, Boluwatife Abigail",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA095",
"name"=>"KAZEEM, Faruq Ayobami",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA096",
"name"=>"KOLADE, Faidat Bolajoko",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA097",
"name"=>"KOLADE, Praise Oluwademilade",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA098",
"name"=>"LAWAL, Abdulmalik Tolulope",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA099",
"name"=>"LAWAL, Mariam Eniola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA100",
"name"=>"LAWAL, Miracle Ahuoyiza",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA101",
"name"=>"MARE, Odunayo Omolara",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA102",
"name"=>"MATTHEW, Kehinde Mary",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA103",
"name"=>"MIKE-SANUSI, Joshua Oluwatosin",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA104",
"name"=>"MOHAMMED, Ni Ojodomo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA105",
"name"=>"MUMEEN, Muis Adeshina",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA106",
"name"=>"MURAINA, Jamiu Olawale",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA107",
"name"=>"MUSTAPHA, Aishat Oluwadamilola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA108",
"name"=>"MUSTAPHA, Feyisayo Umar",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA109",
"name"=>"NKEMDILIM, Victor Ifeoma",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA110",
"name"=>"OBIJOLE, Favour Tolulope",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA111",
"name"=>"ODESANYA, Damilola David",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA112",
"name"=>"ODUKOYA, Oyindamola Rachael",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA113",
"name"=>"ODUNSI, Adeyinka Gabriel",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA114",
"name"=>"ODUSANYA, Wisdom Gabriel",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA115",
"name"=>"OGBAJE, Deborah Elejoh",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA116",
"name"=>"OGUNDELE, Oluwatomilola Ayomipo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA117",
"name"=>"OGUNDOKUN, Akeem Adeyemi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA118",
"name"=>"OGUNGBADE, Olayemi Olalekan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA119",
"name"=>"OGUNGBENLE, Ruth Damilola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA120",
"name"=>"OGUNKUNLE, Ayobami Emmanuel.",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA121",
"name"=>"OGUNWALE, Opeyemi Abraham",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA122",
"name"=>"OKAFOR, Joel Nkemuka",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA123",
"name"=>"OKOJIE, Oseghae Kevin",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA124",
"name"=>"OLABOSHINDE, Eniola Favour",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA125",
"name"=>"OLADOKUN, Stella Olamide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA126",
"name"=>"OLALEYE, Joel Oluwadunsin",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA127",
"name"=>"OLANIYI, David Olajide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA128",
"name"=>"OLATOYINBO, Kabeerah Bukola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA129",
"name"=>"OLATUNJI, Ibrahim Gbolahan",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA130",
"name"=>"OLAWALE, Sofiat Oluwapelumi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA131",
"name"=>"OLAWUMI, Joanna Adeibukun",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA132",
"name"=>"OLAYIWOLA, Habeeb Abiola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA133",
"name"=>"OLOJEDE, Sekinat Oluwabukola.",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA134",
"name"=>"OLORUNNISOMO, Irewande Melissa",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA135",
"name"=>"OLOYEDE, Ifeoluwapo Esther",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA136",
"name"=>"OLUBO, Glory Ayokanmi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA137",
"name"=>"OLUWALOSE, Moshood Oladipupo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA138",
"name"=>"OLUWAYOMI, Kayode Gbenga",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA139",
"name"=>"OLUWGBEMIGA, Omolara Esther",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA140",
"name"=>"OMIKUNLE, Oluwagbemileke Joseph",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA141",
"name"=>"OMODANISI, Joy Omotola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA142",
"name"=>"OMOTOSHO, Solomon Iyiola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA143",
"name"=>"OSENI, Rukayat Ajoke",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA144",
"name"=>"OSHO, Abdulquadri Oluwatosin",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA145",
"name"=>"OWATUNDE, Omotayo Dorcas",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA146",
"name"=>"OWOFUNWA, Opeyemi Joseph",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA147",
"name"=>"OWOLABI, Racheal Omobolanle",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA148",
"name"=>"OWOLOWO, Precious Obasegun",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA149",
"name"=>"OYENIYI, Dahood Damilare",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA150",
"name"=>"OYENIYI, Oyindamola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA151",
"name"=>"OYEWO, Oyeronke Barakat",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA152",
"name"=>"OYEWUSI, Odunayo Oluwafemi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA153",
"name"=>"RASAQ, Titilayo Zainab",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA154",
"name"=>"SALAM, Olaide Tinuke",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA155",
"name"=>"SALAMI, Ridwan Omotayo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA156",
"name"=>"SALAUDEEN, Sodiq Olayinka",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA157",
"name"=>"SANNI, Jemeelat Omolara",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA158",
"name"=>"SANNI, Sulaimon Opeyemi",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA159",
"name"=>"SANUSI, Aisha Amirah",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA160",
"name"=>"SANYA, Oluwanifemi Favour",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA161",
"name"=>"SEIDU, Fatima Oluwatobiloba",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA162",
"name"=>"SOLOMON, Kakachita Bessie",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA163",
"name"=>"SULAIMAN, Habeeb Babatunde",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA164",
"name"=>"SULEIMAN, Fatiat Damilola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA165",
"name"=>"SUNDAY, Kolawole",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA166",
"name"=>"TAIWO, Habeeb",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA167",
"name"=>"UGWU, Glory Chioma",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA168",
"name"=>"WAKILI, Muhammad Zayyad",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA169",
"name"=>"WAKILU, Fathia Ayomide",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA170",
"name"=>"YUNUS, Yahaya Abiola",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA171",
"name"=>"YUSUF, Abdulwahab",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA172",
"name"=>"YUSUF, Hamzat Idowu",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA173",
"name"=>"YUSUFF, Rahmatullahi Sayo",
"level"=>"100",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA174",
"name"=>"ABDULSALAM, Abdullateef Folorunsho",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA175",
"name"=>"ADEDEJI, Daniel Akintunde",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA176",
"name"=>"ADEDOKUN, Adejumoke Stella",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA177",
"name"=>"ADEGBOYE, Damilola Moyinoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA178",
"name"=>"ADEKANYE, Abiodun Quadri",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA179",
"name"=>"ADEOLUWA, Febisola Oluwapelumi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA180",
"name"=>"ADEOSUN, Ridhwan O",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA181",
"name"=>"AGBAJE, Emmanuel Temitope",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA182",
"name"=>"AINA, Oluwapelumi Onaopemipo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA183",
"name"=>"AJEIGBE, Joshua Iyanuoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA184",
"name"=>"AJIBOYE, Aishat Olayinka",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA185",
"name"=>"AKINLOLU, Ayomide Precious",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA186",
"name"=>"AKURE, Solomon Abiola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA187",
"name"=>"ALFRED, Gbemisola Esther",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA188",
"name"=>"ALIU, Christianah Omotayo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA189",
"name"=>"AMOO, Abisola Olajumoke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA190",
"name"=>"CHIKODIRI, Hannah Nnenna",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA191",
"name"=>"DARAMOLA, Oluwatosin Mary",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA192",
"name"=>"GARUBA, Niniola Simbiat",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA193",
"name"=>"IHEME, Precious Chiamaka",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA194",
"name"=>"ISSA, Muyideen",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA195",
"name"=>"KALEJAIYE, Joseph Pelumi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA196",
"name"=>"KOLAWOLE, Kelvin Babatunde",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA197",
"name"=>"LAWAL, Shareefat Tunrayo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA198",
"name"=>"MAKINDE, Adedolapo Babatunde",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA199",
"name"=>"NDUMANYA, Gladys Chinenye",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA200",
"name"=>"OGUNDARE, Idowu Busayo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA201",
"name"=>"OGUNSHINA, Bisola Victoria",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA202",
"name"=>"OKEWOLE, Anu Isaac",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA203",
"name"=>"OMATSEYE, Blessing Mamuyovwi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA204",
"name"=>"OMODARA, Oluwatobiloba Maria",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA205",
"name"=>"ONAOLAPO, Idris Olamilekan",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA206",
"name"=>"OSENI, Aishat Olamide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA207",
"name"=>"RUFAI, Shakirudeen Abidemi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA208",
"name"=>"SALAMI, Ololade Rhoda",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA209",
"name"=>"SALAU, Habeeb Deola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA210",
"name"=>"SANNI, Yusuf Adewale",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA211",
"name"=>"SHOLA, Favour Bola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA212",
"name"=>"SULAIMAN, Ishaq Olamide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MA213",
"name"=>"USMAN, Aishat Bukola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA001",
"name"=>"ABDULKAREEM, Zainab Ajike",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA002",
"name"=>"ABDULQUADIR, Ayobami Nafisat",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA003",
"name"=>"ABDULRASAQ, Makeen Adewale",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA004",
"name"=>"ABERUAGBA, Damola Oladipupo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA005",
"name"=>"ABOBARIN, Damilola Dorcas",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA006",
"name"=>"ABOLARIN, Rebecca Adeola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA007",
"name"=>"ADAH, Lydia Ada",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA008",
"name"=>"ADEAGBO, Maryam Oyindamola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA009",
"name"=>"ADEBAYO, Emmanuel Soji",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA010",
"name"=>"ADEBAYO, Yusuff Akanni",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA011",
"name"=>"ADEKEYE, Samuel Dotun",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA012",
"name"=>"ADEKUNLE, Teslim Oluwasegun",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA013",
"name"=>"ADENIJI, Victor Ayodele",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA014",
"name"=>"ADEOSUN, Ruth Damilola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA015",
"name"=>"ADESANYA, Temitope Ibrahim",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA016",
"name"=>"ADEWOLE, Idris Gbolahan",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA017",
"name"=>"ADEWUMI, Victoria Oluwagbemiga",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA018",
"name"=>"ADEWUYI, Opeyemi Faridat",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA019",
"name"=>"ADEYANJU, Rodiyyah Mosunmola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA020",
"name"=>"ADEYEMI, Olufemi Akinbami",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA021",
"name"=>"ADEYEMI, Oluwaseun Pelumi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA022",
"name"=>"ADEYEYE, Ademola David",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA023",
"name"=>"ADIGUN, Olawale Gaius",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA024",
"name"=>"AFOLAYAN, Anointed-faith Iyiola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA025",
"name"=>"AINA, Emmanuel Eniola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA026",
"name"=>"AINA, Oluwatosin Thompson",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA027",
"name"=>"AINA, Sunday Oluwadamilola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA028",
"name"=>"AIYEKITAN, Mercy Mojolaoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA029",
"name"=>"AJAYEOBA, Adeola Rebecca",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA030",
"name"=>"AJIBODU, Olukunle Moses",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA031",
"name"=>"AJIMATI, Deborah Oyinkansola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA032",
"name"=>"AJISEGIRI, Mistura Morenikeji",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA033",
"name"=>"AKAGBO - SEDOFIA, Sedinam Josephine",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA034",
"name"=>"AKANDE, Idowu Joseph",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA035",
"name"=>"AKINMADE, Elizabeth Oreoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA036",
"name"=>"AKINOLA, Harris Tolulope",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA037",
"name"=>"AKINTOBI, Precious Adunola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA039",
"name"=>"ALE, Oluwadamilola Opeyemi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA040",
"name"=>"ALIU, Aisha Motunrayo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA041",
"name"=>"ANUSIOBI, Peace Chi-amara",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA042",
"name"=>"AREGBESOLA, Joshua Olusegun",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA043",
"name"=>"AREMU, Zainab Oluwatomi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA044",
"name"=>"AREO, David Damilare",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA045",
"name"=>"ARIYO, Shefiu Oluwakayode",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA046",
"name"=>"ATOYEBI, Grace Oluwaseyi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA047",
"name"=>"AYENI, Temilade Grace",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA048",
"name"=>"AYO, Keziah Agbonhijagwe",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA049",
"name"=>"AYODELE, Rukayat Oyindamola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA050",
"name"=>"AZEEZ, Oluwatise Kolawole",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA051",
"name"=>"BABATUNDE, Hope Oluwatofunmi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA052",
"name"=>"BALOGUN, Barakat Ayomide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA053",
"name"=>"BALOGUN, Faheedat Olabisi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA054",
"name"=>"BAMIDELE, Remilekun Heritage",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA055",
"name"=>"BANKOLE, Janet Oluwaseun",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA056",
"name"=>"BOLAJI, Victor Adepoju",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA057",
"name"=>"BOLAKALE, Halimat Bukola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA058",
"name"=>"BRIMAH, Abdul-aziz Ebunoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA059",
"name"=>"BUSAYR, Aminat Oluwaseun",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA060",
"name"=>"CALEB, Oyiza Ovayoza",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA061",
"name"=>"DANJUMA, Unekwuojo Miracle",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA062",
"name"=>"EBO, Ifeoluwa Praise",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA063",
"name"=>"EMMANUEL, Vision Tosin",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA064",
"name"=>"ENIMOLA, Opeyemi Joy",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA065",
"name"=>"ENIOJUKAN, Shekinah Modupe",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA066",
"name"=>"EZENWA, Michael Chijioke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA067",
"name"=>"EZENWAKA, Margaret-mary Onyinye",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA068",
"name"=>"FABIYI, Oluwadamilola Adenike",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA069",
"name"=>"FAKEYE, Duke Fadayomi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA070",
"name"=>"FARAYOLA, Oluwatosin Oyindamola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA071",
"name"=>"FASASI, Eniola Olabisi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA072",
"name"=>"FATAI, Aishat Oluwanishola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA073",
"name"=>"FATAI, Mubarak Olamilekan",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA074",
"name"=>"FATOSA, Taiwo Olamide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA075",
"name"=>"FATOYE, Favour Olanike",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA076",
"name"=>"FRIDAY-OTUN, Boluwaduro Prevail",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA077",
"name"=>"GABRIEL, Oluwatimilehin E",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA078",
"name"=>"GANIYU, Idris Ayoade",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA079",
"name"=>"GBADEYAN, Divine Oluwasemilore",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA080",
"name"=>"GBENLE, Peace Ayomide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA081",
"name"=>"GHAZAL, Ameenat Olajumoke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA082",
"name"=>"IBIYEYE, Aanu Victoria",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA083",
"name"=>"IBRAHEEM, Raheemot Opeyemi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA084",
"name"=>"IBRAHIM, Fatimah Oyiza",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA085",
"name"=>"IBRAHIM, Hikmat Tomiwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA086",
"name"=>"IBRAHIM, Shakirudeen Temitayo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA087",
"name"=>"IMRAN, Abdulsalam Adewunmi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA088",
"name"=>"INYANDA, David ",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA089",
"name"=>"ISIBOR, Serah Ukpomo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA090",
"name"=>"JEREMIAH, Serah Temitope",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA091",
"name"=>"JIMOH, Rofiat Adefunke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA092",
"name"=>"JOHN, Temitope Moses",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA093",
"name"=>"JUNAID, Oluwagbemiga Razaq",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA094",
"name"=>"MORAKINYO, Adekunle Sheriff",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA095",
"name"=>"MORENIKEJI, Mayowa Solomon",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA096",
"name"=>"MUHAMMED, Ayomide Aminat",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA098",
"name"=>"MUSTAPHA, Sodiq Babatunde",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA097",
"name"=>"MUSTAPHA, Teslim Adebayo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA099",
"name"=>"NINIOLA, Opeyemi Bushroh",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA100",
"name"=>"NWEKE, Chukwubinite Emmanuel",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA101",
"name"=>"OBILEYE, Matthew Dare",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA102",
"name"=>"ODEKUNLE, Modinat Oyinlade",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA103",
"name"=>"ODUKOYA, Emmanuel Babasiji",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA104",
"name"=>"ODUNLAMI, Rasheed ",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA105",
"name"=>"OGIDI, Ephraim Boluwatife",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA106",
"name"=>"OGUNRINDE, Olayinka C",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA107",
"name"=>"OHANYERENWA, Donald Chukwudi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA108",
"name"=>"OKEOWO, Ahmed Olamide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA109",
"name"=>"OLADIMEJI, Adetola Ezekiel",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA110",
"name"=>"OLADIMEJI, Azeezat Oreoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA111",
"name"=>"OLADIMEJI, Rukayat Olamide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA112",
"name"=>"OLANIPEKUN, Abdulbarr Abimbola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA113",
"name"=>"OLANIYI, Temidayo Abdulmalik ",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA114",
"name"=>"OLAOFE, Amirah Opeyemi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA115",
"name"=>"OLASINDE, Abigail Oluwatosin",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA116",
"name"=>"OLAYIWOLA, Mayowa Olajoju",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA117",
"name"=>"OLAYIWOLA, Victor Ifeoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA118",
"name"=>"OLAYODE, Oluwatobi Joel",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA119",
"name"=>"OLORIEGBE, Abdulhameed ",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA120",
"name"=>"OLORODE, Godwin Stephen",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA121",
"name"=>"OLUWAMUYIWA, Temitope Kayode",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA122",
"name"=>"OLUWAYALE, Favour Ayomide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA123",
"name"=>"OMAR, Dalhatu Muazu",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA124",
"name"=>"OMOTOLA, Lois Ifeoluwa",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA125",
"name"=>"OMOTOSHO, Iyanuoluwa Grace",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA126",
"name"=>"OMOTOSO, Ifeloluwa Priscillia",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA127",
"name"=>"ONIWA, Ruqoyah Damilola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA128",
"name"=>"OPALUWA, Christiana Chubiyo",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA129",
"name"=>"OPOOLA, Kabirat Olajumoke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA130",
"name"=>"OTOR, Evevlyn Onahi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA131",
"name"=>"OWOLABI, Lateefat Damilola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA132",
"name"=>"OYENIYI, Samuel Olamilekan",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA133",
"name"=>"OYESHOLA, Similoluwa Lizzy",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA134",
"name"=>"OYETOLA, Oyewumi Adedamola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA135",
"name"=>"OYINLOYE, Esther Olajumoke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA136",
"name"=>"PAUL, Dorcas Ojimah",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA137",
"name"=>"POPOOLA, Abdulrasaq Osemhei-khi",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA138",
"name"=>"POPOOLA, Adedoyin Joseph",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA139",
"name"=>"SAHEED, Abdullateef Olarewaju",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA140",
"name"=>"SAHEED, Quadri Aremu",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA141",
"name"=>"SALAU, Ganiyat Folashade",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA142",
"name"=>"SALAU, Olajumoke Celestina",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA143",
"name"=>"SALIU, Abdulazeez Olamide",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA144",
"name"=>"SAMUEL, Omotola Oluwayimika",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA145",
"name"=>"SHEHU, Abubakar Mann",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA146",
"name"=>"SHITTU, Ayomide Loveth",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA147",
"name"=>"SODIQ, Muhammed Adewale",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA148",
"name"=>"SONUYI, Adefemi Daniel",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA149",
"name"=>"SULAIMON, Khadijat Bolatito",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA150",
"name"=>"TEJUMOLA, Abisola Esther",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA151",
"name"=>"TIJANI, Hikmat Jumoke",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA152",
"name"=>"TIJANI, Nofisat Titilola",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA153",
"name"=>"TITUS, Recheal Modupe",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA154",
"name"=>"TOBA, Ebunoluwa Comfort",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA156",
"name"=>"UGBAJAH, Gift Onyinye",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA157",
"name"=>"USMAN, Abdulateef Olalekan",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA158",
"name"=>"WEAH, Juah  Victoria",
"level"=>"200",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA159",
"name"=>"ABBA, Amarachi Deborah",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA001",
"name"=>"ABDULGANIYU, Sofiat Bisola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA002",
"name"=>"ABDULSALAM, Kafayat Damilola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA160",
"name"=>"ABDULYEKEEN, Sirajudeen Opeyemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA003",
"name"=>"ABIOLA, Halimat Omotayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA004",
"name"=>"ABODUNRIN, Radyah Olamide",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA005",
"name"=>"ABOLARIN, Olaide Funmilola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA006",
"name"=>"ADAVA, Oluwatobi Samuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA007",
"name"=>"ADEBAYO, Oyindamola Elizabeth",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA161",
"name"=>"ADEBESIN, Olushola Andrew",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA008",
"name"=>"ADEBIMPE, Oluwaseun Damilola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA009",
"name"=>"ADEDAYO, Kehinde Adefolarin",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA011",
"name"=>"ADEGOKE, Ope ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA012",
"name"=>"ADEKEYE, Stella Abiola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA013",
"name"=>"ADEKOYA, John Adebisi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA014",
"name"=>"ADEKUNLE, Rashidat Opeyemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA015",
"name"=>"ADEKUNLE, Rilwan ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA016",
"name"=>"ADELEKE, Bukola Mary",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA018",
"name"=>"ADENAYA, Rachael Oluwafunmilayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA019",
"name"=>"ADENIJI, Faruq Ayomide",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA020",
"name"=>"ADENIYI, Aminat Aderayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA021",
"name"=>"ADENIYI, Shola Henry",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA022",
"name"=>"ADEWUMI, Ifeoluwa Samuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA162",
"name"=>"ADEYEMI, Azeezat Bukola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA024",
"name"=>"ADEYEYE, Mercy Tofunmi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA163",
"name"=>"ADEYEYE, Oluwakemi Abiodun",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA025",
"name"=>"ADIDI, Gerald Onoriode",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA164",
"name"=>"AFOLAYAN, Kabirat Adeola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA165",
"name"=>"AILERU, Alimot Moturayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA166",
"name"=>"AJALA, Odunayo Ajoke",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA026",
"name"=>"AJETUNMOBI, Khairat Omotoyosi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA027",
"name"=>"AJIBADE, Muritala Abiodun",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA028",
"name"=>"AJIBOLA, Ayomide Stephen",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA029",
"name"=>"AKANJI, Olamide Pamilerin",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA030",
"name"=>"AKOLADE, Zainab Jolayemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA167",
"name"=>"ALABI, Khadijat Olamide",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA031",
"name"=>"ALABI, Mariam Oluwatosin",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA032",
"name"=>"ALABI, Oluwakemi Deborah",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA033",
"name"=>"ALABI, Oluwaseyi ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA034",
"name"=>"ALABI, Rukayat Omobolanle",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA035",
"name"=>"ALASI, Abeebullahi Oladeji",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA036",
"name"=>"ALIYU, Afeez ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA037",
"name"=>"ALIYU, Issa Shehu",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA038",
"name"=>"AMOO, Tajudeen Alade",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA039",
"name"=>"AMUSAT, Aminat Adebukola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA040",
"name"=>"ANDREW, Benard Adeiza",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA168",
"name"=>"ANTHONY, Happiness Ewere",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA041",
"name"=>"ANWO, Moradeun Suwebat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA042",
"name"=>"AROWOLO, Adefikayomi ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA044",
"name"=>"AYEYI, Ayorinde Samuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA169",
"name"=>"AYOBAMI, Barakat Bisola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA045",
"name"=>"AZEEZ, Islamiyah Damilola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA046",
"name"=>"AZEEZ, Oluwapelumi Adeyemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA047",
"name"=>"BABALOLA, Moyosola Grace",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA049",
"name"=>"BALOGUN, Jisola Princess",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA052",
"name"=>"BOLAJI, Waliyat Temitope",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA053",
"name"=>"BOLASERE, Taiwo Oluwaseyi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA054",
"name"=>"BUSHRA, Hamidat ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA055",
"name"=>"DANIEL, Oreoluwa Elizabeth",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA056",
"name"=>"DARAMOLA, Musa Olawale",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA057",
"name"=>"DARE, Tracy Oluwagbemisola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA058",
"name"=>"DAUDA, Shukurat Toyosi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA059",
"name"=>"DIAKITE, Adjasadan Kaba",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA060",
"name"=>"DUROHA, Richard Emeka",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA061",
"name"=>"DUROTOYE, Sulaiman Olalekan",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA170",
"name"=>"EGBEYEMI, Opeyemi Oluwadamilare",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA171",
"name"=>"EJEMA, Victory Ehi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA062",
"name"=>"ELEGBEDE, Olamide Emmanuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA063",
"name"=>"FABIYI, Anuoluwa Blessing",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA065",
"name"=>"FAROTIMI, Anjolaoluwa Ayomide",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA066",
"name"=>"FASHOLA, Opeyemi Emmanuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA068",
"name"=>"FOFANA, Musa ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA069",
"name"=>"HAMZAT, Zainab Morolayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA070",
"name"=>"IBOYI, Hope Aminat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA071",
"name"=>"IBRAHIM, Damilola Christianah",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA072",
"name"=>"IBRAHIM, Fatima Omowunmi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA073",
"name"=>"IDOWU, Phebe Oreoluwa",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA074",
"name"=>"IFEANYI, Chinedu Hope",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA075",
"name"=>"ILORI, Oreoluwa Deborah",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA076",
"name"=>"ISA, Mariam ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA077",
"name"=>"ISHAQ, Habeeb Alabi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA078",
"name"=>"ISMAILA, Halimat Sadia",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA172",
"name"=>"IYANDA, Ayodeji Sultan",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA173",
"name"=>"JAMES, Dorathy Eleojo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA081",
"name"=>"JEREMIAH, Abidemi Joyce",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA082",
"name"=>"JIMOH, Kehinde Hassanat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA083",
"name"=>"JINADU, Aliyyah Bolaji",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA084",
"name"=>"JOHN, Omodolapo Odunayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA174",
"name"=>"JOHNSON, Joshua Ayobami",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA085",
"name"=>"KAREEM, Islamiyat Olajumoke",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA086",
"name"=>"KAREEM, Sultan Babatunde",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA175",
"name"=>"KAZEEM, Abigael Oluwaseun",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA087",
"name"=>"KORODE, Glory Ifedolapo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA088",
"name"=>"LAWAL, Boluwatife Mariam",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA176",
"name"=>"LAWAL, Yetunde Roheemat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA090",
"name"=>"MAJAROOLA, Mercy Victoria",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA177",
"name"=>"MAROOF, Hauwau Aliyu",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA091",
"name"=>"MATTHEW, Sunday ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA092",
"name"=>"MICHAEL, Victoria Aina",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA178",
"name"=>"MOHAMMED, Fridaus Omowumi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA093",
"name"=>"MOIJUEH, Musa ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA094",
"name"=>"MORUFU, Mumeen ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA095",
"name"=>"MUHAMMED, Ayuba Olayinka",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA096",
"name"=>"MUSA, Ibukun Khadijat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA097",
"name"=>"MUSTAPHA, Temitope Rofiat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA179",
"name"=>"NOFIUDEEN, Lukmon Adeola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA098",
"name"=>"NUHU, Shukurat Titilope",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA099",
"name"=>"OCHABA, Alimat Faith",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA100",
"name"=>"ODEWOLE, Yetunde Hanna",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA101",
"name"=>"ODIDIKA, Ifebube Mary",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA102",
"name"=>"ODUJOBI, Opeyemi Samuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA103",
"name"=>"ODUTOLA, Esther Funmilayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA180",
"name"=>"OGUNDOYIN, Boluwatife Racheal",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA080",
"name"=>"OGUNJOBI, Ayorinde James",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA104",
"name"=>"OGUNTUYI, Ifedolapo Mary",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA105",
"name"=>"OKECHUKWU, Kelechi Peter",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA106",
"name"=>"OLA-AKANBI, Farhan Adedoyinsola",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA107",
"name"=>"OLABANJI, Oluwabusayo Lucy",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA181",
"name"=>"OLABODE, Teniola Temitayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA108",
"name"=>"OLADEPO, Bisola Rebecca",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA109",
"name"=>"OLADIMEJI, Taiye Abigeal",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA110",
"name"=>"OLADIPO, Muhammad Oladapo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA111",
"name"=>"OLADIPO, Oluwatobi Ifeoluwa",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA112",
"name"=>"OLADIPO, Oluwayemisi Ruth",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA113",
"name"=>"OLADIPO, Sammiat Omolara",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA114",
"name"=>"OLAITAN, Kayode ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA182",
"name"=>"OLAOYE, Isaiah Oluwanifemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA115",
"name"=>"OLASEHINDE, Adenike Mutiat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA116",
"name"=>"OLATUNDE, Akeem Olakunle",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA117",
"name"=>"OLAYEMI, Oluwadamilola Victoria",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA118",
"name"=>"OLOHUNGBEBE, Imam Opeyemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA119",
"name"=>"OLOWOLAGBA, Olamilekan Temitayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA120",
"name"=>"OLOYEDE, Rhoda Ojurereoluwa",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA121",
"name"=>"OLUBUNMI, Ifeoluwa Mary",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA122",
"name"=>"ONA-ARA, Opeyemi Ekundayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA123",
"name"=>"ONADEKO, Ololade Michael",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA124",
"name"=>"ONI, Abdulmuiz Olalekan",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA125",
"name"=>"ONIKEKU, Deborah Ifeoluwa",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA126",
"name"=>"OPAKUNLE, Ridwan ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA127",
"name"=>"OSANYINTOLA, Boluwatife Deborah",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA128",
"name"=>"OSOBA, Olamide Mariam",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA129",
"name"=>"OSUNSANYA, Oluwasegun Godwin",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA130",
"name"=>"OWODUNNI, Habib Adeyemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA131",
"name"=>"OWODUNNI, Misturah Omolara",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA133",
"name"=>"OYE, Oluwadamilola Dorcas",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA183",
"name"=>"OYENIYI, Toheeb Oyedeji",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA134",
"name"=>"OYETOYAN, Emmanuel Oluwaseun",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA184",
"name"=>"OYETUNDE, Rachel  Oluwatobi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA135",
"name"=>"PETINRIN, Blessing Tolulope",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA136",
"name"=>"POPOOLA, Gbeminiyi Emmanuel",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA137",
"name"=>"RAIMI, Damilola Esther",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA185",
"name"=>"RAJI, Adigun Ashifat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA138",
"name"=>"RASHEED, Zainab Abidemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA186",
"name"=>"RAZAK, Yusuf Ajide",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA187",
"name"=>"SADU, Jamiu Adisa",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA139",
"name"=>"SAHEED, Yusirat Titilope",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA140",
"name"=>"SALAMI, Esther Abiodun",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA141",
"name"=>"SALIU, Ahmed Danjuma",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA142",
"name"=>"SANNI, Abayomi Uthman",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA143",
"name"=>"SANNI, Abdulquddus Olanrewaju",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA144",
"name"=>"SHOGBAMU, Nodrah Opeyemi",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA188",
"name"=>"SOFOLUWE, Motunrayo Monsurat",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA145",
"name"=>"SUBAIRU, Farida Onize",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA146",
"name"=>"TAIWO, Damilola Titilayo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA147",
"name"=>"THANNI, Jubriel Adedolapo",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA148",
"name"=>"TIJANI, Halimat Adenike",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"17/66MA189",
"name"=>"TUKURA, Amina Muazu",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA149",
"name"=>"UGOEZU, Ebuka Anthony",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA150",
"name"=>"UMAR, Samsiyat ",
"level"=>"300",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA001",
"name"=>"ABDRAHIM, Suliah Olanibi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA002",
"name"=>"ABDUL-RASAQ, Raheemat ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA003",
"name"=>"ABDULAZEEZ, Aishat Sanni",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA004",
"name"=>"ABDULAZEEZ, Azeez Olaitan",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA005",
"name"=>"ABDULGANIY, Adam Adeolu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA006",
"name"=>"ABDULMALIK, Taofiq Adinoyi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA007",
"name"=>"ABDULQUADRI, Fadlullah Omoyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA151",
"name"=>"ABDULRASAQ, Azeez Kayode",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA008",
"name"=>"ABDULSALAM, Latifah Damilola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA009",
"name"=>"ABDULSALAM, Nofisat Kofoworola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA010",
"name"=>"ABEGUNDE, Abraham ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA011",
"name"=>"ABEGUNDE, Busayo Abike",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA012",
"name"=>"ABIODUN, Ruth Afolashade",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA175",
"name"=>"ABIODUN, Titilayo Success",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA013",
"name"=>"ADACHE, Joseph Ugbedeojo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA152",
"name"=>"ADAMS, Afeez Adesina",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA014",
"name"=>"ADEBANJO, Tope Abosede",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA016",
"name"=>"ADEBAYO, Deborah Olaitan",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA017",
"name"=>"ADEBAYO, Oluwatosin Adeola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA018",
"name"=>"ADEDAYO, Sheriff Olajide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA153",
"name"=>"ADEDEJI, Emmanuel Temilade",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA017",
"name"=>"ADEDEJI, Oluwaseun Eunice",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA019",
"name"=>"ADEDIRAN, Kehinde Adeola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA020",
"name"=>"ADEDOYIN, Moboluwaduro Daniel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA154",
"name"=>"ADEGOKE, Bolanle Zipporah",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA021",
"name"=>"ADEJARE, Sulaiman Adeyinka",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA155",
"name"=>"ADEKANYE, Damilola Rolake",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA022",
"name"=>"ADELADAN, Rachael Ajoke",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA023",
"name"=>"ADELEKE, Faizat Temitope",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA024",
"name"=>"ADENIYI, Busayomi Demilade",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA025",
"name"=>"ADENUGA, Oluwafunmibi Adeshola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA157",
"name"=>"ADEOYE, Adeolu Oluwapelumi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA026",
"name"=>"ADEOYE, Iyiola Emmanuel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA027",
"name"=>"ADEROHIN, Titanya Taiye",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA158",
"name"=>"ADESANYA, Abiodun Joshua",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA159",
"name"=>"ADESHINA, Sunday Olumide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA160",
"name"=>"ADESUYI, Oyeyemi Oluwapelumi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA028",
"name"=>"ADETA, Taqwallah Niyi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA161",
"name"=>"ADETOLA, Racheal Adeola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA029",
"name"=>"ADEWUYI, Adesola Pelumi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA030",
"name"=>"ADEYEMI, Pelumi Mayowa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA031",
"name"=>"ADEYEMO, Jesujoba Toluwani",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA030",
"name"=>"ADEYEMO, Tomiwa Shadrach",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA031",
"name"=>"ADEYEYE, Damilola Blessing",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA032",
"name"=>"ADISA, Ibrahim Olanrewaju",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA034",
"name"=>"AFOLABI, Saheed Bolaji",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA162",
"name"=>"AFOLAYAN, Adebola Opeyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA035",
"name"=>"AGORO, Azeez Damilola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"13/66MA028",
"name"=>"AGUN, Blessing Eniola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA036",
"name"=>"AHIPUE, Juliet Onyinyechukwu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA041",
"name"=>"AHMED, Aminat Omowunmi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA037",
"name"=>"AJAO, Temitope Oluwatosin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA234",
"name"=>"AJAYI, Christiana Omolara",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA163",
"name"=>"AJAYI, Shola Ezekiel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA046",
"name"=>"AJAYI, Tomisin Lois",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA038",
"name"=>"AJEKIIGBE, Temiloluwa Motunrayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA039",
"name"=>"AJIBOLA, Michael Oluwafemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA040",
"name"=>"AJIBOWU, Olumide Israel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA164",
"name"=>"AJIBOYE, Jacob Sunday",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA165",
"name"=>"AJISEBIOLOWO, Omotayo Olakunle",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA041",
"name"=>"AKANBI, Bello Kayode",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA054",
"name"=>"AKINBOBOLA, Akinboboye ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA166",
"name"=>"AKINDELE, Mubarak Akinwumi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA043",
"name"=>"AKINOLA, Oluwatosin Ajoke",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA044",
"name"=>"AKINPELU, Khalid Abiodun",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA045",
"name"=>"AKINRINLOLA, Ayodeji Samuel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA046",
"name"=>"AKINYEMI, Samson Ayodele",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA047",
"name"=>"AKOGUN, Kehinde Caroline",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA167",
"name"=>"AKOLAWOLE, Emmanuel Eniitan",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA048",
"name"=>"ALAAYA, Lateef Oladipo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA049",
"name"=>"ALABI, Anuoluwapo Feranmi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA050",
"name"=>"ALABI, Bolatito Mayowa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA051",
"name"=>"ALABI, Mojoyinoluwa Elizabeth",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA052",
"name"=>"ALALADE, Peace Mary",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA168",
"name"=>"ALIU, Rasaq ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA169",
"name"=>"ALIU, Sulyman Faadi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA053",
"name"=>"ALIYU, Ohunene Aminat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA054",
"name"=>"ALIYU, Teslim Enesi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA055",
"name"=>"ALLI, Loveth Oluchukwu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA056",
"name"=>"ALUKO, Ruth Mosieyi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA057",
"name"=>"AMAO, Mutiu Babajide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA170",
"name"=>"AMINDU, Ahmed Ajia",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA058",
"name"=>"ANIYIKAYE, Mayowa Solomon",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA059",
"name"=>"ANJORIN, Ridwan Oluwatimilehin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA060",
"name"=>"ARANSIOLA, Timothy Olamide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA061",
"name"=>"AREMU, Muslim Ayobami",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA062",
"name"=>"ARIYO, Omotayo Muheebart",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA063",
"name"=>"AROYEWUN, Idris Babatunde",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA171",
"name"=>"ASA, Oluwatoyin Opeyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA172",
"name"=>"ASHAOLU, Esther Adebola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA069",
"name"=>"ATOYEBI, Semilore Sunday",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA064",
"name"=>"AUDU, Joseph Olusegun",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA065",
"name"=>"AWOLUMATE, Emmanuel Dara",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"13/66MA042",
"name"=>"AWONIYI, Kehinde Precious",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA070",
"name"=>"AWONIYI, Marvellous Taiwo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA066",
"name"=>"AWOTAYO, Oluwafemi Joshua",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA067",
"name"=>"AYANDURO, Victoria Jesupemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA068",
"name"=>"AYENI, Oluwambepelumi Georgina",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA069",
"name"=>"AYINLA, Lawal Oladimeji",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA071",
"name"=>"AYOOLA, Mariam Olajumoke",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA173",
"name"=>"BABATUNDE, Adedoyin Sulaimon",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA174",
"name"=>"BADMUS, Haishah Omotunde",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA072",
"name"=>"BADMUS, Oluwatoyin Karimat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA073",
"name"=>"BALOGUN, Kabirat Mayowa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA074",
"name"=>"BANJO, Basirat Wuraola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA075",
"name"=>"BANJO, Emmanuel Mojolajesu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA076",
"name"=>"BANKOLE, Oluwatunmife Ruth",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA175",
"name"=>"BELLO, Abdulganiyu Olaniyi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA077",
"name"=>"BELLO, Faith Toluwalashe",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA078",
"name"=>"BOLAKALE, Nifesimi Dorcas",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA080",
"name"=>"DADA, Musibau Olaotan",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA081",
"name"=>"DADA-BASHUA, Oluwatosin Latifat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA092",
"name"=>"DARE, Toluwanimi Michael",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA082",
"name"=>"DAUDA, Ridwan Olalekan",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA083",
"name"=>"DAUDA, Riliwan Opeyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA084",
"name"=>"DURODOLA, Olamide Victoria",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA085",
"name"=>"EBENEZER, Helen Godsgift",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA086",
"name"=>"EKE, Victor Onyekachi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA087",
"name"=>"FADIGA, Aishat ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA088",
"name"=>"FASEYI, Joshua Ayotola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA100",
"name"=>"FOLAMI, Ismail Bolaji",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA176",
"name"=>"FOLORUNSHO, Idris ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA089",
"name"=>"FOYE, Hammed Temitope",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA090",
"name"=>"GBADAMOSI, Mubarak Damilola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA091",
"name"=>"GENE, Amirat ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA092",
"name"=>"HAMEED, Farouk Umar",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA093",
"name"=>"IBEANUSI, Oluchi Andaline",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA104",
"name"=>"IBIYEMI, Emmanuel Oluwatosin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA105",
"name"=>"IBRAHIM, Abdulahi Omowunmi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA178",
"name"=>"IBRAHIM, Omolola Zahrat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA179",
"name"=>"IBRAHIM, Raimot ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA094",
"name"=>"IBRAHIM, Titilayo Sarat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA095",
"name"=>"IHEANACHOR, Adanna Amarachi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA180",
"name"=>"IJAGBEMI, Patience Osekafore",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA096",
"name"=>"ISIAQ, Temitope Sofiat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA181",
"name"=>"JIMOH, Muhammed Olakunle",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA097",
"name"=>"JIMOH, Toheeb Opeyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA098",
"name"=>"JIMSON, Godwin Amoto",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA099",
"name"=>"JOLAYEMI, Elijah Omotayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA100",
"name"=>"KAREEM, Oyinlola Musa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA101",
"name"=>"KAYODE, Ayodamola Nifemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA102",
"name"=>"KEHINDE, Grace Adedoyin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA125",
"name"=>"KEITA, Ibrahim Mohamed",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA103",
"name"=>"KOLAWOLE, Oluwatobi Samuel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA104",
"name"=>"LAMUYE, Rachael Ololade",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA105",
"name"=>"LAWAL, Isaiah Abayomi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA106",
"name"=>"LAWAL, Kuburat Omoniyi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA107",
"name"=>"LAWAL, Olayemi Ismail",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA108",
"name"=>"LAWAL, Rashidat Owunene",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA109",
"name"=>"LAWAL, Sulaiman Adedimeji",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA110",
"name"=>"MAGAJI, Abdulquadir Opeyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA111",
"name"=>"MEJIOSU, Shayo Feyi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA112",
"name"=>"MISSA, Abdullah Olawale",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA182",
"name"=>"MOHAMMED, Abdulrasaq ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA113",
"name"=>"MOHAMMED, Sekinat Ojomah",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA134",
"name"=>"MORAKINYO, Gbemiga Ibiyemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA114",
"name"=>"MUKAILA, Osieba Diana",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA115",
"name"=>"MURAINA, Abibat Omotunde",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA116",
"name"=>"MURAINA, Barakat Motunrayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA183",
"name"=>"MUSA, Saidat Iyabo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA117",
"name"=>"MUSA, Yetunde ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA118",
"name"=>"MUSTAPHA, Sodiq Shola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA119",
"name"=>"NDUBUISI, Ogechi Eunice",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA184",
"name"=>"NDUONYI, Godwin Okon",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA137",
"name"=>"NWACHUKWU, Blessing Gift",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA185",
"name"=>"NWEKE, Chinemelu Pascal",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA120",
"name"=>"OBIDEYI, Omotayo Samuel",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA121",
"name"=>"ODUNUYI, Oyinkansola Mary",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA186",
"name"=>"OGBONOMOWO, Akeem Kolawole",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA122",
"name"=>"OGUNDELE, Emmanuel Abiodun",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA123",
"name"=>"OGUNNIKA, Mary Ayomide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA187",
"name"=>"OGUNNIYI, Oluwatayo Fiyinfolu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA144",
"name"=>"OGUNSANYA, Emmanuel Anu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA188",
"name"=>"OGUNTADE, Opeyemi Zainab",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA145",
"name"=>"OGUNTUNDE, Omolara Christiana",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA189",
"name"=>"OJO, Ayobami Blessing",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA190",
"name"=>"OJO, Oluwafunmilayo Racheal",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA191",
"name"=>"OKAFOR, Ngozi ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA124",
"name"=>"OKE, Oluwayemisi Deborah",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA125",
"name"=>"OKI, Rasheed Ololade",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA126",
"name"=>"OKINO, Halimat Osheiza",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA149",
"name"=>"OKORO, Esther Adaeze",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA151",
"name"=>"OKUNOYE, Emmanuel Damilola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA127",
"name"=>"OLADELE, Isaac Sunday",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA153",
"name"=>"OLADOKUN, Olumide Samson",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA128",
"name"=>"OLAGUNJU, Damilola Amidat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA192",
"name"=>"OLAITAN, Olakunle Joseph",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA129",
"name"=>"OLAJIDE, Suliat Emiola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA130",
"name"=>"OLAJUBU, Bunmi Helen",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA193",
"name"=>"OLALEYE, Oluwapelumi Ayomide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA131",
"name"=>"OLANIYAN, Florence Olayemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA132",
"name"=>"OLANIYI, Mariam Taiwo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA133",
"name"=>"OLATINWO, Lateefat Oluwakemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA194",
"name"=>"OLATOKUNBO, Sheu Ayinde",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA195",
"name"=>"OLATUNJI, Ahmed .",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA196",
"name"=>"OLATUNJI, Aminat Temitope",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA134",
"name"=>"OLAYIMIKA, Opeyemi Blessing",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA165",
"name"=>"OLAYIWOLA, Racheal Titilayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA166",
"name"=>"OLOKODANA, Micheal Adedamola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA135",
"name"=>"OLORI, Rachael Ome",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA136",
"name"=>"OLUFEMI, Oluwaseye Michael",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA197",
"name"=>"OLUKOTUN, Deborah Bisayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA137",
"name"=>"OLUKUNLE, Damilola Clinton",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA169",
"name"=>"OLUMODEJI, Oluwatomilayo ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA173",
"name"=>"OLUWAYIOPESE, Mary Iyanuoluwa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA138",
"name"=>"OMIDELE, Timilehin Elijah",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA139",
"name"=>"OMOTARA, Pelumi Grace",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA176",
"name"=>"OMOTOSHO, Olawale Taofiq",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA140",
"name"=>"ONYEMAKA, Daisy Nkonye",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA198",
"name"=>"OPELOYERU, Rabiu Adisa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA141",
"name"=>"ORIADETU, Husseinat Motunrayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA142",
"name"=>"ORISAJI, Yetunde Lolade",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA199",
"name"=>"OSHODI, Abdulrasaq Ademola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA143",
"name"=>"OSOSANYA, Temiloluwa Oluwatosin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA181",
"name"=>"OTI, Elizabeth ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA144",
"name"=>"OTITODUN, Oluwatofunmi Ayobami",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA145",
"name"=>"OTUBAMBO, Temidayo Ganiyah",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA146",
"name"=>"OWOLABI, Damilola Amos",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA200",
"name"=>"OWONIYI, Christianah Omoyeni",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA182",
"name"=>"OWOYEMI, Abdullahi Olanrewaju",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA147",
"name"=>"OYAYODE, Abiola Martha",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA148",
"name"=>"OYELAMI, Deborah Oluwasemilore",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA201",
"name"=>"OYERINDE, Oluwaseun Olamide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA191",
"name"=>"OYESOMI, Oyejide Kamorudeen",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA150",
"name"=>"PELEMO, Kayode Tomisin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA188",
"name"=>"QUSIM, Adeola Adijat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA151",
"name"=>"RAHEEEM, Bukola Lateefat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA202",
"name"=>"RAHEEM, Maryam Omolabake",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA152",
"name"=>"RAIMI, Moriamo ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA193",
"name"=>"ROBERT, Paul Oladimeji",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA196",
"name"=>"SADIQ, Comfort Ohunene",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA197",
"name"=>"SAHEED, Yusuf Adebayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA154",
"name"=>"SALAM, Ridwan Olola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA155",
"name"=>"SALAU, Olajumoke Maryam",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA203",
"name"=>"SALAUDEEN, Hussenat Fadekemi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA156",
"name"=>"SALAUDEEN, Zubaidah Jubril",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA157",
"name"=>"SALIMON, Daud Olamiposi",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA204",
"name"=>"SALMON, Habeeb Ayinde",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA158",
"name"=>"SANNI, Babatunde David",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA192",
"name"=>"SANNI, Farrouq Korede",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA203",
"name"=>"SEGUN-BUSARI, Aanu-oluwa ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA205",
"name"=>"SENJOBI, Grace Adedoyin",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"13/66MA131",
"name"=>"SHINSHIMA, Maranatha Sekuranen",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA159",
"name"=>"SHITTU, Sadiq ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA206",
"name"=>"SHUAIB, Taofeeq Olatunde",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA207",
"name"=>"SIDEEQ, Kehinde Hoozaine",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA160",
"name"=>"SIMON, Lilian Chigemezu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA208",
"name"=>"SOLIHU, Abdulafeez Olayinka",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA161",
"name"=>"SULAIMAN, Ismail Adedayo",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA162",
"name"=>"SULAIMON, Rofiat Aramide",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA210",
"name"=>"SULYMAN, Abdulmajeed Olayinka",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA163",
"name"=>"TAIWO, Oluwapelumi Habeebat",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA164",
"name"=>"TAIWO, Oluwatoyin Bimpe",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA165",
"name"=>"TAJUDEEN, Fatimah Omotosho",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA166",
"name"=>"TITILOYE, Oluwakemi Esther",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA167",
"name"=>"UMAR, Abdulkabir Adisa",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA213",
"name"=>"UMARU, Victor Aliyu",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"14/66MA215",
"name"=>"WAHAB, Ayomide Olayinka",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA168",
"name"=>"YAKUBU, Saheed Olalekan",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA169",
"name"=>"YINUSA, Sodiq ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA170",
"name"=>"YOROKI, Damilola Jude",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA171",
"name"=>"YUSUF, Mariam Damilola",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA172",
"name"=>"YUSUF, Saadatu ",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"16/66MA209",
"name"=>"YUSUF, Saheed Omotosho",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"15/66MA173",
"name"=>"YUSUF, Taofeeq Olakunle",
"level"=>"400",
"department"=>"ACCOUNTING"
],
[
"matric_no"=>"18/66MC001",
"name"=>"ABDULKAREEM, Muinat Olamide",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC002",
"name"=>"ABDULLAHI, Abdulsalam Alabi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC003",
"name"=>"ABDULLAHI, Nafisat",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC004",
"name"=>"ABDULSALAM, Abdulrasheed Tunde",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC005",
"name"=>"ABDULSALAM, Soliat Titilayo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC006",
"name"=>"ABIOLA, Balikis Atinuke",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC007",
"name"=>"ABUBAKAR, Hauwa Fatima",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC008",
"name"=>"ABUTU, Esther Ebulejonu",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC009",
"name"=>"ADEBISI, Abubakar Lekan",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC010",
"name"=>"ADEBIYI, Temitope Richard",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC011",
"name"=>"ADEBOYE, Boluwatife Esther",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC012",
"name"=>"ADEDAYO, Ruth Chiamaka",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC013",
"name"=>"ADEDEJI, Ajibola Kingsley",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC014",
"name"=>"ADEDIRAN, Blessing Adewumi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC015",
"name"=>"ADEKOYA, Rachael Adeola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC016",
"name"=>"ADEKUNLE, Fathia Olajumoke",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC017",
"name"=>"ADELAJA, Oluwatobi Samuel",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC018",
"name"=>"ADELAKUN, Eniola Saidat",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC019",
"name"=>"ADELEKE, Lateefat Olayemi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC020",
"name"=>"ADELOWO, Segun Joseph",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC021",
"name"=>"ADENIRAN, Farouk Dolapo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC022",
"name"=>"ADENIRAN, Mary",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC023",
"name"=>"ADEOTI, Basil Oluwatoni",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC024",
"name"=>"ADISA, Janet Olanike",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC025",
"name"=>"AFOLABI, Fikayo Olamide",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC026",
"name"=>"AGBETOLA, Maryham Odunola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC027",
"name"=>"AJIDE, Emmanuel Oluwaseyi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC028",
"name"=>"AKANDE, Mayowa Esther",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC029",
"name"=>"AKANJI, Oluwafolakemi Praise",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC030",
"name"=>"AKINLABI, Jamiu Akorede",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC031",
"name"=>"ALABI, Oluwadamilola Elizabeth",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC032",
"name"=>"ALABI, Oreoluwa Ogechi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC033",
"name"=>"ALBERT, Egweye Blessing",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC034",
"name"=>"ALEEM, Ridwanullai Opeyemi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC035",
"name"=>"ALIYU, Fareedah Adebusola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC036",
"name"=>"ALIYU, Mariam Ozohu",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC037",
"name"=>"AMUPITAN, Olasunkanmi Paul",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC038",
"name"=>"ARANSIOLA, Adedeji",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC039",
"name"=>"AREMU, Segun Oluwafemi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC040",
"name"=>"ARIYO, Christopher Opeyemi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC041",
"name"=>"AROWOSAIYE, Olaitan Kausarat",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC042",
"name"=>"AWEDA, Ridwan Olayinka",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC043",
"name"=>"AYEDEBINU, James Adegoke",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC044",
"name"=>"AYINDE, Idris Olawale",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC045",
"name"=>"AYODELE, Mary Pelumi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC046",
"name"=>"AZEEZ, Afeez Olayinka",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC047",
"name"=>"BABATUNDE, Mustakeem Oluwaseyi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC048",
"name"=>"BABATUNDE, Sulaimon Alabi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC049",
"name"=>"BADEJI, Barakat Omolara",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC050",
"name"=>"BALOGUN, Oluwaseun Samuel",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC051",
"name"=>"BALOGUN, Taiwo Mary",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC052",
"name"=>"BAMIDELE, Ayobami Enoch",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC053",
"name"=>"BANKOLE, Feyisayo Esther",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC054",
"name"=>"BELLO, Faith Adedoyin",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC055",
"name"=>"BELLO, Karimat Iyanuoluwa",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC056",
"name"=>"DARAMOLA, Damola Ibrahim",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC057",
"name"=>"DAUD, Abdulmalik Olamilekan",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC058",
"name"=>"DIAKITE, Fatimah",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC059",
"name"=>"DRAMMEH, Mutar Mahammed",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC060",
"name"=>"EBO, Oluwagbemisola Boluwatife",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC061",
"name"=>"EGBONRELU, Theophilus Bode",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC062",
"name"=>"EGBULEM, Pauline Etanami",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC063",
"name"=>"EKUNDAYO, Emmanuel Ademola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC064",
"name"=>"EKUNDAYO, Matthew Oladipupo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC065",
"name"=>"EMMANUEL, Juliet Ogechi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC066",
"name"=>"ESHO, Temiloluwa Mercy",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC067",
"name"=>"FAJUMO, Oluwadara Eniola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC068",
"name"=>"FOLORUNSHO, Wuraola Islamiat",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC069",
"name"=>"GAFAR, Habeeb Akande",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC070",
"name"=>"GANIYY, Rasaq Olanrewaju",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC071",
"name"=>"GBADEYAN, Ayomide Samuel",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC072",
"name"=>"GUEYO, Ntemsi Nelly alvine",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC073",
"name"=>"HASSAN, Ibrahim O",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC074",
"name"=>"IGBONGI, Oche Oche",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC075",
"name"=>"ISHAKA, Mercy Ojotule",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC076",
"name"=>"JAJI, Fatimat Kofoworola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC077",
"name"=>"JAMIU, Sideeqoh Olayinka",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC078",
"name"=>"JIBRIL, Muhammed Olayinka",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC079",
"name"=>"JIDDAH, Maimunah Suleiman",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC080",
"name"=>"JOSEPH, Julianah Eiza",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC081",
"name"=>"KAYODE, Mojeed Olamilekan",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC082",
"name"=>"KOLADE, Precious Oluwatoyin",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC083",
"name"=>"LASISI, Esther Gbemisola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC084",
"name"=>"LAWAL, Habeeb Kayode",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC085",
"name"=>"LAWAL, Rukayat Bolanle",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC086",
"name"=>"MACAULEY, Titilope Ruth",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC087",
"name"=>"MAHMUD, Balikis Oluwaseun",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC088",
"name"=>"MOSEBOLATAN, Suliyat Olamide",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC089",
"name"=>"NASIRU, Abdullahi Oladipupo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC090",
"name"=>"NWANI, Okechukwu",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC091",
"name"=>"OBANIYI, Stephen Oluwapelumi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC092",
"name"=>"ODEWOLE, Temidire David",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC093",
"name"=>"ODUNLAMI, Deborah Adeola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC094",
"name"=>"OGUNDEYI, Sherif Olamilekan",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC095",
"name"=>"OGUNJOBI, Peter Oluwafemi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC096",
"name"=>"OGUNLEYE, Omotoyosi Elizabeth",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC097",
"name"=>"OJUKWU, Amarachukwu Genevieve",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC098",
"name"=>"OKOCHA, Samuel Chukwuka",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC099",
"name"=>"OKORAFO, Success Onyedikachi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC100",
"name"=>"OLADEJO, Janet Ajoke",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC101",
"name"=>"OLAOYE, Pelumi David",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC102",
"name"=>"OLATOYE, Shakirat Damilola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC103",
"name"=>"OLAYIWOLA, Kehinde Hannah",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC104",
"name"=>"OLOGUN, Isaac Abayomi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC105",
"name"=>"OLORUNTOBA, Grace Temitope",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC106",
"name"=>"OLUFADE, Favour Oluwafunmilayo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC107",
"name"=>"OLUSOLA, Blessing Oluwaseun",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC108",
"name"=>"OLUWANISOLA, Lateefat Kemisola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC109",
"name"=>"OLUWATOYIN, Oluwapelumi Elisabeth",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC110",
"name"=>"OMORAKA, Oluwatobi Gabriel",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC111",
"name"=>"ONI, Marvelous Adesoji",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC112",
"name"=>"OPEYEMI, Faruq Adeola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC113",
"name"=>"OREKOYA, Adedayo Oluwapelumi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC114",
"name"=>"OSINLOYE, Oluwayemisi Moradeke",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC115",
"name"=>"OSUMUO, Rosemary Chidinma",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC116",
"name"=>"OYEBAMIJI, Suliyat Odunayo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC117",
"name"=>"PETER, Christian Ayomikun",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC118",
"name"=>"RAJI, Mary Oluwafunmilola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC119",
"name"=>"SADIQ, Aishat Oiza",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC120",
"name"=>"SAHO, Ibrahim Mohammed",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC121",
"name"=>"SANI, Basil Ene-ojo",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC122",
"name"=>"SANNI, Rabiu Bala",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC123",
"name"=>"SANUSI, Aminat Damilola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC124",
"name"=>"SHEIDU, Abiodun Opeyemi",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC125",
"name"=>"SHEWU, Zainab Titilola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC126",
"name"=>"SHOPITAN, Tobiloba Mariam",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC127",
"name"=>"SULAIMAN, Abdulquadri Damola",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC128",
"name"=>"SULAIMON, Abdulquadri Olanrewaju",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC129",
"name"=>"TORU, Hannah Timileyin",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC130",
"name"=>"UZAIGBE, Moses A",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC131",
"name"=>"WILLIAMS, Olamide Blessing",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC132",
"name"=>"WRIGHT, Dorlintha Treasure",
"level"=>"100",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC133",
"name"=>"ADEJUMO, Adewunmi Aishat",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC134",
"name"=>"ADEKANYE, Peter Olumide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC135",
"name"=>"ADESHINA, Barakat Temilade",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC136",
"name"=>"ADEYEMI, Abdulsalam Oladimeji",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC138",
"name"=>"AYOOLA, Simisola Olabisi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC139",
"name"=>"AZEEZ, Basirat Omowunmi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC140",
"name"=>"CHUKWUNONYE, Chidinma Blessing",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC141",
"name"=>"MUHAMMED, Kaosara O",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC142",
"name"=>"OBARO, Dolapo Olarewaju",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC143",
"name"=>"OGUNDELE, Stephanie Toluwanimi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC144",
"name"=>"OGUNFEYIJIMI, Damilola Ayomikun",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC145",
"name"=>"OKUNLOLA, Mayowa Oladimeji",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC146",
"name"=>"OLABIYI, Elizabeth Abiola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC147",
"name"=>"OLAWALE, Samuel Adeshina",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC148",
"name"=>"OLORUKOOBA, Farid Yofola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC149",
"name"=>"OLUDEPO, Olubunmi Precious",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC150",
"name"=>"ORIMOLADE, Funke Elizabeth",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC151",
"name"=>"OYEBOLA, Oyeronke Christiana",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC152",
"name"=>"ROTIMI, Oluwaseun Odunayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC153",
"name"=>"SAMUEL, Timilehin Boluwatife",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC154",
"name"=>"SULAIMON, Ahmed Babatunde",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC155",
"name"=>"UNUANE, Mercy Imade",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC156",
"name"=>"YINUSA, Rofiat Funmilayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MC157",
"name"=>"YUSUFF, Mustapha Olalekan",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC001",
"name"=>"ABDULLAHI, Yusuf Salman",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC002",
"name"=>"ABDULQUADRI, KhadijatOlamide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC003",
"name"=>"ABDULRASAK, OlalekanAmoo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC004",
"name"=>"ABODUNRIN, Jayeola Esther",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC005",
"name"=>"ABOLARIN, Victor Oluwafemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC006",
"name"=>"ABUBAKAR, AbdulkadirOlayiwola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC007",
"name"=>"ADEBAYO, KehindeAbeebat",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC008",
"name"=>"ADEBAYO, Mariam Titilayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC009",
"name"=>"ADEBAYO, Racheal Oluwaferanmi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC010",
"name"=>"ADEBOYE, AbiodunOluwafemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC011",
"name"=>"ADEGBILE, Joseph Abayomi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC012",
"name"=>"ADEKOYA, OluwatobiDamilare",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC013",
"name"=>"ADELANI, RashidatAgbeke",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC014",
"name"=>"ADEOLA, MisturahOmoshalewa",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC015",
"name"=>"ADERIBIGBE, AderemiAdeyemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC016",
"name"=>"ADESANYA, MorufatKemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC017",
"name"=>"ADEWUYI, Sunday Emmanuel",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC018",
"name"=>"ADEYEMI, Gbolahan Joshua",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC019",
"name"=>"ADEYEMO, KhaeratOluwatobi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC020",
"name"=>"ADIGUN, AdeniranMutiu",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC021",
"name"=>"ADIGUN, Ignatius Abiodun",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC022",
"name"=>"AFOLABI, SodiqBabatunde",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC023",
"name"=>"AJANI, John Oluwatobi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC024",
"name"=>"AJAYI, JamiuJimoh",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC025",
"name"=>"AJIBOLA, Oluwayanmito Eunice",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC026",
"name"=>"AJIBOYE, Michael Ayomide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC027",
"name"=>"AJIMOTI, Oluwadara Victoria",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC028",
"name"=>"AKANBI, NafisatAfolashade",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC029",
"name"=>"AKINKITAN, Joel Sunday",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC030",
"name"=>"AKINOLA, Emmanuel Oluwatimilehi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC031",
"name"=>"AKINOLA, JalaaldeenOmodolapo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC032",
"name"=>"AKINOLUYEMI, Abel Akintayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC034",
"name"=>"AKINWALE, RidwanBolaji",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC035",
"name"=>"ALABI, AbdulrahmanOpeyemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC036",
"name"=>"ALABI, HabeebBabatunde",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC037",
"name"=>"ALAGBE, Mercy Titilope",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC038",
"name"=>"ALAGBO, Bashir Ayinde",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC039",
"name"=>"AMMINULLAHI, FathiaOreoluwa",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC040",
"name"=>"ANI, GodswillAkinsheye",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC041",
"name"=>"ARAROMI, Daniel Ebenezer",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC042",
"name"=>"ASAKE, OpeyemiIyanuoluwa",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC044",
"name"=>"AWOYEMI, Ayomide Isaac",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC045",
"name"=>"AYANKEYE, Enoch Oluwafikayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC046",
"name"=>"AZEEZ, FatiuAdeola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC047",
"name"=>"BABAWALE, Opeyemi Emmanuel",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC048",
"name"=>"BAKARE, Surprise Iyiola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC049",
"name"=>"BALA, Francis ",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC050",
"name"=>"BALOGUN, AbiodunToheeb",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC051",
"name"=>"BALOGUN, Dhikrullahi Ghazal",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC052",
"name"=>"BONA, Samuel Aiah",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC054",
"name"=>"DARAMOLA, RamotaOlamide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC055",
"name"=>"DAUD, AminatBukola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC056",
"name"=>"DOGO, Yakubu Jeremiah",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC057",
"name"=>"DOSUNMU, RukayatOlayinka",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC058",
"name"=>"DUNTOYE, Yetunde Theresa",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC059",
"name"=>"EGBERONGBE, MotunrayoAjoke",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC060",
"name"=>"EHONIYOTAN, Samuel Oluwasanmi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC061",
"name"=>"EJIKO, Oluwatobi Michael",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC063",
"name"=>"EMOGABOR, Ejiroghene Faith",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC064",
"name"=>"EWUOSO, IsmoilOriyomi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC065",
"name"=>"FARUQ, AyisatOkikiola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC066",
"name"=>"GBADAMOSI, AdeolaOpeyemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC067",
"name"=>"HASSAN, Musbau B",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC068",
"name"=>"IBRAHIM, Ibrahim Babatunde",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC069",
"name"=>"IBRAHIM, KawtharOluwadamilola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC070",
"name"=>"IBUOYE, Lydia Olaide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC071",
"name"=>"IGE, Tiwalola Sharon",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC072",
"name"=>"IGOH, Godwin Ngbede",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC073",
"name"=>"JACOB, Peter Kayode",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC074",
"name"=>"JAMIU, SikiruKunle",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC075",
"name"=>"JIMOH, Ramah Eniola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC076",
"name"=>"JIMOH, SheriffdeenAdeniyi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC077",
"name"=>"JOHN, Abel Onoru-oyiza",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC078",
"name"=>"JOLAOSHO, Victoria Bisola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC079",
"name"=>"JOSEPH, Samson Omeiza",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC080",
"name"=>"JULIUS, Omolola Deborah",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC081",
"name"=>"KAREEM, RidwanOjo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC082",
"name"=>"KOLAPO, Mariam Olayemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC083",
"name"=>"LAARO, AbdulwarithOlakunle",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC084",
"name"=>"LATEEF, YinkaAbdulrahman",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC085",
"name"=>"LAWAL, LateefatAdenike",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC086",
"name"=>"LOKO, AyomipoSebayi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC087",
"name"=>"MADUBIKE, Praise Nmesoma",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC088",
"name"=>"MICHAEL, Daniel Omuya",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC089",
"name"=>"MOHAMMED, LuqmanKorede",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC090",
"name"=>"MOHAMMED, Mustapha Onimisi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC091",
"name"=>"MOMODU, Mutiu Adebayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC092",
"name"=>"MOSOBALAJE, HabeeblahAbidemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC093",
"name"=>"MUHAMMED, KaosaraObafolahan",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC094",
"name"=>"MUHAMMED, OmobolanleRukayat",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC095",
"name"=>"MUHAMMED, RukayatAbidemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC096",
"name"=>"MUSA, MuhammedFolounsho",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC097",
"name"=>"NDUBUISI, Babara Winner",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC098",
"name"=>"OBILOWO, BolajiRidwan",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC099",
"name"=>"OCHOTUKPO, AuduEloyi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC100",
"name"=>"ODEDOKUN, Blessing Oluwayemisi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC101",
"name"=>"OGUNDIMU, BalikisOlolade",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC102",
"name"=>"OJO, AbosedeAjarah",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC103",
"name"=>"OKONKWO, KehindeIfeoma",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC104",
"name"=>"OKWELUM, AugustinaIfakachukwu",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC105",
"name"=>"OLAJIDE, Precious Ayobami",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC107",
"name"=>"OLANIYAN, Anuoluwa Grace",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC108",
"name"=>"OLATUNJI, AbdulmalikAyomide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC109",
"name"=>"OLATUNJI, SeyiPeteru",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC110",
"name"=>"OLAWALE, MisbaudeenOluwasegun",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC111",
"name"=>"OLAYEMI, OluwaseyiTosin",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC112",
"name"=>"OLOHUNGBEBE, Kehinde Aisha",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC113",
"name"=>"OLOYEDE, KafayatOlabisi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC114",
"name"=>"OLOYEDE, Kayode Alexander",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC115",
"name"=>"OLUEJIRE, Precious ",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC116",
"name"=>"OLUNLOYO, ZainabOlabisi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC117",
"name"=>"OMALI, Patience Matthew",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC118",
"name"=>"OMOTOSHO, Julius Olumide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC119",
"name"=>"OMOTOSHO, QudusOpeyemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC120",
"name"=>"ONIFADE, Taiwo Deborah",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC121",
"name"=>"ONOGWU, Joy Enewa",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC122",
"name"=>"ONOWHAKPOR, Blessing ",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC123",
"name"=>"OPADOKUN, Toluwalope Vincent",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC124",
"name"=>"OPAREMI, Ayomide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC125",
"name"=>"OPELOYERU, ZainabOlamide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC126",
"name"=>"ORILOWO, FuadOlarewaju",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC127",
"name"=>"OSANIPIN, KofoworolaTitilayo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC128",
"name"=>"OSASONA, OloruntobaAyodeji",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC129",
"name"=>"OSSAI, Francis Fredrick",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC130",
"name"=>"OWOLABI, Oreoluwa Elizabeth",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC131",
"name"=>"OWOYEMI, Kehinde Peter",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC132",
"name"=>"OYEKANMI, Oluwagbemiga Thomas",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC133",
"name"=>"OYEWOLE, Mercy Juliana",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC134",
"name"=>"PODEKE, Ditimi Emmanuel",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC135",
"name"=>"RASHEED, KikelomoSherifat",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC136",
"name"=>"SAHEED, LateefatOmowunmi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC138",
"name"=>"SALAUDEEN, Rasheed Adeyemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC139",
"name"=>"SANNI, Gabriel Onechojown",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC140",
"name"=>"SANNI, Khadijat None",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC141",
"name"=>"SHABA, AdebukonlaOlamide",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC142",
"name"=>"SHENENI, Abraham Zhokwo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC143",
"name"=>"SHUAIB, Bashir Ayonitemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC144",
"name"=>"SULYMAN, BalikisOpeyemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC145",
"name"=>"SUNMOLA, Maryam Adeola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC146",
"name"=>"TAFU, Emmanuel Temitope",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC147",
"name"=>"TAOFEEK, SinotaDolapo",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC148",
"name"=>"TOWOLAWI, SimbiatFeyisola",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC149",
"name"=>"UMEAKA, Confidence Ikechukwu",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC150",
"name"=>"YAHAYA, ZulihatOnyioyiza",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC151",
"name"=>"YUSUF, AbiodunOlalekan",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC152",
"name"=>"YUSUF, AishatAbidemi",
"level"=>"200",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC153",
"name"=>"ABBAH, Blessing Faith",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC001",
"name"=>"ABDULAZEEZ, AwawuAjoke",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC002",
"name"=>"ABDULAZEEZ, HalimahOlaitan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC003",
"name"=>"ABDULGANIYU, BaydauOluwatoyin",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC004",
"name"=>"ABDULLAHI, RidwanLekan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC006",
"name"=>"ABDULRAZAQ, Yusuf Taiye",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC007",
"name"=>"ABDULSALAM, AzeezatOlayinka",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC008",
"name"=>"ADANRI, Adeniyi Francis",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC156",
"name"=>"ADARANIJO, RofiatOmobolanle",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC009",
"name"=>"ADEBAYO, OluwatoyinZainab",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC010",
"name"=>"ADEBAYO, Tobiloba Akeem",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC011",
"name"=>"ADEDAYO, Amos Oluwasola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC013",
"name"=>"ADEDOKUN, AdegokeSodiq",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC157",
"name"=>"ADEGBOLA, OluwatobilobaRodiat",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC014",
"name"=>"ADEJI, OluwatomisinGbemisola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC015",
"name"=>"ADEKOYA, BilikisAdekusibe",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC016",
"name"=>"ADELEKE, Oluwapelumi David",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC018",
"name"=>"ADEOTI, Daniel Adedayo",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC019",
"name"=>"ADEOTI, Oluwafunmibi Patience",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC020",
"name"=>"ADERINTO, WareezAkande",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC021",
"name"=>"ADETOYI, Marvellous Jeremiah",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC022",
"name"=>"AFESOJAYE, Oluwatoyin Janet",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC023",
"name"=>"AFOLABI, Boluwatife Solomon",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC024",
"name"=>"AFOLABI, OluwaseunMotunrayo",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC158",
"name"=>"AFOLAYAN, Samson Tomiwa",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC025",
"name"=>"AGUNLOYE, Olugbenga Abraham",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC026",
"name"=>"AHMED, AbubakarGboro",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC027",
"name"=>"AHMED, Adijat",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC159",
"name"=>"AHMED, SodikIshola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC160",
"name"=>"AJAO, MofiyinfoluwaAbiola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC161",
"name"=>"AJAYI, BukolaOmowunmi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC028",
"name"=>"AJIBADE, HabeebOlamilekan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC029",
"name"=>"AJIDE, BoluwatifeBusayo",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC030",
"name"=>"AKANDE-JAJI, MisturahMosunmola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC031",
"name"=>"AKEYO, OluwadamilolaOluwabunmi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC034",
"name"=>"AKOMOLAFE, OlusholaOlawale",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC162",
"name"=>"AKUBO, Martins Friday",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC035",
"name"=>"ALIU, Mustapha Ajani",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC036",
"name"=>"ALIYU, RodiatOlamide",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC037",
"name"=>"ALLI, AkoredeFaruq",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC038",
"name"=>"ALPHONSUS, Nnajike Paul",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC039",
"name"=>"AMOS, IbuoyeOlarewaju",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC040",
"name"=>"ARATUMI, ToheeratOlabisi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC041",
"name"=>"AREMU, OlasunkanmiFarouq",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC163",
"name"=>"AROWOLO, MudashiruAbiodun",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC043",
"name"=>"AUDU, Onyeche Gift",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC164",
"name"=>"AWONIYI, Helen Olubunmi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC044",
"name"=>"AYOADE, AmeenahOlolade",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC046",
"name"=>"AYODELE, Mercy Oluwatobi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC165",
"name"=>"AZUBIKE, Christian Chukwuebuka",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC047",
"name"=>"BABALOLA, AishatOdunola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC049",
"name"=>"BABATUNDE, KazeemOlaitan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC166",
"name"=>"BALOGUN, Ebenezer Oluwadamilare",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC052",
"name"=>"BASHIR, HafusohAdejoke",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC055",
"name"=>"CONDE, Fanta Bintu",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC056",
"name"=>"DADA-BASHUA, Femi Shamsideen",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC057",
"name"=>"DIYAOLU, PhebeanOmolara",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC058",
"name"=>"EJEH, Margaret Eleojo",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC059",
"name"=>"FAGADE, Jamiu Adebayo",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC167",
"name"=>"FAGBAYI, TawaAbosede",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC061",
"name"=>"FATAI, NoimotOlajumoke",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC062",
"name"=>"FAYOYIN, Gabriel Temitope",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC064",
"name"=>"HASSAN, Emmanuel Olabode",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC168",
"name"=>"IBIDUN, SuliyatFolashade",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC065",
"name"=>"IBIWOYE, Tony",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC066",
"name"=>"IBRAHIM, BalikisEniola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC067",
"name"=>"IBRAHIM, MuinatMorenikeji",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC068",
"name"=>"IBRAHIM, OlawaleOlagoke",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC169",
"name"=>"IGBEBI, Abigail Joy",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC069",
"name"=>"IGWEBUIKE, Onyinyechukwu Jane",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC170",
"name"=>"IPADEOLA, Abdulrahman Akin",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC171",
"name"=>"ISIAQ, MonsuratOpeyemi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC070",
"name"=>"ISSA, AbiodunYunus",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC072",
"name"=>"JIMOH, RafiuAyomide",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC073",
"name"=>"JOSEPH, AriyoGamaliel",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC074",
"name"=>"KABA, Fatimah Fanta",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC076",
"name"=>"KAYODE, AnthoniaOmolara",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC077",
"name"=>"KAZEEM, Divine Joshua",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC172",
"name"=>"KEHINDE, Bamise Hezekiah",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC079",
"name"=>"LAWAL, BalikisAbolaji",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC081",
"name"=>"LAWAL, Sanni Dele",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC082",
"name"=>"LESLIE, Emmanuel Ayomide",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC083",
"name"=>"MAKAMA, Muhammad Ahmed",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC084",
"name"=>"MATHEW, Oluwatobi Abraham",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC086",
"name"=>"MUHAMMED, ShadiatAbeni",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC087",
"name"=>"MUKAILA, OlalekanMonsuru",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC088",
"name"=>"MURAINA, QudusOlamide",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC089",
"name"=>"MUSILIUDEEN, KehindeAdeola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC090",
"name"=>"MUSTAPHA, TaofeekatAjoke",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC091",
"name"=>"NKRUMAH, Esther Esi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC092",
"name"=>"OBA, IkimotToyin",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC093",
"name"=>"OBA, Oluwapelumi Daniel",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC174",
"name"=>"OBASESAN, Rebecca Oluwafunmike",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC095",
"name"=>"OGUNDEJI, OluwatosinTemilade",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC096",
"name"=>"OGUNNIYI, SekinatDamilola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC175",
"name"=>"OGWEH, Emeka Pascal",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC176",
"name"=>"OJO, AishatOlayinka",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC099",
"name"=>"OJO, OluwatimilehinOpeyemi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC100",
"name"=>"OJURI, Maria Adeife",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC101",
"name"=>"OKEKE, ArinzeHyncenth",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC103",
"name"=>"OKUNOLA, ZainabDolapo",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC104",
"name"=>"OLABODE, Omolara Esther",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC105",
"name"=>"OLADIMEJI, LukmanOluwatimilehin",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC106",
"name"=>"OLAJIDE, OluwafunmilayoAbosede",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC107",
"name"=>"OLANREWAJU, Dasola Mariam",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC108",
"name"=>"OLATUNJI, Emmanuel Adeshola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC111",
"name"=>"OLORUNMOLU, Sunday Jacob",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC177",
"name"=>"OLOTU, FavourOyindamola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC178",
"name"=>"OLUSHOLA, OluwafunmilayoBukola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC112",
"name"=>"OMUYA, Faith Sefinat",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC113",
"name"=>"ONIFADE, Sarah",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC115",
"name"=>"OWOLABI, RiliwanOlayinka",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC116",
"name"=>"OWOYEMI, Ibrahim Olayinka",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC117",
"name"=>"OYEKALE, Hezekiah Oladimeji",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC118",
"name"=>"OYELEKE, Oluwagbemileke Felix",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC119",
"name"=>"OYEWOLE, IdrisOlanrewaju",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC180",
"name"=>"OYEWOLE, Toluwase Deborah",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC181",
"name"=>"POPOOLA, Halima Opeyemi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC120",
"name"=>"QUADRI, IdrisOlamilekan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC121",
"name"=>"RASAKI, Samuel Olamilekan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC123",
"name"=>"SALAMI, GaniyatOlabisi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC124",
"name"=>"SALAMI, KaosaratOlayemi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC125",
"name"=>"SALAUDEEN, TawakalituAbike",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC126",
"name"=>"SANGOBOWALE, TaiwoRecheal",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC182",
"name"=>"SANNI, Hammed Olayinka",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC128",
"name"=>"SANYAOLU, Cornelius Olamide",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC131",
"name"=>"SHONIBARE, Toluwalope Florence",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC132",
"name"=>"SHUAIB, RaimatYetunde",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC133",
"name"=>"SIMON, Esther Onyeje",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC183",
"name"=>"SOGBANMU, BasheerAkolawole",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC134",
"name"=>"SULAIMON, TeslimahEniola",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC135",
"name"=>"TAIYE, Usman Olarewaju",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC136",
"name"=>"THOMAS, Christiana Oluwapelumi",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC137",
"name"=>"TIAMIYU, OpeyemiBaseet",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC182",
"name"=>"TITILOPE, AbdulrasheedOlalekan",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC184",
"name"=>"TOLORUNLEKE, Samuel Ayobami",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC138",
"name"=>"UDOYE, Christian Uchechukwu",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC139",
"name"=>"UZOR, Oscar Chibueze",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"17/66MC185",
"name"=>"WATCHMAN, Joshua Oyeintari",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC140",
"name"=>"YUSUF, BarakatAmope",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC141",
"name"=>"ZAKARIYAH, RokeebBamidele",
"level"=>"300",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC001",
"name"=>"ABDULFATTAH, Halimat Tosin",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC142",
"name"=>"ABDULKAREEM, Dauda Omotosho",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC143",
"name"=>"ABDULLAHI, Sikirat Oluwatoyin",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC002",
"name"=>"ABDULMALIK, Zainab Tope",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC144",
"name"=>"ABDULRASAQ, Abdullateef Olarewaju",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC003",
"name"=>"ABDULSALAM, Noimot Abimbola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC015",
"name"=>"ABUBAKAR, Ashiru ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC005",
"name"=>"ADEBOYE, Alan Peter",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC007",
"name"=>"ADENIRAN, Favour Adebimpe",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC145",
"name"=>"ADENIYI, Khadijat Bolajoko",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC146",
"name"=>"ADEREMI, Tinuade Moyosore",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC008",
"name"=>"ADEWOYE, Oreoluwa Gift",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC028",
"name"=>"ADEYEMI, Adedapo Muyideen",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC009",
"name"=>"ADEYEMI, Elijah Adekunle",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC147",
"name"=>"ADEYEMI, Olamilekan Olushola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC148",
"name"=>"ADEYEMI, Opeyemi Arinola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC030",
"name"=>"ADEYEMO, Ayodeji Hezekiah",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC010",
"name"=>"AGUN, Ifeoluwa Isreal",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC011",
"name"=>"AINA, Oluwapelumi Dorcas",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC012",
"name"=>"AJIBOWO, Olawale George",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC013",
"name"=>"AKANBI, Olaide Ayomide",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC014",
"name"=>"AKANHO, Jamiu ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC149",
"name"=>"AKERELE, Halimat Toluwalope",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC150",
"name"=>"AKINBAMI, Mary Abimbola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC015",
"name"=>"AKINBOTE, Kofoworola Esther",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC016",
"name"=>"AKPAN, Imoh Richard",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC017",
"name"=>"ALABI, Shukurat Oyindamola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC018",
"name"=>"ALLI, Mayowa Tijani",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC019",
"name"=>"ALUKO, Muhammad ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC021",
"name"=>"AMUDA, Tawakalitu Adeola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC023",
"name"=>"ANJORIN, Ayobami Emmanuel",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC024",
"name"=>"ARANSIOLA, Maria Abiodun",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC025",
"name"=>"ARCHIBONG, Imaikop ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC051",
"name"=>"AREMU, Jacob Akintunde",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC152",
"name"=>"ASUNOMEH, Omosomi Elizabeth",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC153",
"name"=>"AWONUGA, Mariam Abolore",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC027",
"name"=>"AZEEZ, Basirat Adenike",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC028",
"name"=>"AZEEZ, Semirat Oyindamola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC029",
"name"=>"AZEEZADEROGBA, Bakiyat Bolanle",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC030",
"name"=>"BABALOLA, Abiodun Abimbola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC155",
"name"=>"BADMUS, Abdul-azeez Tobilola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC031",
"name"=>"BAKARE, Adedolapo Rodiat",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC067",
"name"=>"BAKARE, Ridwan Temitayo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC156",
"name"=>"BALOGUN, Idris Yinka",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC032",
"name"=>"BALOGUN, Ridwanulahi Adisa",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC157",
"name"=>"BALOGUN, Seyi Stephen",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC158",
"name"=>"BAMIDELE, Dorcas ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC159",
"name"=>"BASHIR, Abdulazeez Kola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC033",
"name"=>"BELLO, Hanat Idowu",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC035",
"name"=>"DADA, Morenike Bolatito",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC160",
"name"=>"DAUDA, Sherif Opeyemi",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC036",
"name"=>"EDEMINAM, Emmanuel Brendan",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC037",
"name"=>"EKERENAM, Victory ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC038",
"name"=>"EMIABATA, Mabel Temitope",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC039",
"name"=>"EYINADE, Ololade Janet",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC040",
"name"=>"FOLARORI, Muiz Alabi",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC041",
"name"=>"FOLAWIYO, Sheriff Olushola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC042",
"name"=>"GANIYU, Segun Temitope",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC084",
"name"=>"GBADAMASI, Abibat Yetunde",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC045",
"name"=>"GBAGBA, Bilkis Moyosore",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC046",
"name"=>"HAMZAT, Habeebulah Ayodele",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC161",
"name"=>"HAMZAT, Rukayat Olamide",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC047",
"name"=>"IDELUOKPEA, Emmanuel Ebosereme",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC049",
"name"=>"IDRIS, Jamiu Kolapo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC050",
"name"=>"IMARUN, Oluwaseun ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC051",
"name"=>"IYANDA, Aishat Olajumoke",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC053",
"name"=>"JACOB, David Oluwasegun",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC162",
"name"=>"JAGUN, Ibraheem Baba",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC163",
"name"=>"JAMIU, Baliqis Ayoku",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC096",
"name"=>"JEROME, Musa Adama",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"13/66MC072",
"name"=>"JIMBA, Tosin Moses",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC054",
"name"=>"JIMBA, Zainab Ajoke",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC056",
"name"=>"JOHN, Eyitayo Victor",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC164",
"name"=>"KAMORUDEEN, Radiat Omotoke",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC165",
"name"=>"KAREEM, Sherif Ayinde",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC166",
"name"=>"KAYODE, Joseph Babatunde",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC057",
"name"=>"KOLADE, Oluwagbemileke Seun",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC112",
"name"=>"KOUYATE, Saramba Sarah",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC059",
"name"=>"KUNLE, Mary Busayo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC167",
"name"=>"LAMILISA, Oyinkansola Kikelomo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC061",
"name"=>"LATEEF, Maryam Temitope",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC105",
"name"=>"MARTINS, Moses Suyamsireni",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC063",
"name"=>"MUSLIU, Toyib Abiodun",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC064",
"name"=>"MUSTAPHA, Abdul-qudus Opeyemi",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC065",
"name"=>"MUSTAPHA, Opeyemi Barakat",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC168",
"name"=>"OBA, Adekunle Oluwaseun",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC169",
"name"=>"ODUNTAN, Quadri Omobolaji",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC067",
"name"=>"ODUSANYA, Damilola Oladipupo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC068",
"name"=>"OGBOLE, Veronica Ehiaghe",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC069",
"name"=>"OGUNDE, Oluwakemi Ibirogba",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC170",
"name"=>"OGUNLEYE, Oluwaseyi David",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC171",
"name"=>"OGUNSOLA, Olansile Azeez",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC126",
"name"=>"OHIARE, Yazid Isah",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC070",
"name"=>"OJEDELE, Gloria Dolapo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC127",
"name"=>"OJEDOKUN, Philips Oluwadolapo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC071",
"name"=>"OJEIKERE, Emmanuel Idialoise",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC072",
"name"=>"OKE, Olawuyi Babasayo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC073",
"name"=>"OKOYE, Chisom Vivian",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC074",
"name"=>"OLABODE, Ayodeji Samuel",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC172",
"name"=>"OLADIMEJI, Mosunmola Bose",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC173",
"name"=>"OLADIPO, Bashirat Olayinka",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC075",
"name"=>"OLAJUMOKE, Tobi Victor",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC076",
"name"=>"OLAOYE, Emmanuel Oyinlola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC140",
"name"=>"OLATUNDE, Shola Richard",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC077",
"name"=>"OLATUNJI, Nathaniel Oluwaseyi",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC078",
"name"=>"OLAYINKA, Oluwatobi Ayodele",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC174",
"name"=>"OLOLADE, Blessing Joy",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC079",
"name"=>"OLONADE, Idris Anuoluwapo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC144",
"name"=>"OLORUNTOBA, Taofeeq Oluwatosin",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC081",
"name"=>"OLUWAFEMI, Tosinmile Demilade",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC082",
"name"=>"OMOIGUI, Osamudiame Daniel",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC149",
"name"=>"OMOLABI, Islamiyat Yetunde",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC175",
"name"=>"OMOLE, Anthony Olushola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC084",
"name"=>"ONADIPE, Modinat Oluwatoyin",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC085",
"name"=>"ONASHILE, Favour Ifedolapo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC086",
"name"=>"ONI, Oluwabukola Feyisara",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC087",
"name"=>"ONUOHA, Chioma Elizabeth",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC088",
"name"=>"ORTESE, Dooyum Debbie",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC089",
"name"=>"OSASONA, Samuel Morayo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC176",
"name"=>"OSENI, Oyinkansola Selimat",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC090",
"name"=>"OSINLOYE, Adegoke Adefolurin",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC091",
"name"=>"OSODE, Iremide Lydia",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC158",
"name"=>"OTOBO, Michael Abah",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC177",
"name"=>"OWOLABI, Olasunkanmi Taofeek",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC178",
"name"=>"OYAWOYE, Oluwaferanmi Olaoluwa",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC162",
"name"=>"OYETUNJI, Philip Oluwapelumi",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC092",
"name"=>"PETER, Elizabeth Ese-ohe",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC093",
"name"=>"POPOOLA, Oluwasegun Gabriel",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC094",
"name"=>"QUADRI, Basirat Abiola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC165",
"name"=>"QUADRI, Rafiat Abeo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC095",
"name"=>"RAHMAN, Damilola Fatiat",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC179",
"name"=>"RUFAI, Morufat Taye",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC096",
"name"=>"SADULLAH, Nofisat Motunrayo",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC097",
"name"=>"SANOH, Fatima Mustapha",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC099",
"name"=>"SMITH, Anretioluwa Serah",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC100",
"name"=>"SODIQ, Ibrahim Olawale",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC101",
"name"=>"SULAIMON, Basirat Olaide",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC180",
"name"=>"SULEIMAN, Habeeb Olayiwola",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC102",
"name"=>"SULEIMAN, Maryam Olamide",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC181",
"name"=>"TINUOYE, David Oladimeji",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC183",
"name"=>"WEMIMO, Okewunmi Deborah",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"16/66MC184",
"name"=>"YINKA, Yakub ",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"15/66MC104",
"name"=>"YUSSUF, Olamilekan Uthman",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"14/66MC184",
"name"=>"YUSUF, Abiodun Babatunde",
"level"=>"400",
"department"=>"BUSINESS ADMINISTRATION"
],
[
"matric_no"=>"18/66MB001",
"name"=>"ABDULGANIYU, Hawau Oyindamola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB002",
"name"=>"ABDULKAREEM, Hikmat Wuraola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB003",
"name"=>"ABDULLAHI, Maryam Yetunde",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB004",
"name"=>"ACHONYE, Blessing Ginikachukwu",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB005",
"name"=>"ADAMA, Peter Adeoluwa",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB006",
"name"=>"ADEBAYO, Ayodeji Matthew",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB007",
"name"=>"ADEKANYE, Ifeoluwa Precious",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB008",
"name"=>"ADEKEYE, Adesoji Isaac",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB009",
"name"=>"ADEKUNLE, Kafayat Oyinkansola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB010",
"name"=>"ADELEKAN, Khadijat Adepeju",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB011",
"name"=>"ADENEKAN, Ajoke Christianah",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB012",
"name"=>"ADENIYI, Johnson Adedolapo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB013",
"name"=>"ADEOYE, Moruf Olalekan",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB014",
"name"=>"ADESHINA, Joseph Oluwasegun",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB015",
"name"=>"ADETONA, Victoria Oluwapamilerin",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB016",
"name"=>"ADEWOYE, Elijah Adedeji",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB017",
"name"=>"ADEYEMI, Temitope Blessing",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB018",
"name"=>"AFOLABI, Omolara Cynthia",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB019",
"name"=>"AFOLAYAN, Grace Oluwakemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB020",
"name"=>"AINA, Omobolanle Olubunmi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB021",
"name"=>"AJENIPA, Mubarak",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB022",
"name"=>"AJIBOLA, Irewole Godwin",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB023",
"name"=>"AJIBOYE, Emmanuel Oluwafemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB024",
"name"=>"AKANDE, Abdulhamid Oyinlola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB025",
"name"=>"AKANDE, Fatimoh Oyindamola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB026",
"name"=>"AKINBAYODE, David Ifedayo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB027",
"name"=>"AKINDE, Ibrahim",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB028",
"name"=>"AKINNUOYE, Similoluwa Samson",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB029",
"name"=>"AKINTADE, Samuel Ayomide",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB030",
"name"=>"AKPA, Deborah Oyilonye",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB031",
"name"=>"ALABI, Abdulkabir Olaniyi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB032",
"name"=>"ALABI, Busayo Comfort",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB033",
"name"=>"ALEMIKA, Fumilayo Roseline",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB034",
"name"=>"ALIYU, Aishah Titilayo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB035",
"name"=>"AMUDA, Damilola Abigail",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB036",
"name"=>"ANWO, Abdulwaheed Babatomiwa",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB037",
"name"=>"APATA, Habeebat Olatundun",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB038",
"name"=>"AREWA, Oluwatomisin Rebecca",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB039",
"name"=>"ARISE, Oluwafumilayo Precious",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB040",
"name"=>"AROMASODUN, Omobolanle Iswat",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB041",
"name"=>"AWOTUNDUN, Suliat Omolara",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB042",
"name"=>"AYENI, Emmanuel Toluwani",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB043",
"name"=>"AYEYEMI, Olajumoke Olanrewaju",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB044",
"name"=>"AYODELE, Deborah Oluwagbemilayo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB045",
"name"=>"AYODELE, Princess Aderemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB046",
"name"=>"AZEEZ, Akeemat Ayandoyin",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB047",
"name"=>"BABALOLA, Fathiat Olawumi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB048",
"name"=>"BADEJO, Miracle Aduragbemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB049",
"name"=>"BADMUS, Ayomide Meshach",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB050",
"name"=>"BALOGUN, Dorcas Oyinkansola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB051",
"name"=>"BAMIDELE, Covenant Kikelomo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB052",
"name"=>"DAIRO, Gbemisola Temiloluwa",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB053",
"name"=>"DANIEL, Ayobami Emmanuel",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB054",
"name"=>"DUROJAYE, Solomon Oluwaferanmi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB055",
"name"=>"EMMANUEL, Gladys Olayemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB056",
"name"=>"ENYA, Michael Ifunanya",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB057",
"name"=>"FAGBEMI, David Oluwatobi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB058",
"name"=>"FARONBI, Damilola Aduragbemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB059",
"name"=>"GANIYU, Kazeem Ayinla",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB060",
"name"=>"GANIYU, Mariam Oluwafeyisayo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB061",
"name"=>"HAMMED, Adams Oladipupo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB062",
"name"=>"HAMZAT, Maryam Temitope",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB063",
"name"=>"IBITOYE, Halleluyah Phoebe",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB064",
"name"=>"IBITOYE, Oyindamola Beatrices",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB065",
"name"=>"IBRAHIM, Amos Andre",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB066",
"name"=>"IBRAHIM, Kehinde Husseinat",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB067",
"name"=>"IBRAHIM, Mushrofat Ashabi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB068",
"name"=>"IBRAHIM, Rodiat Precious",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB069",
"name"=>"IDOWU, Pelumi Bidemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB070",
"name"=>"IDRIS, Bilkisu",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB071",
"name"=>"IMRAN, Yusuf Oyeniyi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB072",
"name"=>"ITOJE, Ayomide Deborah",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB073",
"name"=>"IYAMAH, Destiny Chigozie",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB074",
"name"=>"IYOHA, Endurance Ofure",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB075",
"name"=>"JIMOH, Aishat Ayoola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB076",
"name"=>"JIMOH, Taofeeqat Olamide",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB077",
"name"=>"JOSEPH, Joy Onyinoyi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB078",
"name"=>"JOSHUA, Oluwashola Moses",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB079",
"name"=>"KAREEM, Zainab Omowunnmi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB080",
"name"=>"KOLAWOLE, Kehinde Margret",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB081",
"name"=>"LAWAL, Abdulfatai Opeyemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB082",
"name"=>"LAWAL, Abdulwahab Olamilekan",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB083",
"name"=>"LAWAL, Hikmat Eyitayo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB084",
"name"=>"MEJULE, Tomiwa Toyosi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB085",
"name"=>"MOKOLADE, Victor Olanrewaju",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB086",
"name"=>"MUHAMMED, Jamiu Sodiq",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB087",
"name"=>"MUSA, Abdulqudus Abidemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB088",
"name"=>"NOAH, Khadijat Abiodun",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB089",
"name"=>"NWAKEZE, Michael Kenechukwu",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB090",
"name"=>"NWAOSU, Evelyn Kikelomo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB091",
"name"=>"ODEDE, Abimbola Orezime",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB092",
"name"=>"ODUGBEMI, Temitope Joy-favour",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB093",
"name"=>"OGUNWOLE, Joy Oluwaseun",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB094",
"name"=>"OJO, Ayomide Elizabeth",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB095",
"name"=>"OKAFOR, Clinton Ogonna",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB096",
"name"=>"OKE, Kehinde Vincent",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB097",
"name"=>"OLADEINDE, Nifemi Aishat",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB098",
"name"=>"OLADELE, Yusuf Olayinka",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB099",
"name"=>"OLALERE, Aishat Omolola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB100",
"name"=>"OLANREWAJU, John",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB101",
"name"=>"OLANREWAJU, Mariam Mosunmola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB102",
"name"=>"OLATUNDE, Joshua Oluwasegun",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB103",
"name"=>"OLAWOYIN, Waris Abiola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB104",
"name"=>"OLAYEMI, Pelumi Samson",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB105",
"name"=>"OLAYINKA, Oluwaseun Joseph",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB106",
"name"=>"OLORUNLOGBON, Mercy Adesola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB107",
"name"=>"OLORUNTOBA, Favour Jesutomigbekele",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB108",
"name"=>"OLUSANYA, Memunat Oluwaseyi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB109",
"name"=>"OLUWABUNMI, Tolulope Enitan",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB110",
"name"=>"OPAYEMI, Grace Toluwalope",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB111",
"name"=>"ORE, Aishat Odunnike",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB112",
"name"=>"ORIADE, Stephen Abayomi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB113",
"name"=>"ORJI, Victoria Ebere",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB114",
"name"=>"OSHO, Adenike Eniola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB115",
"name"=>"OSINOWO, Abdul- Rahman",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB116",
"name"=>"OTARO, Olajumoke Simisola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB117",
"name"=>"OWOLABI, Favour Mary",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB118",
"name"=>"OYINLOYE, Pelumi Abosede",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB119",
"name"=>"POPOOLA, Lateefat Oluwadamilola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB120",
"name"=>"RASHEED, Damilola Adediran",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB121",
"name"=>"SAAD, Nusirat Funmilayo",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB122",
"name"=>"SALAMI, Aminat Ayomide",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB123",
"name"=>"SALAMI, Emmanuel Obafemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB124",
"name"=>"SALAUDEEN, Bashirah Boluwatife",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB125",
"name"=>"SALAUDEEN, Saidat Adeola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB126",
"name"=>"SALAWUDEEN, Habeeb Adinoyi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB127",
"name"=>"SANUSI, Joyce Abidemi",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB128",
"name"=>"SHEHU, Destiny Jemila",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB129",
"name"=>"SHODA, Mustapha Abolaji",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB130",
"name"=>"SILLAH, Cessay Zainab",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB131",
"name"=>"SOLARIN, Timothy",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB132",
"name"=>"SUBAIR, Taofeek Tunde",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB133",
"name"=>"SULAIMON, Simbiat Mopelola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB134",
"name"=>"TIJANI, Ridwan Babatunde",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB135",
"name"=>"UJAH, Paul Onojah",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB136",
"name"=>"UMORU, Oluwatobi Abraham",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB137",
"name"=>"USMAN, Sediq Abubakar",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB138",
"name"=>"UTHMAN, Kehinde Muhammed",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB139",
"name"=>"YAHAYA, Halimat Modupe",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB140",
"name"=>"YISAU, Shakirat Eniola",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB141",
"name"=>"YOMI, Esther Taiye",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB142",
"name"=>"YUSUF, Aminat",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB143",
"name"=>"YUSUF, Fatiha Titilope",
"level"=>"100",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB144",
"name"=>"ABATAN, Yemi Samuel",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB145",
"name"=>"ADEBAYO, Baasit Bolaji",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB146",
"name"=>"ADEBAYO, Damilola Mariam",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB147",
"name"=>"ADEBOYE, Oluwatimileyin Blessing",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB148",
"name"=>"AFOLABI, Basit Olalekan",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB149",
"name"=>"AGBOOLA, Usman Olamide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB150",
"name"=>"AKANO, Olaitan Ahmed",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB151",
"name"=>"AREMU, Kehinde Iyanuoluwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB152",
"name"=>"ASHOGBON, Oluwabanke Esther",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB153",
"name"=>"EYO, Blessing Gift",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB154",
"name"=>"IBUKUN, Oluwasegun",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB155",
"name"=>"LAMIDI, Zainab Eniola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB156",
"name"=>"NNAMDI, Chiamaka Nora",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB157",
"name"=>"ODUNLAMI, Fiyinfoluwa Olumide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB158",
"name"=>"OGUNDARE, Abimbola Hannah",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB159",
"name"=>"OKEKE, Prosper Uchenna",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB160",
"name"=>"ONI, Olawale Solomon",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB161",
"name"=>"SHOBAJO, Motunrayo Omotoyosi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"18/66MB162",
"name"=>"YISA, Gafar Opeyemi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB001",
"name"=>"ABDULAZEEZ, Abdulghaffar Amoto",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB002",
"name"=>"ABDULAZEEZ, Kabir Onimisi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB003",
"name"=>"ABDUSSALAM, Maryam Folake",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB004",
"name"=>"ABIDOYE, Oluwasunmisola Favour",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB005",
"name"=>"ABIOYE, Abiola Abosede",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB006",
"name"=>"ADEBAMBO, Abdulwaheed Adedayo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB007",
"name"=>"ADEBAYO, Adijat Ajoke",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB008",
"name"=>"ADEBAYO, Blessing Olawunmi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB009",
"name"=>"ADEBAYO, Oluwatobi Benjamin",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB010",
"name"=>"ADEDEJI, Adedolapo Roqeebat",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB011",
"name"=>"ADEGBOYE, Adetolani Olamide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB012",
"name"=>"ADEGOKE, Paul Opeyemi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB014",
"name"=>"ADEKUNLE, Philip Adeniyi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB015",
"name"=>"ADEKUNLE, Rodiat Adebusola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB016",
"name"=>"ADELOKUN, Timilehin Ayodeji",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB017",
"name"=>"ADEMOLA, Lateef Ayinde",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB018",
"name"=>"ADENIJI, Oluwapelumi Hannah",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB019",
"name"=>"ADENIJI, Victoria Ololade",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB020",
"name"=>"ADEOTI, Oluwaferanmi Abimbola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB021",
"name"=>"ADEOWU, Fatihat Adedunni",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB022",
"name"=>"ADEOYE, Victor Joshua",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB023",
"name"=>"ADERIBIGBE, Suliyat Ajoke",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB024",
"name"=>"ADESHINA, Zainab Olajumoke",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB025",
"name"=>"ADETUNJI, Timothy Damilare",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB026",
"name"=>"ADEWUYI, Justina Moromoke",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB027",
"name"=>"ADEYEMI, Mary Precious",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB028",
"name"=>"ADEYEMI, Praise Ifedun",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB029",
"name"=>"ADUNOLA, Aishat Oladeebo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB030",
"name"=>"AGBOOLA, Victor Ifedayo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB031",
"name"=>"AHMED, Kaothar Omoseni",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB032",
"name"=>"AJIBOLA, Oluwabukunolami Mary",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB033",
"name"=>"AJIBOYE, Goodness Oluwaferanmi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB034",
"name"=>"AKINTOBI, Azeezat Adenike",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB035",
"name"=>"AKINYEMI, Olufikayo Victoria",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB036",
"name"=>"ALAMU, Eunice Ibukunoluwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB037",
"name"=>"AMEH, Abraham ",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB038",
"name"=>"AMOO, Tolani Hameed",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB039",
"name"=>"ANIYIKAYE, Tunmise Iyiola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB040",
"name"=>"APATA, Oluwatofunmi Esther",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB041",
"name"=>"ARANSIOLA, Quam Ajibola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB042",
"name"=>"AREMU, Azeez ",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB043",
"name"=>"AWOLESI, Yetunde Oluwakemi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB044",
"name"=>"AYODEJI, Ruth Anuoluwapo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB045",
"name"=>"BABATUNDE, Peter Bukola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB046",
"name"=>"BADRU, Quadri Olanrewaju",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB047",
"name"=>"BAKARE, Mujeeb Atobijuloluwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB048",
"name"=>"BALOGUN, Grace Adesola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB049",
"name"=>"BALOGUN, Oluwaseun Josephine",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB050",
"name"=>"BAMIDELE, Opeyemi Emmanuel",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB051",
"name"=>"BOLORUNDURO, Praise Adeshina",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB052",
"name"=>"BUKOYE, Usman Tunde",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB053",
"name"=>"CHINWENDU, Emmanuel Uche",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB054",
"name"=>"DADA, Taibat Oluwakemi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB055",
"name"=>"DAGBO-SOLIU, Salamat ",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB056",
"name"=>"DARAMOLA, Kikelomo Faith",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB057",
"name"=>"EIDAH, Priscilla Eiojo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB058",
"name"=>"EKPENDU, Chidiebere Desire",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB059",
"name"=>"EWAJANE, Precious Tinuola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB060",
"name"=>"FADIRAN, Adebimpe Omowunmi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB061",
"name"=>"FOLAJAYE, Abdulrahman Olaitan",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB062",
"name"=>"HAMMED, Qudus ",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB063",
"name"=>"HINMIKAIYE, Daniel Seyi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB064",
"name"=>"HINMIKAIYE, Opeoluwa Louis",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB065",
"name"=>"IFELOUWAPO, Samuel Omotayo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB066",
"name"=>"IGBERASE, Breakthrough Ofure",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB067",
"name"=>"IGE, Olanrewaju Olayinka",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB068",
"name"=>"IKPONMWOSA, Glory Osamudiame",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB069",
"name"=>"IKUBORIJE, Funmilayo Ruth",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB070",
"name"=>"IMAM, Saka Mudashir",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB071",
"name"=>"ISAIAH, Emmanuel Temitope",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB072",
"name"=>"JIDE-KAZEEM, Abdulraheem Olamide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB073",
"name"=>"JIMOH, Lasisi ",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB074",
"name"=>"JIMOH, Sulaimon Adebayo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB075",
"name"=>"JOHN, Justina Ene",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB076",
"name"=>"JONATHAN, Stephen Kyaune",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB077",
"name"=>"JOSEPH, Precious Oreoluwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB078",
"name"=>"JOSHUA, Patience Amaze",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB079",
"name"=>"KAREEM, Oluwakemi Maureen",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB080",
"name"=>"KOLADE, Boluwatife Victoria",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB081",
"name"=>"LAWAL, Habeeb Adelani",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB082",
"name"=>"MAFOLASIRE, Yinka Janet",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB083",
"name"=>"MOHAMMED, Ibrahim Ayomide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB084",
"name"=>"MORENIKEJI, Caleb Oluwajimi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB085",
"name"=>"MOYOSORE, Abdulmujeeb Adesina",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB086",
"name"=>"MUSA, Faruq Ayodeji",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB087",
"name"=>"MUSA-AZEEZ, Fatiah Teniola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB088",
"name"=>"MUSTAPHA-AKANDE, Abdulmatin Ola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB089",
"name"=>"NWANA, Elvis Onyedikachukwu",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB090",
"name"=>"OCHIODE, Faith Ejenwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB091",
"name"=>"OGBECHIE, Emmanuel Chijioke",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB092",
"name"=>"OGUNLEYE, Muyideen Abayomi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB093",
"name"=>"OGUNNIYI, Oluwabusolami Cassand",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB094",
"name"=>"OJIBARA, Farida Omowunmi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB095",
"name"=>"OJUGBELE, Eniola Oluwaseyi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB096",
"name"=>"OKUNADE, Lateefah Busayo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB097",
"name"=>"OLADIMEJI, Oluwaseyi Oluwatobil",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB098",
"name"=>"OLADIPO, Daniel Olawale",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB099",
"name"=>"OLAGOKE, Emmanuel Olamide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB100",
"name"=>"OLALEYE, Aisha Abiola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB101",
"name"=>"OLARENWAJU, Abdul Malik",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB102",
"name"=>"OLORUNDARE, Ibrahim Adeshina",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB103",
"name"=>"OLU-BOLAJI, Elizabeth Ifeoluwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB104",
"name"=>"OLUKOREDE, Rhoda Ojuekanmi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB105",
"name"=>"OLUMESE, Divine Temidire",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB106",
"name"=>"OLUSEGUN, Angel Oluwatomisin",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB107",
"name"=>"OLUWALANA, Kayode Daniel",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB108",
"name"=>"OMOBORIOWO, Testimony ",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB109",
"name"=>"ONWUKWE, Daniel Abiodun",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB110",
"name"=>"OPEYEMI, Dada Temiloluwa",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB111",
"name"=>"ORIADE, Jeremiah Ayanfe",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB112",
"name"=>"OSHOMOBA, Peculiar Oshomogho",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB113",
"name"=>"OWOLABI, Widaad Tolani",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB114",
"name"=>"OWOLOWO, Nafisat Olamide",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB115",
"name"=>"OYEBODE, Olukayode Joseph",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB116",
"name"=>"OYEDELE, Zulaykha Adeola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB117",
"name"=>"OYETUNJI, Mubaraq Sholadoye",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB118",
"name"=>"OYEWOLE, Augustine Oluwatimiley",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB119",
"name"=>"OYEWOLE, Emmanuel Anuoluwapo",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB120",
"name"=>"OYINLOYE, Adebowale Sunday",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB121",
"name"=>"RAHEEM, Rokeeb Olayori",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB122",
"name"=>"RAIYEWA, Deborah Omolola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB124",
"name"=>"RAJI, Michael Onimisi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB125",
"name"=>"REGINALD, Shalom Chisom",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB126",
"name"=>"SALAM, Olaide Karimat",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB127",
"name"=>"SALAMI, Ebunoluwa Grace",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB128",
"name"=>"SALAWUDEEN, Folakemi Mariam",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB129",
"name"=>"SALIFU, Mercy Adikechi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB130",
"name"=>"SANNI, Khadijat Ozohu",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB131",
"name"=>"SANNI, Muhibat Ahu-oiza",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB132",
"name"=>"TAOFEEK, Zainab Oyindamola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB133",
"name"=>"TEJUMADE, Ifeoluwa Faith",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB134",
"name"=>"TINUOYE, Favour Olamoposi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB135",
"name"=>"UMORU, Udukhokhe Victor",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB136",
"name"=>"USMAN, Aminat Oyiza",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB137",
"name"=>"USMAN, Mariam Ayinke",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB138",
"name"=>"WELEBE, Fortune Nesi",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB139",
"name"=>"YUNUS, Nafisat Abisola",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB140",
"name"=>"ZUBAIR, Mujidat Tope",
"level"=>"200",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB001",
"name"=>"ABDULKAREEM, Zaynab Kemisola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB002",
"name"=>"ABDULLAHI, Shukurat Oganija",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB003",
"name"=>"ABDULRAHMAN, Yusuf ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB004",
"name"=>"ABDUSSALAM, Abdullah Akande",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB005",
"name"=>"ABHULIMEN, Raphael Osagie",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB006",
"name"=>"ABOLAJI, Oluwapelumi Esther",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB007",
"name"=>"ABOLARIN, Peace Odunayo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB008",
"name"=>"ABUBAKAR, Abubakar Babatunde",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB141",
"name"=>"ABUBAKAR, Ahmed Hassan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB009",
"name"=>"ADEBANWO, Adegoke Glorious",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB010",
"name"=>"ADEBAYO, Zuliat Opeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB011",
"name"=>"ADEBOWALE, Adenike Fatima",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB012",
"name"=>"ADEBOYE, Deborah Yetunde",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB142",
"name"=>"ADEDEJI, Ezekiel Adewale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB013",
"name"=>"ADEGBE, Quareebat Mojoyinola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB014",
"name"=>"ADELAKUN, Amos Ifeoluwa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB143",
"name"=>"ADELEKE, Comfort Aanuoluwa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB015",
"name"=>"ADENEKAN, Habeeb Tobi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB016",
"name"=>"ADEOTI, Azeez Ademola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB017",
"name"=>"ADERIBIGBE, Shifau Bukola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB018",
"name"=>"ADESOBA, Ruth Titilope",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB019",
"name"=>"ADESUA, David Aduralere",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB020",
"name"=>"ADETUYI, Ann Ololade",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB021",
"name"=>"ADEYEMI, Oluwaseun Mary",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB144",
"name"=>"ADEYEMI, Saidat Adebimpe",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB022",
"name"=>"ADULOJU, John Olayinka",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB023",
"name"=>"AFOLABI, Shakir Adesanu",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB024",
"name"=>"AFOLABI, Tolulope Janet",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB026",
"name"=>"AGBELUSI, Olamilekan Ebenezer",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB027",
"name"=>"AHMAD, Zaynab Oluwakemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB028",
"name"=>"AHMED, Abubakar Buba",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB029",
"name"=>"AINA, Onaopemipo Abigel",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB030",
"name"=>"AJAYI, Grace Ohunene",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB032",
"name"=>"AJIBOLA, David Oluwaseun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB033",
"name"=>"AJIBOLA, Zainab Omodasola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB034",
"name"=>"AJIBOSIN, Habib Adebowale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB035",
"name"=>"AJIBOWU, Kaothar Ibukunoluwa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB036",
"name"=>"AJIBOYE, Damilola Omotoyosi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB037",
"name"=>"AJISAFE, Fiyinfunoluwa Florence",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB145",
"name"=>"AKANDE, Hikmot Adepeju",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB038",
"name"=>"AKANDE, Suliyat Bolanle",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB039",
"name"=>"AKINDEKO, Feyisayo Tolulola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB040",
"name"=>"AKINDELE, Gideon Titilope",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB146",
"name"=>"AKINOLA, Rukayat Oluwabunmi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB041",
"name"=>"AKINTAYO, Faruq Abbey",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB042",
"name"=>"AKINWOLE, Oluwaseun Mary",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB043",
"name"=>"ALABI, Jamaldeen Olatilewa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB044",
"name"=>"ALABI, Oluwabunmi Abosede",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB045",
"name"=>"ALABI, Rasheed Olanrewaju",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB046",
"name"=>"ALASINRIN, Sofiu ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB047",
"name"=>"ALIU, John ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB147",
"name"=>"AMUDA, Ganiyat Jumoke",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB049",
"name"=>"AMUJO, Adedamola Praises",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB050",
"name"=>"AMUSA, Oyindamola Ifeoluwa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB051",
"name"=>"ANIOBI, Gloria Nnenna",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB052",
"name"=>"APALARA, Oluwasogo Adeola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB053",
"name"=>"AREMU, Fasilat Bukola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB054",
"name"=>"AREMU, Zubair Oladimeji",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB055",
"name"=>"ARO, Teslimot Mogbonjubola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB056",
"name"=>"AROMARADU, Aishat Soliu",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB057",
"name"=>"ATOBARU, Ahmed Adeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB058",
"name"=>"ATOLAGBE, Abolaji James",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB061",
"name"=>"AWOMOKUN, Tolulope Victoria",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB062",
"name"=>"AWONIYI, Oluwapelumi James",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB063",
"name"=>"AYANTOYE, Omobukunola Temitope",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB064",
"name"=>"AYOKU, Abeeb Olayinka",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB065",
"name"=>"AZEEZ, Hadizat Damilola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB066",
"name"=>"BALOGUN, Hammed Oluwatobi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB067",
"name"=>"BALOGUN, James Oladapo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB068",
"name"=>"BALOGUN, Sheriff Olalekan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB069",
"name"=>"BAMIDELE, Oluwaseyi Olaniyi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB070",
"name"=>"BAMISILE, Phebe Anuoluwapo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB148",
"name"=>"BELLO, Lukman Olamilekan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB071",
"name"=>"BELLO, Sheriff Olaide",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB072",
"name"=>"DADA, Blessing Tolulope",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB074",
"name"=>"DUNMOYE, Habib Olasunkanmi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB075",
"name"=>"EDU, Kazeem Adewale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB076",
"name"=>"ELEDIKO, Zainab Ayoni",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB077",
"name"=>"ELELU, Abdullah Ibrahim",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB079",
"name"=>"FAGBAMILA, Soliat Oyindamola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB080",
"name"=>"FAKAYODE, Sodiq Adewale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB149",
"name"=>"FASASI, Isah Damilare",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB081",
"name"=>"GBADABO, Princess Aderonke",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB082",
"name"=>"GBADEYAN, Taiwo Racheal",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB083",
"name"=>"GOBIR, Oseni Aliyu",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB085",
"name"=>"HASSAN, Khaleedat Omogbolade",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB086",
"name"=>"HASSAN, Raimot Opeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB087",
"name"=>"IBIRONGBE, Esther Taiwo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB088",
"name"=>"IBIYEMI, Bello Abdulquadri",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB089",
"name"=>"IBRAHIM, Abdulbasit Olalekan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB090",
"name"=>"IBRAHIM, Babangida ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB091",
"name"=>"IBRAHIM, Qazeem Opeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB092",
"name"=>"IBUOYE, Damilola Victoria",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB093",
"name"=>"IHEKIRE, Ugochi Jane",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB095",
"name"=>"ISSA, Khadijat ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB096",
"name"=>"ISSA, Sodiq Olawale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB097",
"name"=>"IYAPO, Oluwafolakemi Helen",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB098",
"name"=>"JIMOH, Damilola Jafar",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB099",
"name"=>"JIMOH, Hassan Olamide",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB100",
"name"=>"JIMOH, Opeyemi Kafayat",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB150",
"name"=>"JIMOH, Opeyemi Quadri",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB151",
"name"=>"JUNAID, Emmanuel  Oluwaseun ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB101",
"name"=>"KEHINDE, Alice Bukola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB152",
"name"=>"LAMIDI, Olayinka Ahmed",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB103",
"name"=>"LAWAL, Abdulsamad Ayomide",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB104",
"name"=>"MAYE, Habeebat Omogbenga",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB105",
"name"=>"MEJIOSU, Hawau Biodun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB106",
"name"=>"MICHAEL, Roseline Temitayo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB107",
"name"=>"MOHAMMED, Ameedat Opeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB109",
"name"=>"MUHAMMED-BELLO, Mariam Olayinka",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB153",
"name"=>"MUKADAM, Shukurah Bukola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB111",
"name"=>"MUSA, Aminu Olaitan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB112",
"name"=>"MUSIBAU, Suraju Babatunde",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB113",
"name"=>"MUSTAPHA, Hauwa Omotolani",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB114",
"name"=>"NAGERI, Aishat Oluwakemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB115",
"name"=>"NUHU, Suleiman ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB116",
"name"=>"NURUDEEN, Ibrahim Olamilekan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB117",
"name"=>"ODEJOBI, Waliyat Oluwadamilola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB118",
"name"=>"ODENIRAN, Benjamen Adebayo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB119",
"name"=>"OGUNBEWON, Precious Oluwapelumi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB121",
"name"=>"OGUNNIGBO, Emmanuel Seun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB122",
"name"=>"OHUU, Oladimeji Adebowale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB123",
"name"=>"OJO, Oluwatosin Damilola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB154",
"name"=>"OKE, David Oladimeji",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB124",
"name"=>"OKE, Raliat Kofoworola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB155",
"name"=>"OKOLIE, Winifred Ifechukwude",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB125",
"name"=>"OLABANJI, Aisha Olaitan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB127",
"name"=>"OLALEKE, Peace Olawale",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB128",
"name"=>"OLANREWAJU, Haadi ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB129",
"name"=>"OLANREWAJU, John Olamilekan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB130",
"name"=>"OLAOTI, Olaoluwa Oluwaseun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB131",
"name"=>"OLATEJU, Samson Segun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB132",
"name"=>"OLATUNDE, Ridwan Oyeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB133",
"name"=>"OLAWOYIN, Funmilola Ayobami",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB156",
"name"=>"OLAYEMI, Omolola Victoria",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB134",
"name"=>"OLAYINKA, Saheed ",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB135",
"name"=>"OLAYIWOLA, Olayinka Olabisi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB136",
"name"=>"OLOFINLADE, Paul Seun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB137",
"name"=>"OLOKO, Basirat Damilola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB138",
"name"=>"OLONINAYIN, John Babatunde",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB139",
"name"=>"OLOYE, Semiat Abisola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB140",
"name"=>"OLUSEYE, Shukurat Omotoyosi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB141",
"name"=>"OMOLODUN, Afolahun Oluwatomiwa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB142",
"name"=>"OMONIYI, Elizabeth Ololade",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB143",
"name"=>"OMONIYI, Promise Moshopefoluwa",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB144",
"name"=>"OMOTINUGBON, Bisola Janet",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB145",
"name"=>"OMOTOSHO, Yunusa Olushola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB146",
"name"=>"ONOFUA, Patricia Eni-ibukun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB147",
"name"=>"OPAKUNLE, Zainab Damilola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB148",
"name"=>"OPESEYITAN, Samuel Damilare",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB149",
"name"=>"ORIAJE, Olaronke Wasilat",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB151",
"name"=>"OSENI, Abdul-hazeem Temilade",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB153",
"name"=>"OYEBODE, Ameedat Funmilayo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB154",
"name"=>"OYEDOKUN, Michael Timileyin",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB155",
"name"=>"OYEWO, Israel Damilola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB156",
"name"=>"RAJI, Mubaarak Abiodun",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB157",
"name"=>"ROTIMI, Oluwatoyin Oluwanifemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB158",
"name"=>"SADIQ, Olayinka Ajibike",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB160",
"name"=>"SALAUDEEN, Fasilat Olaiya",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB161",
"name"=>"SALAUDEEN, Islamiyat Monisola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB162",
"name"=>"SAMUEL, Abidemi Mary",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB163",
"name"=>"SANNI, Abdulrasheed Opeyemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB164",
"name"=>"SANNI, Ganiu Abidemi",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB165",
"name"=>"SANUSI, Nafisat Olamide",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB166",
"name"=>"SANYAOLU, Yetunde Zainab",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB167",
"name"=>"SARUMI, Faridah Doyinsola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB168",
"name"=>"SARUMI, Fatimah Arolayo",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB169",
"name"=>"SHEU, Ahmad Onimago",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB170",
"name"=>"SHOLANKE, Oluwatobi Lekan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB171",
"name"=>"SODIQ, Abdulquadir Taiye",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB172",
"name"=>"SOLAJA, Taiwo Omolola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB173",
"name"=>"SULAIMAN, Zainab Alake",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB174",
"name"=>"SULU, Uthman Omotosho",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB157",
"name"=>"SYLVESTER, Samuel Oluwatunmise",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB176",
"name"=>"TAHIR, Anas Unekwu",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB177",
"name"=>"TAIWO, Oluwapelumi Victoria",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB178",
"name"=>"TEJUMOLA, Bukunmi Olamide",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB179",
"name"=>"TIJANI, Yetunde Aminat",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB180",
"name"=>"TITILOYE, Oluwatimilehin Emmanuel",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB181",
"name"=>"WILLIAMS, Nancy Ebere",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB182",
"name"=>"YAHAYA, Dauda Kolade",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB183",
"name"=>"YAQUB, Misturah Eniola",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"17/66MB158",
"name"=>"YUSUF, Adigun  Ridwan",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB184",
"name"=>"YUSUF, Lateefat Ajike",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB185",
"name"=>"YUSUF, Taiwo Rofiat",
"level"=>"300",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB002",
"name"=>"ABDUL, Olugbenga Joshua",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB003",
"name"=>"ABDULGAFAR, Aishat Oluwaseyi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB005",
"name"=>"ABDULLAHI, Ayobami Omobolanle",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB007",
"name"=>"ABDULLAHI, Mubarak Arisekola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB008",
"name"=>"ABDULLAHI, Murshidat Damilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB007",
"name"=>"ABDULLATEEF, Ibrahim Baye",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB009",
"name"=>"ABDULLATEEF, Mariam ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB010",
"name"=>"ABDULRAHMAN, Zainab Ajoke",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB186",
"name"=>"ABDULRASAQ, Bashirat Bolatito",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB011",
"name"=>"ABDULRASHEED, Basheerat Omowunmi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB185",
"name"=>"ABDULRAZAQ, Ganiyat Bukola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB012",
"name"=>"ABDULSALAM, Lukman Opeyemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB013",
"name"=>"ABDULWAHAB, Arafat Alao",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB014",
"name"=>"ABIDOYE, Yetunde Oluwatobi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB015",
"name"=>"ABIKOYE, Adenike Agnes",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB009",
"name"=>"ABIKOYE, Opeyemi John",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB012",
"name"=>"ABIOYE, Lateef Olasubomi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB187",
"name"=>"ABODERIN, Gabriel Oluwafemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB016",
"name"=>"ABUBAKAR, Aminat Nana",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB016",
"name"=>"ADEBAYO, Ajoke Abimbola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB188",
"name"=>"ADEBISI, Sodiq Olawale",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB189",
"name"=>"ADEBIYI, Luqman Adetunji",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB190",
"name"=>"ADEBIYI, Matthew Folusho",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB017",
"name"=>"ADEBO, Abiola Olayinka",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"13/66MB011",
"name"=>"ADEGBITE, Olufemi Solomon",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB018",
"name"=>"ADEGUN, Timileyin Racheal",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB019",
"name"=>"ADEKANLE, Abiodun Andrew",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB020",
"name"=>"ADEKEYE, Adedotun Faruq",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB021",
"name"=>"ADELEKE, Adewale Michael",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB022",
"name"=>"ADELEKE, Victor Oreoluwa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB023",
"name"=>"ADENIRAN, Hammed Adewole",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB024",
"name"=>"ADENIYI, Muhammed Damilare",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB025",
"name"=>"ADEOTI, Fiyinfoluwa Elizabeth",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB026",
"name"=>"ADEOTI, Samuel Segun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB027",
"name"=>"ADEOTI, Uthman Olabode",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB028",
"name"=>"ADEOYE, Mariam Adenike",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB191",
"name"=>"ADEWUMI, Oluwaseye Ademola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB188",
"name"=>"ADIGUN, Favour Jesudamilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB039",
"name"=>"ADIO, Hussein Tolani",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB192",
"name"=>"AFOLABI, Ganiyat ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB030",
"name"=>"AFOLABI, Haneezat Oyindamola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB031",
"name"=>"AFOLAYAN, Abayomi Muiz",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB193",
"name"=>"AGBAJE, Aishat Titilope",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB194",
"name"=>"AGBARIGIDOMA, Abubakar Olamilekan",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB032",
"name"=>"AGBOOLA, Faruq Olayinka",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB189",
"name"=>"AGUNBIADE, Bisola Folake",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB033",
"name"=>"AHMED, Nasiru Dare",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB046",
"name"=>"AHMED, Yusuf Aliyu",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB034",
"name"=>"AINA, Dorcas Motunrayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB035",
"name"=>"AJAO, Bashir Kayode",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB036",
"name"=>"AJIBOYE, Olubanke Gold",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB037",
"name"=>"AJIJOLAKEWU, Sulaiman Aremu",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB038",
"name"=>"AKANJI, Abdulkudus Babatunde",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB039",
"name"=>"AKANJI, Oluwapelumi Emmanuel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB040",
"name"=>"AKANNI, Emmanuel Tolani",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB041",
"name"=>"AKANNI, Ikram Oluwatobi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB042",
"name"=>"AKINDEIN, Akinwunmi Olaoluwa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB051",
"name"=>"AKINPELU, Lukmon Ajibade",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB053",
"name"=>"AKINTOYE, Dolapo Olufunke",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB191",
"name"=>"AKINWANDE, Faith Olawunmi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB043",
"name"=>"AKINYEMI, Oluwaseyi Olaitan",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB054",
"name"=>"AKOR, Grace Mwaaga",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB195",
"name"=>"AKOTONOU, Toyin Viviane",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB045",
"name"=>"ALABI, Abdulquadri ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB046",
"name"=>"ALABI, Hadizat Omobolanle",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB056",
"name"=>"ALAO, Balikis Oluwabukola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB047",
"name"=>"ALAO, Muhammed ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB048",
"name"=>"ALIMI, Maryam Omodolapo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB049",
"name"=>"ALIU, Raheemat Motunrayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB058",
"name"=>"ALIYU, Fadheelah ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB051",
"name"=>"ALONGE, Maureen Olusola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB052",
"name"=>"AMASA, Aishat Niniola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB053",
"name"=>"AMIDU, Muyideen Atanda",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB054",
"name"=>"ANIFOWOSHE, Kamoru Akorede",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB193",
"name"=>"AREMU, Yusuf ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB055",
"name"=>"ARIJE, Iraadat Opeyemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB064",
"name"=>"AROGUNDADE, Olaide Ayishat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB066",
"name"=>"ATANDA, Azeezat Abimbola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB196",
"name"=>"ATOYEBI, Abisola Faidat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB056",
"name"=>"AWEDA, Ismaila Oluwaseun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB069",
"name"=>"AWOLOWO, Oladayo Samson",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB058",
"name"=>"AWOPONLE, Emmanuel Oloyede",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB071",
"name"=>"AYODEJI, Samuel ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB072",
"name"=>"AYODELE, Folasade Funso",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB060",
"name"=>"BABAWALE, Solomon Oluwatosin",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB078",
"name"=>"BALOGUN, Abdulmutalib Olakunle",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB197",
"name"=>"BALOGUN, Ariyo Uthman",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB063",
"name"=>"BALOGUN, Sofiat Oluwadamilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB064",
"name"=>"BAMIDELE, Damilola Janet",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB065",
"name"=>"BELLO, Faith Damilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB066",
"name"=>"BELLO, Farouk Damilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB088",
"name"=>"BELLO, Olanrewaju Stephen",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB067",
"name"=>"BELLO, Suleiman ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB068",
"name"=>"BIOBAKU, Yusuf Babatunde",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB069",
"name"=>"BUHARI, Aminat Modupe",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB198",
"name"=>"DADA, Oluwafunsho Daniel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB070",
"name"=>"DARE, Emmanuel Oluwagbenga",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB093",
"name"=>"DIJI, Olamide Samuel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB072",
"name"=>"DURODOLA, Morenikeji Rafat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB075",
"name"=>"ELEGBEDE, Zainab Adunni",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB076",
"name"=>"ELEJA, Faizol Muhammed",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB077",
"name"=>"ELEMOSHO, Muhammed Abiodun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB096",
"name"=>"ELETU, Lolade Tijani",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB078",
"name"=>"ELEYINMI, Michael Mayowa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB079",
"name"=>"EMIOLA, Yusuf Alabi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB080",
"name"=>"FALADE, Pelumi Alex",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB081",
"name"=>"FANIKU, Oluwatobi Samuel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB082",
"name"=>"FATIGUN, Subomi Emmanuel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB197",
"name"=>"GABRIEL, Nosayaba Emmanuel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB083",
"name"=>"GBADAMOSI, Habeeb Odunayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB084",
"name"=>"GBALA, Olamide Bernard",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB101",
"name"=>"GIDADO, Damilola Sidikat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB085",
"name"=>"HABEEB, Busari Onimago",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB199",
"name"=>"HABIB, Saadat Dupe",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB086",
"name"=>"HAMMED, Bolaji Mojeed",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB087",
"name"=>"HASSIF, Mariam Aramide",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB088",
"name"=>"IBIKUNLE, Mary Damilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB104",
"name"=>"IBIYEMI, Oluwaseyi Mathew",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB089",
"name"=>"IBRAHIM, Mubaraq Abdulrahman",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB090",
"name"=>"IDOWU, Christiana Ebunoluwa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB091",
"name"=>"IDOWU, Shakiru Olamilekan",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB092",
"name"=>"IDRIS, Adijat Oluwakemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB200",
"name"=>"IDRIS, Ganiyat Titilayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB093",
"name"=>"IDRIS, Taiwo Oladipupo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB094",
"name"=>"IDRIS, Yusuf Tosin",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB095",
"name"=>"IFEKOYA, Kanyinsola Damilola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB096",
"name"=>"IJAIYA, Maryam Ololade",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB097",
"name"=>"IKEJIOFOR, Peter Mmaduabuchukwu",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB198",
"name"=>"ILYAS, Ismail ",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB199",
"name"=>"IPADEOLA, Sodiq Adeola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB098",
"name"=>"ISHOLA, Oluwadamilola Noilat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB099",
"name"=>"ISSA, Dauda Bolakale",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB100",
"name"=>"JAJI, Abdulwahab Temitope",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB113",
"name"=>"JATTO, Awau Osheiza",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB102",
"name"=>"JIMOH, Florence Yetunde",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB103",
"name"=>"JIMOH, Habeeb Olatunde",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB201",
"name"=>"JOSHUA, Isaiah Omeiza",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB202",
"name"=>"JOSHUA, Sunday Elijah",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB105",
"name"=>"KAMALDEEN, Abdulkareem Abiodun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB201",
"name"=>"KAZEEM, Taiwo Adeshina",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB107",
"name"=>"KAZEEM, Taofeek Olalekan",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB109",
"name"=>"KOLAWOLE, Israel Opeyemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB110",
"name"=>"KOLAWOLE, Sheriff Bidemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB111",
"name"=>"KUSHIMO, Olajumoke Ifeoluwa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB112",
"name"=>"LATINWO, Semirat Remilekun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB113",
"name"=>"LAWAL, Azeezat Bisola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB114",
"name"=>"LAWAL, Hakeem Kola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB115",
"name"=>"LAWAL, Islamiyat Adedamola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB116",
"name"=>"LAWAL, Ridwan Omolosho",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB125",
"name"=>"LAWAL, Taofeek Mohammad",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB117",
"name"=>"MAHMUD, Abdulqodri Adebayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB118",
"name"=>"MAKINDE, Hassan Omotola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB203",
"name"=>"MOYOSORE, Yasirat Ashabi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB121",
"name"=>"MURITALA, Fatimat Bukola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB123",
"name"=>"MUSTAPHA, Aminat Omowonuola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB125",
"name"=>"NWAOGU, Doreen Chiamaka",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB139",
"name"=>"ODEDE, Akeem Ishola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB126",
"name"=>"ODEKUNLE, Olalekan Joseph",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB127",
"name"=>"ODELEYE, Kifayat Olaiya",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB208",
"name"=>"ODUKOYA, Quadri Ademola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB128",
"name"=>"ODUTAYO, Adewole Ayomide",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB142",
"name"=>"OFFOGA, Onoshoreame Joy",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB143",
"name"=>"OGUNBIYI, Balikis Olayinka",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB147",
"name"=>"OGUNWALE, Lawal Babatunde",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB129",
"name"=>"OJO, Deborah Adesewa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB154",
"name"=>"OKE, Ayodeji Olawale",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB155",
"name"=>"OKUNNUGA, Oyabanjo Anthony",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB130",
"name"=>"OLADIRAN, Grace Oladoyin",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB157",
"name"=>"OLAFIOYE, Olagoke Olalekan",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB161",
"name"=>"OLANIYI, Abdullah Aremu",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB131",
"name"=>"OLANIYI, Faruq Babajide",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB132",
"name"=>"OLARINDE, Aisha Olaitan",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB133",
"name"=>"OLAWALE, Rasaq Ademola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB134",
"name"=>"OLAWUYI, Damilola Aminat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB135",
"name"=>"OLAWUYI, Oluwadamilare Peter",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB136",
"name"=>"OLAYEMI, Monsurat Omowunmi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB164",
"name"=>"OLAYEMI, Opeyemi Oladimeji",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB137",
"name"=>"OLAYIWOLA, Adeola Azeezat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB204",
"name"=>"OLOGBONJAIYE, Michael Adebowale",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB138",
"name"=>"OLOJEDE, Oluwakemi Deborah",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB139",
"name"=>"OLOOLA, Omodunni Inioluwa",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB140",
"name"=>"OLOWOOKERE, Temitope Olabisi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB141",
"name"=>"OLUFEMI, Ayotomiwa Simeon",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB142",
"name"=>"OLUKADE, Muzzammil Adebayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB143",
"name"=>"OLUROTIMI, Faith Oluwabukola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB171",
"name"=>"OMONIYI, Damilola Michael",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB144",
"name"=>"OMOTUNDE, Segun Stephen",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB145",
"name"=>"ONOTU, Ruth Idowu",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB172",
"name"=>"OPAKUNLE, Ayobami Boluwatife",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB146",
"name"=>"OTOLORIN, Oluwakemisola Abisola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB147",
"name"=>"OWOADE, Arafat Omolola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB148",
"name"=>"OWOLABI, Esther Oluwaseun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB175",
"name"=>"OYEDIRAN, Aisat Motunrayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB149",
"name"=>"OYEDOKUN, Paul Aduragbemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB150",
"name"=>"OYEKALE, Sharon Eniola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB151",
"name"=>"OYELOWO, Oluwatosin Dorcas",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB176",
"name"=>"OYENIYI, Damilola Comfort",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB152",
"name"=>"OYESIJI, Bisola Mercy",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB153",
"name"=>"OYEWOLE, Oluwakemi Adebimpe",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB155",
"name"=>"RAJI, Adedolapo Muhammed",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB156",
"name"=>"RAJI, Nafisat Remilekun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB157",
"name"=>"RAJI, Yusuf Olanrewaju",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB158",
"name"=>"ROTIMI, Ayodeji Moses",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB205",
"name"=>"SAADUDEEN, Jamiu M",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB159",
"name"=>"SALAKO, Michael Adesola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB160",
"name"=>"SALAKO, Temilade Adeola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB161",
"name"=>"SALAMI, Barakat Labake",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB162",
"name"=>"SALAMI, Olamide Shakirat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB163",
"name"=>"SALAUDEEN, Raheemat Olajumoke",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB212",
"name"=>"SALIMON, Sodiq Iyanda",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB206",
"name"=>"SALMAN, Abdulwaheed Opeyemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB164",
"name"=>"SAMUEL, Ben Paul",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB165",
"name"=>"SANUSI, Sultan Olajuwon",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB187",
"name"=>"SEKONI, Boluwatife Adekemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB167",
"name"=>"SHITTU, Muftiat Oriyomi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB168",
"name"=>"SHITTU, Opeyemi Simisola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB169",
"name"=>"SHOLEYE, Temitope Dorcas",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB170",
"name"=>"SOLARIN, Elizabeth Abimbola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB207",
"name"=>"SOLIU, Yetunde Selimat",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB171",
"name"=>"SULAIMAN, Sidikat Funmi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB172",
"name"=>"SULE, Idris Oluwafemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB208",
"name"=>"SULYMAN, Sherifat Toyin",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB173",
"name"=>"TAIWO, Olutola Daniel",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB174",
"name"=>"THOMAS, Omolola Elizabeth",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB175",
"name"=>"TIAMIYU, Damilola Mubarak",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB176",
"name"=>"TIJANI, Kafayat Omolayo",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB213",
"name"=>"TIJANI, Taofiqat Morenike",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB177",
"name"=>"USMAN, Aishat Oluwatoyin",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB178",
"name"=>"USMAN, Suliat Abiodun",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB179",
"name"=>"UTHMAN, Kaosara Ololade",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"16/66MB210",
"name"=>"UTHMAN, Khalilullah Bolaji",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB214",
"name"=>"WAHAB, Rukayat Abimbola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB181",
"name"=>"YAHAYA, Adedamola Mujidah",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB215",
"name"=>"YAHAYA, Ronke Zaynab",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB182",
"name"=>"YUSUF, Aminat Abidemi",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB183",
"name"=>"YUSUF, Sodiq Ademola",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"14/66MB201",
"name"=>"ZAKARI, David Danjuma",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66MB184",
"name"=>"ZUBAIR, Ameenah Modupe",
"level"=>"400",
"department"=>"FINANCE"
],
[
"matric_no"=>"15/66RP001",
"name"=>"ABDULAZEEZ, Abdulrouf",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP002",
"name"=>"ABDULAZEEZ, Risikat",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP003",
"name"=>"ABDULGANIYU, Kehinde Abdulbasit",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP004",
"name"=>"ABDULGANIYU, Muh-fawaz Opeyemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP006",
"name"=>"ABDULLAHI, Fatimoh",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP007",
"name"=>"ABDULLAHI, Muhammed Jamiu",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP008",
"name"=>"ABDULSALAM, Abdullahi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP009",
"name"=>"ABDULWAHAB, Abdulgaffar",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP010",
"name"=>"ABDUR-RAHMAN, Olaitan Aiku",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP011",
"name"=>"ABEFE, Abibat Dolapo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP158",
"name"=>"ABIODUN, Zainab Yetunde",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP012",
"name"=>"ABIODUN-OLUMO, Abubakar Mobolaji",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP013",
"name"=>"ABOLOYE, Tomisin Solomon",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP014",
"name"=>"ABUBAKAR, Abdulmajeed Ajibola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP015",
"name"=>"ADEBAYO, Abibat Abidemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP016",
"name"=>"ADEBAYO, Kamaldeen Adisa",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP017",
"name"=>"ADEBISI, Khadijat Adejumoke",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP020",
"name"=>"ADEBOYE, Yetunde Hasmau",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP021",
"name"=>"ADEDEJI, Adesewa Ayobami",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP022",
"name"=>"ADEDIRAN, Oyindamola Tobiloba",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP023",
"name"=>"ADEGOKE, Ifeoluwa Rachael",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP026",
"name"=>"ADEKUNLE, Mariam Adedoyin",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP027",
"name"=>"ADELAJA-OVA, Emmanuel Enubeoiza",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP028",
"name"=>"ADELEYE, Taiwo Oluwabusayo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP029",
"name"=>"ADEMOLA, Habeeb Abdul-rauf",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP030",
"name"=>"ADEPOJU, Barakat Temitope",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP031",
"name"=>"ADESINA, Ibrahim Adedapo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP019",
"name"=>"ADEYEMO, Stephen Oluwatomiwa",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP032",
"name"=>"ADIO, Barakat Adebimpe",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP033",
"name"=>"AFOLAYAN, Olayinka John",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP035",
"name"=>"AHMADU, Salima Paiko",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP036",
"name"=>"AJAO, Shukurat Adewunmi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP037",
"name"=>"AJAYI, Bashiru Junior",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP038",
"name"=>"AJAYI, Deborah Oluwadamilola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP039",
"name"=>"AJAYI, Olawale Samuel",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP025",
"name"=>"AJAYI, Oluwadamilare Ayobami",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP040",
"name"=>"AJIBARE, Odunayo Oyindamola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP041",
"name"=>"AJIDE, Habeeb Olayinka",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP042",
"name"=>"AKANDE, Qosim Olawuyi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP043",
"name"=>"AKANNI, Habeeb Abiodun",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP028",
"name"=>"AKANO, Babatunde Abiola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP030",
"name"=>"AKINOSUN, Paul Olumide",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP031",
"name"=>"AKINTUNDE, Adebayo Yusuph",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP045",
"name"=>"ALABI, Ismail Ayodeji",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP046",
"name"=>"ALABI, Yushau Ololade",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP036",
"name"=>"ALAO, Rasheedat Mosunmola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP047",
"name"=>"ALAWIYE, Peter Olakunle",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP048",
"name"=>"ALHERI, Taiye",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP049",
"name"=>"ALIU, Abdulkareem Kayode",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP050",
"name"=>"ALIU, Oluwatosin Mary",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP037",
"name"=>"ALIU, Rahmat Eniola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP051",
"name"=>"AMAO, Racheal Oluwabukola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP052",
"name"=>"AMINU, Idayat Abiodun",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP054",
"name"=>"ANIFALAJE, Oyindamola Adebusayo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP056",
"name"=>"ARAFAT, Owolewa Aderonke",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP057",
"name"=>"ASORIRE, Silifat Odunayo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP058",
"name"=>"AUDU, Abdul-rasheed Ohikueme",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP060",
"name"=>"AYODELE, Abraham Oluwatoyin",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP062",
"name"=>"AZEEZ, Kamilu Omowunmi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP063",
"name"=>"BABAMALE, Ramat",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP065",
"name"=>"BAMIGBOYE, Temitope Oluwabunmi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP066",
"name"=>"BELLO, Ibrahim Adewunmi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP068",
"name"=>"BELLO, Muheez Olarewaju",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP069",
"name"=>"BELLO, Olamilekan Azeez",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP070",
"name"=>"BUARI, Hafis Oluwaseun",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP159",
"name"=>"CHUKWUMA, Chibueze Martins",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP071",
"name"=>"DAIRO, Abeebllahi Abiodun",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP072",
"name"=>"DAVID, Moses Toluwalase",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP073",
"name"=>"ELUMOYE, Gbolagade",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP074",
"name"=>"ENIOLA, Boluwatife Olayemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP075",
"name"=>"FAGBEMIGUN, Adedayo Mary",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP076",
"name"=>"FARONBI, Aduragbemi Deborah",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP077",
"name"=>"FATAI, Abisola Olashile",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP079",
"name"=>"GBADEBO, Adedayo Damilare",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP080",
"name"=>"GBADERO, Ezekiel Babatunde",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP071",
"name"=>"HASSAN, Shakiru Babatoyese",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP081",
"name"=>"IBIWOYE, Joshua Oluwatomiwa",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP082",
"name"=>"IBRAHIM, Elizabeth Omotoyosi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP083",
"name"=>"IBRAHIM, Yusuf Opeyemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP084",
"name"=>"IDOWU, Abdulganiyu Adekunle",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP085",
"name"=>"IGBARIE, Nathan Gbolahan",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP086",
"name"=>"IHEME, Stephen Chinonso",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP075",
"name"=>"ILUFOYE, Funmilayo Damilola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP160",
"name"=>"IMUSE, Idia Naomi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP079",
"name"=>"IROAGANACHI, Joy Ngozi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP087",
"name"=>"ISAIAH, Segun Mikail",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP080",
"name"=>"JAMIU, Waliyat Adeola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP089",
"name"=>"JIMOH, Hafsat Kikelomo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP090",
"name"=>"JOHN, Blessing Ahu-oiza",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP092",
"name"=>"KOLAWOLE, Faruk Ololade",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP093",
"name"=>"KOLAWOLE, Tolulope Junior",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP094",
"name"=>"KUDABO, Paul Toluwase",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP096",
"name"=>"LAWAL, Mercy Oyiza",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP161",
"name"=>"LAWAL, Moniola Oluwatosin",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP097",
"name"=>"LIASU, Sofiat Odunayo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP098",
"name"=>"MABINUORI, Anuoluwapo Junaid",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP085",
"name"=>"MAKANJUOLA, Oluwadamilola Oreduwa",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP099",
"name"=>"MAKINDE, Joseph Kehinde",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP100",
"name"=>"MALIK, Faidat Olajumoke",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP101",
"name"=>"MALIK, Malik Olajide",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP102",
"name"=>"MBAGU, James Chukwuemeka",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP086",
"name"=>"MOGAJI, Taiwo Mummini",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP103",
"name"=>"MUDASHIR, Nafisat Abidemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP104",
"name"=>"MURITALA, Sodiq Adio",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP105",
"name"=>"MUSA, Zainab Motunrayo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP106",
"name"=>"NWACHUKWU, Gift Genevieve",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP107",
"name"=>"ODEWALE, Abayomi Samson",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP162",
"name"=>"ODIKO, Israel Endurance",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP163",
"name"=>"OGEDENGBE, Olushola Matthew",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP164",
"name"=>"OGUNDIPE, Tolulope Elizabeth",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP108",
"name"=>"OGUNJOBI, Matthew Olalekan",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP109",
"name"=>"OJELABI, Baliqees Omowunmi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP110",
"name"=>"OKOTUME, Daniel Ayomide",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP111",
"name"=>"OLADIPUPO, Taiwo Mary",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP112",
"name"=>"OLAGOKE, Faizat Omotolani",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP113",
"name"=>"OLALERE, Ifeoluwa Esther",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP100",
"name"=>"OLANIYI, Olakunle Oluwatobi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP101",
"name"=>"OLANREWAJU, Abayomi Hanatt",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP102",
"name"=>"OLANREWAJU, Isiaka Adisa",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP114",
"name"=>"OLASOGBA, Tomiwa Daniel",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP115",
"name"=>"OLAWEPO, Abdulsamad Opeyemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP117",
"name"=>"OLAYODE, Samuel Bolaji",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP165",
"name"=>"OLOYEDE, Deborah Opeyemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP166",
"name"=>"OLUKOYA, Christiana Seun",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP167",
"name"=>"OLUWAFEMI, Moyinoluwa Cecilia",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP118",
"name"=>"OLUWATIMEHIN, Oluwademilade Progress",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP110",
"name"=>"ONIYA, Oluwasegun Emmanuel",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP120",
"name"=>"ONIYIDE, Ibukunoluwa Segun",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP121",
"name"=>"ONOJA, Hezekiah Chubiyojo",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP124",
"name"=>"OWA, Israel Ayoola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP125",
"name"=>"OWOLABI, Blessing",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP127",
"name"=>"OWOYEMI, Azeezat Olajumoke",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP128",
"name"=>"OYEBODE, Nusirat Gbemisola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP113",
"name"=>"OYEWOLE, Eniola Tolulope",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP129",
"name"=>"RAHMON, Toheeb Olamide",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP119",
"name"=>"RAJI, Saadat Adedamola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP120",
"name"=>"RASAQ, Nafisat Adeola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP130",
"name"=>"SAKA, Sherifat Omowunmi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP127",
"name"=>"SANI, Halima",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP131",
"name"=>"SHOWEMIMO, Yetunde Barakat",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP134",
"name"=>"SULEIMAN, Nureni Opeyemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP135",
"name"=>"SULYMAN, Hammed Bolaji",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP135",
"name"=>"SYLVESTER, Juviet James",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP136",
"name"=>"TOKOYA, Olaoluwa Olaniyi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP168",
"name"=>"UGBAJEH, Elizabeth",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP136",
"name"=>"YINUSA, Ridwan Olanrewaju",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP170",
"name"=>"YUSUF, Idris Opeyemi",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP137",
"name"=>"YUSUF, Muftiat Olanike",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP138",
"name"=>"YUSUF, Rodiyat Eniola",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"15/66RP139",
"name"=>"YUSUFF, Habeeb Babatunde",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"14/66RP139",
"name"=>"ZAKARIYA, Samuel Shaji",
"level"=>"400",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP127",
"name"=>"ABAKPA, Hannah Naomi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP001",
"name"=>"ABAYOMI, Khalilulah Oladipupo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP002",
"name"=>"ABDUL-AZEEZ, Folashade Khadijat",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP003",
"name"=>"ABDUL-SALAM, Ridwan Olanrewaju",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP004",
"name"=>"ABDULAZEEZ, Shakirat Agoro",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP006",
"name"=>"ABDULRAHMAN, Aishat Oyindamola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP008",
"name"=>"ABDULSALAM, Khadijat Titilayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP009",
"name"=>"ABEGUNDE, Oluwajuwon Seun",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP010",
"name"=>"ABUBAKAR, Fadheelat Yetunde",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP012",
"name"=>"ADEBOYE, Omogbolahan Oluwatomisin",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP013",
"name"=>"ADEDIRAN, Rofiqat Folake",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP128",
"name"=>"ADEGOKE, Omotolani Ololade",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP016",
"name"=>"ADENUBI, Matthew Gbenga",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP018",
"name"=>"ADEWALE, Oluwaseun Henry",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP019",
"name"=>"ADEWUMI, Adeolu Samson",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP020",
"name"=>"ADEWUMI, Gold Adeola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP129",
"name"=>"ADIGUN, Abike Aduragbemi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP021",
"name"=>"AHMED, Samsudeen Olarewaju",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP022",
"name"=>"AJAYI, Oluwafunmilola Comfort",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP023",
"name"=>"AJAYI, Paul Adefarati",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP025",
"name"=>"AJIKOBI, Rasheedat Lami",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP026",
"name"=>"AJILA, Serah Ibukunoluwa",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP027",
"name"=>"AJITERU, Ayodeji Israel",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP029",
"name"=>"AKINTOMIDE, Timileyin M",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP030",
"name"=>"AKOREDE, Firdaws Abdulrasheed",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP130",
"name"=>"AKOREDE, Mubarak Olamide",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP031",
"name"=>"ALABIDUN, Tawakalitu Bolanle",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP032",
"name"=>"ALLIU, Nabilat Ahuoiza",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP033",
"name"=>"AMOO, Modinat Olajuwon",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP034",
"name"=>"AMUSAN, Ayobami Ademola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP035",
"name"=>"ANIMASHAUN, Mudirah Olajumoke",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP037",
"name"=>"ATANDA, Jamiu Babatunde",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP131",
"name"=>"ATOLAGBE, Folashade Elizabeth",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP038",
"name"=>"ATOLAGBE, Oluwaseyi Samuel",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP039",
"name"=>"ATUTU, Vivian Gift",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP040",
"name"=>"AWUJOOLA, Toheeb Olawale",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP132",
"name"=>"AYANTUNJI, Oluwatobi Peace",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP043",
"name"=>"BABALOLA, Nurudeen Olamilekan",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP044",
"name"=>"BABALOLA, Pauline Asipita",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP045",
"name"=>"BADMUS, Ganiyu Abdullahi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP048",
"name"=>"BALOGUN, Aminat Ajoke",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP049",
"name"=>"BALOGUN, Idris Damilola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP050",
"name"=>"BELLO, Emmanuel Olaoluwa",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP051",
"name"=>"BELLO, Rashida",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP052",
"name"=>"CHIMA, Olive",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP053",
"name"=>"DADA, Timilehin Stephen",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP054",
"name"=>"DANIEL, Mary Ufedo-ojo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP133",
"name"=>"DAVID, Joshua Nonso",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP055",
"name"=>"DAVIES, Samuel Dickson",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP056",
"name"=>"EDUNGBOLA, Susanna Oluwakemi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP057",
"name"=>"EGEONU, Ogochukwu Ann",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP059",
"name"=>"ENESI, Lydia Titilayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP134",
"name"=>"ENO, Nfoniso Fortune",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP060",
"name"=>"ESSIEN, Victoria Ekanem",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP061",
"name"=>"EYIJE, Margaret Echi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP062",
"name"=>"FASINU, Austin Sewhenu",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP063",
"name"=>"GANIYU, Qudus Korede",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP064",
"name"=>"HAMZAT, Nafisat Abiodun",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP065",
"name"=>"HASSAN, Quadri Adeola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP066",
"name"=>"IBITOYE, Rukayat Temitope",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP068",
"name"=>"IBRAHIM, Monsurat Omolade",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP135",
"name"=>"IDOWU, Mafus Olamide",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP069",
"name"=>"ISAH, Mercy Ohunene",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP071",
"name"=>"ISSA, Abdulrasheed Olayinka",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP136",
"name"=>"JACOB, Esther Ojonugwa",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP073",
"name"=>"JAMES, Hannah Omolayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP074",
"name"=>"JIMOH, Olamide Zinat",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP076",
"name"=>"JIMOH, Sodiq Olamilekan",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP137",
"name"=>"JOSEPH, Oluwasegun Mauton",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP079",
"name"=>"JOSEPH, Victoria Bosede",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP080",
"name"=>"KAJOGBOLA, Opeyemi Oluwole",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP082",
"name"=>"KOLADE, Isaac Opeoluwa",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP083",
"name"=>"KUNTOR, Adebayor Michael",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP084",
"name"=>"LAARO, Ridwan Bolaji",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP085",
"name"=>"LAWAL, Afeez Olusegun",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP086",
"name"=>"LAWAL, Ibrahim Adekunle",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP087",
"name"=>"LAWAL, Jamiu Ajibola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP088",
"name"=>"LAWAL, Mariam Oluwafunmilayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP089",
"name"=>"LAWAL, Rukayat Titilayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP090",
"name"=>"MASHOOD, Fatimah Busari",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP091",
"name"=>"MATTHEW, Akinola Nathan",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP138",
"name"=>"MAUNTA, Funmilayo Blessing",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP092",
"name"=>"MUSA, Ridwan Uche",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP093",
"name"=>"MUSA, Waris Olamilekan",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP094",
"name"=>"MUSTAPHA, Olawunmi Asiat",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP095",
"name"=>"NDUMANYA, Isioma Rapuluchukwu",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP096",
"name"=>"OBADARE, Kayode Francis",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP097",
"name"=>"ODEBISI, Hikmat Oladoyin",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP098",
"name"=>"ODEKUNLE, Benjamin Oladipupo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP099",
"name"=>"ODEYALE, Seyi Segun",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP102",
"name"=>"OGUN, Rotimi Daniel",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP104",
"name"=>"OGUNYEMI, Victoria Oluwatosin",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP105",
"name"=>"OJELABI, Gideon Bamidele",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP106",
"name"=>"OJO, Temitope Christianah",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP107",
"name"=>"OJUBANIRE, Fatimah Ifeoluwa",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP139",
"name"=>"OJUSIN, Simeon R",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP108",
"name"=>"OKECHUKWU, Chiamaka Esther",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP140",
"name"=>"OKEKE, Lindaline Nkiruka",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP109",
"name"=>"OKEYELE, Timothy Oluwaseyi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP110",
"name"=>"OKOJI, Rosemary Onyinyechukwu",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP111",
"name"=>"OKOTUME, Precious Esther",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP141",
"name"=>"OLABAYIWA, Omoshalewa Racheal",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP112",
"name"=>"OLABODE, Hawaw Omotayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP142",
"name"=>"OLANIYI, Olaoluwa Martins",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP114",
"name"=>"OLAONIPEKUN, Nofisat Morenikeji",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP143",
"name"=>"OLARETAN, Oluwadamilare Micheal",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP115",
"name"=>"OLAREWAJU, Abosede",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP116",
"name"=>"OLATUNDE, Aisha Omotoyosi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP117",
"name"=>"OLAYEMI, Juwon Sunday",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP118",
"name"=>"OLOMODA, Fathiat Kofoworola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP120",
"name"=>"OLORUNNAIYE, Moyinoluwa Faustina",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP121",
"name"=>"OLORUNSOLA, Samuel Fagbarahanmi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP122",
"name"=>"OLUMOH, Mariam Adebukola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP123",
"name"=>"OLUWATOSIN, Oluwadamilola Oluwaseun",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP124",
"name"=>"OLUWAWOLE, Glory Boluwatife",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP125",
"name"=>"OMOGBAI, Noah Ohis",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP126",
"name"=>"OMOSIDI, Adam",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP127",
"name"=>"OMOTOSHO, Aminat Temitope",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP128",
"name"=>"OMOTOSHO, Muinat Tayo",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP129",
"name"=>"OTEGBEYE, Omotola Salimat",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP130",
"name"=>"OWA, Oluwafemi Emmanuel",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP131",
"name"=>"OYEBAMIRE, Monsuru Idowu",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP132",
"name"=>"OYELEKE, Oluwamayowa Hezekiah",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP134",
"name"=>"OYEYODE, Kolapo Ridwan",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP135",
"name"=>"QUADRI, Shukurat Tanwa",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP136",
"name"=>"ROBERTS, Victoria Olajumoke",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP138",
"name"=>"SALAKO, Sodiq Gbenga",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP139",
"name"=>"SALAM, Omowunmi Ganiyat",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP140",
"name"=>"SANNI, Aishat Temitope",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP141",
"name"=>"SHOSAN, Ifeanyi Oluwafemi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP142",
"name"=>"SOBANKE, Fatimah Boluwatife",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP143",
"name"=>"SODIQ, Kudirat Abiola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP144",
"name"=>"SULAIMON, Ganiu Oyeyemi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP145",
"name"=>"SUNMONU, Temitayo Oyindamola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP146",
"name"=>"TANIMOWO, Idris Ololade",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP147",
"name"=>"TEJUMADE, Oluwatomi Tamarapreye",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP148",
"name"=>"TOYIN, Shukurah Oyinkansola",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP149",
"name"=>"UGOCHUKWU, Lillian Onyebuchi",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP169",
"name"=>"UNEGBU, Collins Echezona",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP151",
"name"=>"USMAN, Abdulwasiu Oladimeji",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP152",
"name"=>"USMAN, Hakeem Olamilekan",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP153",
"name"=>"WAHAB, Zainab Omolara",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP154",
"name"=>"YEYE, Victoria Tolulope",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP155",
"name"=>"YUSUF, Habeeb Olawale",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"16/66RP157",
"name"=>"ZUBAIR, Aminat",
"level"=>"300",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP116",
"name"=>"ABDULSALAM, Suliyat Damilola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP117",
"name"=>"ADEAGBO, Adewale Joseph",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP118",
"name"=>"ADEBISI, Olajumoke Amudalat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP119",
"name"=>"ADEGBOLA, Olasunkanmi Sekinat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP120",
"name"=>"ADEJUYIGBE, Moyomade Peace",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP121",
"name"=>"ADELEYE, Ademide Janet",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP122",
"name"=>"ADESINA, Adetunji Micheal",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP123",
"name"=>"AIJEBA, Mercy Otiti",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP124",
"name"=>"AJAO, Rofiat Ayinke",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP125",
"name"=>"AJIBOYE, Ademola Isaac",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP126",
"name"=>"AKINBODE, Victoria Ayomide",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP127",
"name"=>"AMOS, Titilayo Abosede",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP128",
"name"=>"BAIYEWUMI, Oluwaseyi Ayomide",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP129",
"name"=>"EKPE, Chinemerem Kenneth",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP130",
"name"=>"ISMAIL, Zainab Bukky",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP131",
"name"=>"IWUJI, Amaka Judith",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP132",
"name"=>"JIMOH, Abimbola Ibrahim",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP133",
"name"=>"JOLAOSO, Abisola Justinah",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP134",
"name"=>"NURUDEEN, Akeem Ayobami",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP135",
"name"=>"OLUNLADE, Opeyemi Gauzat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP136",
"name"=>"SAMUEL, Agmada Gracious",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP137",
"name"=>"SAMUEL, Oluwademilade Dorcas",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP001",
"name"=>"ABDULLAHI, Olaide Bushirat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP002",
"name"=>"ABDULMAJEED, Kawthar Eniola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP003",
"name"=>"ABDULRAHMAN, Heekmat Omobolanle",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP004",
"name"=>"ABDULRAHMAN, Mahmud Ishola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP005",
"name"=>"ABIODUN, Oluwadamilare Omotoyosi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP006",
"name"=>"ABIODUN, Rukayat Juliet",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP007",
"name"=>"ABIOLA, Babatunde Fuad",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP009",
"name"=>"ADEBIYI, Ifeoluwa Mary",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP010",
"name"=>"ADEDEJI, Ebunoluwa Helen",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP011",
"name"=>"ADEGOKE, Abimbola Ahmed",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP012",
"name"=>"ADELU, Adesewa Adedoyin",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP013",
"name"=>"ADEMILUYI, Boluwatife Stephen",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP014",
"name"=>"ADENIGBA, Justina Olamide",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP015",
"name"=>"ADENIRAN, Lydia Oluwakemi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP016",
"name"=>"ADENIRAN, Oluwatomisin Adetayo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP017",
"name"=>"ADEOYE, Elisha Oluwadamilare",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP018",
"name"=>"ADEWALE, Khadijat Adejumoke",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP019",
"name"=>"ADEYEYE, Abigeal",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP020",
"name"=>"AGBAJE, Oluwasanmi Israel",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP021",
"name"=>"AHMED, Sunday Diamond",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP022",
"name"=>"AINA, Oluwasemilore Omobolanle",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP023",
"name"=>"AJAO, Rashidat Opeyemi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP024",
"name"=>"AJIJOLA, Kamaldeen Olasunkanmi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP025",
"name"=>"AJOMALE, Ayodeji Simon",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP026",
"name"=>"AKHIGBE, Francis",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP028",
"name"=>"AKINDELE, Mariam Ajoke",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP030",
"name"=>"AKOR, Winner Ojochide",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP031",
"name"=>"ALABI, Samuel Babawale",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP032",
"name"=>"ALAWODE, Tobi Isaac",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP033",
"name"=>"ALAYAKI, Oladimeji Jamiu",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP034",
"name"=>"ALEBIOWU, Anuoluwapo Pelumi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP035",
"name"=>"ALIAGAN, Rukayat Opeyemi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP036",
"name"=>"ALIU, Ayodotun Dominic",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP037",
"name"=>"AMODU, Kehinde Oluwatoyin",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP038",
"name"=>"AMOO, Fuad Ademola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP039",
"name"=>"ANDAH, Victor Jesutofunmi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP040",
"name"=>"ANIMASHAUN, Mary Adeola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP041",
"name"=>"ANNETOR, Blessing Beauty",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP042",
"name"=>"ASONIBARE, Ezekiel Femi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP043",
"name"=>"ATOLAGBE, Nurudeen Omotayo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP044",
"name"=>"AYANDELE, Olamide Caleb",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP045",
"name"=>"AYINLA, Toheeb Olaniyi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP046",
"name"=>"AYODELE, Michael Idowu",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP048",
"name"=>"BABATUNDE, Tosin Ibukun",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP049",
"name"=>"BADMUS, Olajumoke Alimat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP050",
"name"=>"BAKER, Rofiah Omotolani",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP051",
"name"=>"BALOGUN, Gafar Oyerinde",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP052",
"name"=>"BALOGUN, Samiat Adeoti",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP053",
"name"=>"BELLO, Abdulrahman Adekunle",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP054",
"name"=>"BOLARINWA, Teniola Aminat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP055",
"name"=>"CHINEDU, Tochukwu Somfeh",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP056",
"name"=>"DARE, Oyindamola Tolulope",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP057",
"name"=>"DRAMMEH, Bangali Muhammed",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP058",
"name"=>"EDWARD, Alice Damilola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP059",
"name"=>"ETENIKANG, Imelda Abiodun",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP060",
"name"=>"EZEH, Wisdom Chinedu",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP061",
"name"=>"HASSAN, Hussein Oladimeji",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP062",
"name"=>"IDRIS, Bidemi Mariam",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP063",
"name"=>"IDRIS, Mariam Motunrayo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP064",
"name"=>"ILELABOYE, Basirat Motunrayo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP065",
"name"=>"INAWOLE, Iyanuoluwa Adeola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP066",
"name"=>"ISIAQ, Aishat Motunrayo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP069",
"name"=>"JAMIU, Barakat Oluwakemi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP070",
"name"=>"JIMOH, Barakat Temitope",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP071",
"name"=>"KAYODE, Balikis Abimbola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP072",
"name"=>"KAYODE, Samuel Opeyemi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP073",
"name"=>"KING, Kanyinsola Hannah",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP074",
"name"=>"KING, Olamide Grace",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP075",
"name"=>"KOLADE, Oluwatobi Segun",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP076",
"name"=>"LAMOYERO, Kabir Abisoye",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP077",
"name"=>"LATEEF, Damilare Tiamiyu",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP078",
"name"=>"LAWAL, Babatunde Ismail",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP080",
"name"=>"LAWAL, Maryam Olatundun",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP082",
"name"=>"MAHMOOD, Abdulkabir Abiodun",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP083",
"name"=>"MAKINDE, Olamide Saidat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP084",
"name"=>"MOHAMMED, Barakat Oyinkansola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP085",
"name"=>"MOHAMMED, Rukayah Lami",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP087",
"name"=>"MOLIK, Hikmot Abodunrin",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP088",
"name"=>"MOSHOOD, Robiat Bisola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP089",
"name"=>"MUDASIRU, Olawunmi Monsurat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP090",
"name"=>"OBADINA, Basirat Taiwo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP091",
"name"=>"ODUNLAMI, Taiwo Mary",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP092",
"name"=>"ODUOLA, Soromidayo Oluwatimileh",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP093",
"name"=>"OGUNMEFUN, Mistura Omowunmi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP094",
"name"=>"OGUNYEMI, Florence Damilola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP095",
"name"=>"OJO, Teminijesu Damilola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP096",
"name"=>"OKEFE, Sunday Onoja",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP097",
"name"=>"OLADELE, Ayobami Francis",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP098",
"name"=>"OLAKI, Faithful Jesutofunmi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP099",
"name"=>"OLAWEPO, Gabriel Rotimi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP100",
"name"=>"OLAWOYIN, Eniola Khadijat",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP101",
"name"=>"OLAWUNMI, Oluwadamilare Bolajoko",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP102",
"name"=>"OLIVER, Peace Chinonso",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP103",
"name"=>"OLOGUNDE, Olaoluwa James",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP104",
"name"=>"OLORUNBIYI, Faith Oluwatobiloba",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP107",
"name"=>"OLUREMI, Tosin Deborah",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP108",
"name"=>"OLUWADELE, Ifeoluwa Christiana",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP109",
"name"=>"OMAKUN, Taofeek Omobolaji",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP110",
"name"=>"ONIFADE, Taiwo Sitrah",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP111",
"name"=>"ONOIGBORIA, Benjamin Aikhuomobh",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP112",
"name"=>"OWOLABI, Mariam Olamide",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP113",
"name"=>"OYEWOLU, Damilola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP114",
"name"=>"ROSHEED, Abdulsomod Adebowale",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP115",
"name"=>"ROTIMI, Bukola Mercy",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP116",
"name"=>"SAKARIYAU, Islamiat Abiodun",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP117",
"name"=>"SALAM, Hikmot Adedolapo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP118",
"name"=>"SALAUDEEN, Abdullahi Olamilekan",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP119",
"name"=>"SALMON, Abdulquadri O",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP120",
"name"=>"SHOETAN, Mariam Odunayo",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP121",
"name"=>"SONUGA, Taiwo Deborah",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP122",
"name"=>"SULAIMAN, Monsurat Opeyemi",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP123",
"name"=>"SULAIMON, Mujidah Olaide",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP124",
"name"=>"TIJANI, Oluwadamilola Abdulazeez",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP125",
"name"=>"YISA, Yusuf Olamitunji",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"17/66RP126",
"name"=>"YUSSUF, Imranat Kofoworola",
"level"=>"200",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP001",
"name"=>"ABDUL-AZEEZ, Folarin Alade",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP002",
"name"=>"ABDULKAREEM, Muniamot Oluwakemi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP003",
"name"=>"ABIODUN, Waliu Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP004",
"name"=>"ABIOYE, Victor Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP005",
"name"=>"ABUBAKAR, Fatimoh Opeyemi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP006",
"name"=>"ADACHE, Peter Ugbede",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP007",
"name"=>"ADAM, Damilola Mogaji",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP008",
"name"=>"ADEBOYE, Funke Mariam",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP009",
"name"=>"ADEDIMEJI, Michael Oladapo",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP010",
"name"=>"ADEDOKUN, Afeez Adesola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP011",
"name"=>"ADEJUMOBI, Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP012",
"name"=>"ADEKOGBE, Tobiloba Sodik",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP013",
"name"=>"ADENIRAN, Zainab Olawumi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP014",
"name"=>"ADEOTI, Olawale Emmanuel",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP015",
"name"=>"ADEREMI, Lydia Oluwatoyin",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP016",
"name"=>"ADEWUMI, Alfred Adedoyin",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP017",
"name"=>"ADISA, Fausat Adeola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP018",
"name"=>"AFOLABI, Abraham Segun",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP019",
"name"=>"AGBI, Abimbola Micheal",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP020",
"name"=>"AGBO, Goodness Nwakuso",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP021",
"name"=>"AGBOOLA, Olaseeni Farouk",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP022",
"name"=>"AJALA, Halimat Olubukola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP023",
"name"=>"AJAYI, Ganiu Adinoi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP024",
"name"=>"AJAYI, Oyindamola Temitayo",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP025",
"name"=>"AJIBOYE, Olutoye Adebowale",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP026",
"name"=>"AJIBOYE, Timileyin Oluwatobi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP027",
"name"=>"AKANDE, Shuaib Olamide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP028",
"name"=>"AKANDE, Taiwo Damilare",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP029",
"name"=>"AKERELE, Olajumoke Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP030",
"name"=>"AKINLEYE, Suliyat Kofoworola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP031",
"name"=>"ALARAPE, Azeez Babatunde",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP032",
"name"=>"ALIYU, Abubakry Abiodun",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP033",
"name"=>"ANIMASHAUN, Idowu Afolashade",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP034",
"name"=>"AROLU, Arafat Titilope",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP035",
"name"=>"ATOLAGBE, Elijah Oluwatobi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP036",
"name"=>"AWEDA, Islamiyah Temitope",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP037",
"name"=>"AYO-ABIDOLU, Ewaoluwa Precious",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP038",
"name"=>"AYODOKUN, Faith Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP039",
"name"=>"AYORINDE, Halimat Omowumi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP040",
"name"=>"AZZAN, Mujibat Omotoyosi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP041",
"name"=>"BABAJIDE, Iyanuoluwa Rachael",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP042",
"name"=>"BABATUNDE, Oluwatoyosi Esther",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP043",
"name"=>"BALOGUN, Ibrahim Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP044",
"name"=>"BELLO, Aishat Omolola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP045",
"name"=>"BELLO, Damilola Rachel",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP046",
"name"=>"BELLO, Morolake Faheedat",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP047",
"name"=>"BELLO, Oluwatosin Mariam",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP048",
"name"=>"DANIEL, Nathan Ose",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP049",
"name"=>"EBITO, Grace Mfon",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP050",
"name"=>"EIGEGE, Emmanuel Acheme",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP051",
"name"=>"EJIOGU, Sharon Oyinkansola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP052",
"name"=>"FADAKA, Eniola Catherine",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP053",
"name"=>"FELIX, Favour Ugochi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP054",
"name"=>"GABRIEL, Blessing Jesulayomi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP055",
"name"=>"IBRAHIM, Ganiyat",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP056",
"name"=>"IBRAHIM, Islamiyat Motunrayo",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP057",
"name"=>"JAJI, Abdulrahman Ademola",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP058",
"name"=>"JEREMIAH, David Haruna",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP059",
"name"=>"JIMOH, Aishat Yetunde",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP060",
"name"=>"JIMOH, Ayomide Buliamin",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP061",
"name"=>"JOHN, Rebecca Oluwapelumi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP062",
"name"=>"JOKOTOLA, Timothy Amos",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP063",
"name"=>"KENNETH, Success",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP064",
"name"=>"KOLAWOLE, Deborah Tiwajuopelo",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP065",
"name"=>"LAWAL, Amidat Modupe",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP066",
"name"=>"LAWAL, Jeremiah David",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP067",
"name"=>"LAWAL, Jubril Eyitayo",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP068",
"name"=>"MAJEKADEGBE, Aminat Bamitale",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP069",
"name"=>"MICHEAL, Damilola Abidemi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP070",
"name"=>"MOHAMMED, Ibrahim Omaiye",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP071",
"name"=>"MOMOH, Hephzibah Damilare",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP072",
"name"=>"MOSES, Emmanuel Pelumi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP073",
"name"=>"MOYOSORE, Ayomide Ajayi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP074",
"name"=>"NWACHUKWU, Marvellous Chisom",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP075",
"name"=>"NWAEZE, John Chukwuemeka",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP076",
"name"=>"OBIKOYA, Kehinde Joseph",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP077",
"name"=>"OBIOHA, Immaculate Chidiebere",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP078",
"name"=>"ODUOLA, Timilehin Mary",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP079",
"name"=>"OGBODO, Jessica Chinwendu",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP080",
"name"=>"OKOLI, Blessing Chinaecherem",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP081",
"name"=>"OKOLI, Joseph Chukwudi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP082",
"name"=>"OKWUTE, Jennifer Isioma",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP083",
"name"=>"OLABODE, Faith Oluwatunmise",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP084",
"name"=>"OLADIJI, Deborah Modupeoluwa",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP085",
"name"=>"OLAJIDE, Adenike Oluwaseun",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP086",
"name"=>"OLAOYE, Iretiola Ajoke",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP087",
"name"=>"OLAWALE, Hikmat Oludoyin",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP088",
"name"=>"OLAWUSI, Olamilekan Olamide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP089",
"name"=>"OLAYINKA, Idowu Eunice",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP090",
"name"=>"OLONINIJO, Taiye Joel",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP091",
"name"=>"OLORUNTOBA, Oyindamola Emmanuel",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP092",
"name"=>"OLOSO, Bushrah Bolanle",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP093",
"name"=>"OMOYOSI, Dominion Temiloluwa",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP094",
"name"=>"ORESHADE, Opeyemi Elizabeth",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP095",
"name"=>"OSHEIDU, Daniel Olumide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP096",
"name"=>"OYEDELE, Abayomi Owolabi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP097",
"name"=>"OYEDELE, Ronke Grace",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP098",
"name"=>"OYETUNJI, Oluwaseyi Victory",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP099",
"name"=>"PETER, Deborah Favour",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP100",
"name"=>"RAZAQ, Remilekun Rasheedat",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP101",
"name"=>"RUFUS, Precious Chidiomimi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP102",
"name"=>"SALAMI, Samiat Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP103",
"name"=>"SALATY, Fatimat Yahya",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP104",
"name"=>"SALAUDEEN, Rafiat Ayobami",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP105",
"name"=>"SALIHU, Lukman Gupa",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP106",
"name"=>"SAMUEL, Abosede Opeyemi",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP107",
"name"=>"SENAIKE, Michael Ayomide",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP108",
"name"=>"SILLAH, Mariam Janke",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP109",
"name"=>"SULAIMON, Rashidat Motunrayo",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP110",
"name"=>"SULEIMAN, Damilola Rashidat",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP111",
"name"=>"TAIWO, Tolani Christiana",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP112",
"name"=>"TIAMIYU, Ridwan Olamilekan",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP113",
"name"=>"TUNDE, David Akin",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP114",
"name"=>"YAKUB, Jubril Bolarinwa",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66RP115",
"name"=>"YUSUF, Abdullateef Olayinka",
"level"=>"100",
"department"=>"INDUSTRIAL RELATIONS AND PERSONNEL MANAGEMENT"
],
[
"matric_no"=>"18/66MG001",
"name"=>"ABALAGADA, Suliat Oluwatoyin",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG002",
"name"=>"ABASS-OLISA, Akeem Adetayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG003",
"name"=>"ABD-FATAI, Roqeeb",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG004",
"name"=>"ABDUL, Yusuf Oladapo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG005",
"name"=>"ABDULFATAI, Kaosarat Morenikeji",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG006",
"name"=>"ABDULGANIYU, Fuad Olamide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG007",
"name"=>"ABDULQUADRI, Salamat Adejumoke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG008",
"name"=>"ABDULRAHEEM, Zeenat Ajibola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG009",
"name"=>"ABDULRASAK, Aishat Oyinbukunmi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG010",
"name"=>"ABDURAFI, Sururat Abisola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG011",
"name"=>"ABIODUN, Abraham Oreoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG012",
"name"=>"ABIODUN, Oluwadamilola Deborah",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG013",
"name"=>"ABU, Olamide Victoria",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG014",
"name"=>"ABUBAKAR, Hussayn Sulyman",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG015",
"name"=>"ABUBAKAR, Jumai Ojonufedo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG016",
"name"=>"ADAMS, Joy Ohonmome",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG017",
"name"=>"ADEBAYO, Aisha Olaide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG018",
"name"=>"ADEBAYO, Muhammed Adeniyi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG019",
"name"=>"ADEBISI, Esther Abosede",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG020",
"name"=>"ADEBIYI, Adekunle Emmanuel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG021",
"name"=>"ADEBIYI, Adeola Anike",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG022",
"name"=>"ADEBOYE, Adebukola Dorcas",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG023",
"name"=>"ADEBOYE, Muhammed Mukthar",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG024",
"name"=>"ADEDEJI, Joshua Olayinka",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG025",
"name"=>"ADEFENWA, Yusuf Adebisi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG026",
"name"=>"ADEGBOYEGA, Daniel Kehinde",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG027",
"name"=>"ADEGOKE, Adebisi Adebukola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG028",
"name"=>"ADEGOKE, Oyinade Esther",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG029",
"name"=>"ADEMOLA, Adewole Israel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG030",
"name"=>"ADENIRAN, Taiye Enoch",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG031",
"name"=>"ADENIYI, Abayomi Adeshina",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG032",
"name"=>"ADENIYI, Omowunmi Tinuade",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG033",
"name"=>"ADENLE, Zeenat Yetunde",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG034",
"name"=>"ADEOYE, Timilehin Ezekiel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG035",
"name"=>"ADERIBIGBE, Moses Adekunle",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG036",
"name"=>"ADESEYOJU, Oluwafisayomi Victoria",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG037",
"name"=>"ADESOLA, Joseph Olanrewaju",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG038",
"name"=>"ADESOYE, Olasunkanmi Emmanuel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG039",
"name"=>"ADEWOYE, Mercy",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG040",
"name"=>"ADEWUMI, Opeyemi Daniel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG041",
"name"=>"ADEWUMI, Prayer Anuoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG042",
"name"=>"ADEYANJU, Matthew Ayomide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG043",
"name"=>"ADEYEMI, Tosin Florence",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG044",
"name"=>"ADIGUN, Temitope Ajoke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG045",
"name"=>"ADIMULA, Goodness Enoch",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG046",
"name"=>"ADIRO, Mayowa Uthman",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG047",
"name"=>"AFOLABI, Oluwaseun Emmanuel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG048",
"name"=>"AGBOOLA, Damola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG049",
"name"=>"AGBOOLA, Raheem Temidayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG050",
"name"=>"AGUNBIADE, Mariam Omowumi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG051",
"name"=>"AHMED, Nusirat Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG052",
"name"=>"AHMED, Oladimeji Gabriel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG053",
"name"=>"AINA, David Oluwajuwon",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG054",
"name"=>"YAKUB, Aishat",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG055",
"name"=>"AJALA, Victoria Aduragbemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG056",
"name"=>"AJANAKU, Temidayo Esther",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG057",
"name"=>"AJAO, Abdulmalik Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG058",
"name"=>"AJAYI, Stephen Oluwadamilola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG059",
"name"=>"AJEWOLE, Israel Abiodun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG060",
"name"=>"AJIBIKE, Olamide Christopher",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG061",
"name"=>"AJIBOLA, Abdulmuheen Olamilekan",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG062",
"name"=>"AJIBOLA, Iyanuoluwa Taiye",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG063",
"name"=>"AKINBOTE, Ayorinde Gabriel",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG064",
"name"=>"AKINOLA, Kehinde Oluwatofunmi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG065",
"name"=>"ALABELAPO, Hamdallah Ayoola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG066",
"name"=>"ALADEOJEBI, Iyanuoluwa Timilehin",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG067",
"name"=>"ALESHINLOYE, Sherifat Omowumi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG068",
"name"=>"ALIMI, Fatimah Dolapo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG069",
"name"=>"ALIU, Halimat Ayomide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG070",
"name"=>"ALLI, Barakat Temilayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG071",
"name"=>"ALOWOESHIN, Habeeb Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG072",
"name"=>"AMAKYE, Ruben Asante",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG073",
"name"=>"AMINU, Abdullahi Adinoyi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG074",
"name"=>"AMINU, Aishat Boluwatife",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG075",
"name"=>"ANJORIN, Ifeoluwa Busolami",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG076",
"name"=>"APOOYIN, Awwal Adegoke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG077",
"name"=>"AROGUNDADE, Racheal Oluwatomidimu",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG078",
"name"=>"ASHAOLU, Raphael Olakunle",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG079",
"name"=>"ATOLANI, Sharon Tolulope",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG080",
"name"=>"AUDU, Daniel Ayomide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG081",
"name"=>"AYINDE, Adam Abdullateef",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG082",
"name"=>"BABATUNDE, Adedeji Enoch",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG083",
"name"=>"BABATUNDE, Blessing Lola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG084",
"name"=>"BABATUNDE, Moyosoreoluwa Eunice",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG085",
"name"=>"BABATUNDE, Olamide Sunday",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG086",
"name"=>"BADMUS, Maryam Ajoke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG087",
"name"=>"BAKARE, Kehinde Suliat",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG088",
"name"=>"BAKARE, Olasubomi Faruq",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG089",
"name"=>"BAKARE, Taiwo Sukurat",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG090",
"name"=>"BALOGUN, Al-ameen Ademola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG091",
"name"=>"BALOGUN, Sultan Abiodun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG092",
"name"=>"BAMGBOYE, Adenike Victoria",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG093",
"name"=>"BANJO, Victoria Ebunola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG094",
"name"=>"BELLO, Faheedat Ololade",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG095",
"name"=>"BELLO, Olamide Damilola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG096",
"name"=>"BELLO, Rukayat Omowunmi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG097",
"name"=>"BUSARI, Olawale Ahmed",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG098",
"name"=>"CHUKWU, Happiness Adaobi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG099",
"name"=>"DURU, Favour Chineme",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG100",
"name"=>"EKUNDAYO, Okikijesu Joseph",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG101",
"name"=>"FASHANU, Olaide Esther",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG102",
"name"=>"FASUGBA, Sunday Oluwafemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG103",
"name"=>"FERUKE, Abdulbasit Azeez",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG104",
"name"=>"FOLORUNSHO, David Oluwasegun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG105",
"name"=>"GANIYU, Aminat Adenike",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG106",
"name"=>"GIDADO, Gold Mary",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG107",
"name"=>"IBITOYE, Omotayo Malik",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG108",
"name"=>"IBIWOYE, Samson Damilare",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG109",
"name"=>"IBRAHIM, Utaji Bello",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG110",
"name"=>"IDRIS, Idowu Mufutau",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG111",
"name"=>"IFEANYICHUKWU, Esther Chisom",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG112",
"name"=>"IFEDELE, Clement Stephen",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG113",
"name"=>"IKUBORIJE, Temitope Segun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG114",
"name"=>"ISAH, Oziohu Blessing",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG115",
"name"=>"ISIAKA, Lateefat Ajobi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG116",
"name"=>"ISSA, Ahmad",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG117",
"name"=>"IYANIWURA, Oluwatosin Joana",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG118",
"name"=>"JAIYEOLA, Emmanuella Oluwafunso",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG119",
"name"=>"JIMBA, John Adekunle",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG120",
"name"=>"JIMOH, Ahmed Ishola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG121",
"name"=>"JINADU, Saheed Segun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG122",
"name"=>"JUMAIH, Barakat Olamide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG123",
"name"=>"KADIR, Rukayat Titilope",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG124",
"name"=>"KAYODE, Oluwaferanmi Precious",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG125",
"name"=>"KOSOLU, Matthew Bolarinwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG126",
"name"=>"LAMBE, Mayokun John",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG127",
"name"=>"LAWAL, Afeez",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG128",
"name"=>"LAWAL, Mariam Oyindamola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG129",
"name"=>"LUKMAN, Abdul-basit Ayodele",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG130",
"name"=>"MAC-IVER, Biliqus Teniola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG131",
"name"=>"MALAKA, Lillian Fejiro",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG132",
"name"=>"MAMODU, Awwal Enemona",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG133",
"name"=>"MARYAM, Abdulwahab Oyindamola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG134",
"name"=>"MATTHEW, Micheal Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG135",
"name"=>"MAXWELL, James Olamide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG136",
"name"=>"MORAKINYO, Tolulope Olushola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG137",
"name"=>"MOSHOOD, Muftaudeen Alabi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG138",
"name"=>"MUONEME, Kingsley Okechukwu",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG139",
"name"=>"MURAINA, Akinola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG140",
"name"=>"MURITALA, Salmat Funmilayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG141",
"name"=>"MURITALA, Wulemot Oluwabukola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG142",
"name"=>"MUSA, Umar Oluwaseun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG143",
"name"=>"MUSBAUDEEN, Barakat Adenike",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG144",
"name"=>"MUSLIM, Robiu Adeola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG145",
"name"=>"MUSTAPHA, Halidu",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG146",
"name"=>"NASIR, Fawaz Fehintoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG147",
"name"=>"NTTIWUH, Roseline Uchechi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG148",
"name"=>"OBAFEMI, Aishat Abiodun",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG149",
"name"=>"ODEMUYIWA, Damilola Esther",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG150",
"name"=>"ODENIYI, Arinola Muizzah",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG151",
"name"=>"ODUWOLE, Faruq Adetola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG152",
"name"=>"OGUNGBEMI, Oluwamuyiwa Ayobami",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG153",
"name"=>"OGUNGBESAN, Okikiola Gold",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG154",
"name"=>"OGUNYEMI, Olamilekan Ayobami",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG155",
"name"=>"OGWU, Divine Ene",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG156",
"name"=>"OJOKUNLE, Johnson Olamilekan",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG157",
"name"=>"OKANLAWON, Mubarak",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG158",
"name"=>"OKUNADE, Azeezat Ayobami",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG159",
"name"=>"OLADAYO, Ahmod Olalekan",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG160",
"name"=>"OLADELE, Olamide Esther",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG161",
"name"=>"OLADIMEJI, Mogbolahan Ifeoluwapo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG162",
"name"=>"OLAJIDE, Festus Ayobami",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG163",
"name"=>"OLAJIDE, Martha Olamide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG164",
"name"=>"OLAJIDE, Olaniyi Peter",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG165",
"name"=>"OLAKRISTI, Precious Oluwadamilola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG166",
"name"=>"OLANIPEKUN, Peace Adeola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG167",
"name"=>"OLANREWAJU, Abidemi Murphy",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG168",
"name"=>"OLANREWAJU, Omotoyosi Balikis",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG169",
"name"=>"OLASOJI, Emmanuel Ayomide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG170",
"name"=>"OLASUNKANMI, Victoria Omolayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG171",
"name"=>"OLATEJU., Jamiu. Olaitan",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG172",
"name"=>"OLATUNDE, Taiwo Olawale",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG173",
"name"=>"OLAWEPO, Paul Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG174",
"name"=>"OLAWUYI, Omodunni Kareemah",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG175",
"name"=>"OLAYEMI, Jeremiah Olabisi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG176",
"name"=>"OLORODE, Patrick Temidayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG177",
"name"=>"OLOSHO, Samuel Oluwajoba",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG178",
"name"=>"OLOWOMEYE, Damilola Sunday",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG179",
"name"=>"OLOYEDE, Ayomide Patricia",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG180",
"name"=>"OLUKAN, Stephen Olabode",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG181",
"name"=>"OLUKOGA, Olayisola Taofeekat",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG182",
"name"=>"OLUKUNLE, Adeola Marufat",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG183",
"name"=>"OLUNREBI, Mosquorat Mojoyinola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG184",
"name"=>"OLUPONA, Adebola David",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG185",
"name"=>"OLUWAYOMI, Emmanuel Ayomide",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG186",
"name"=>"OMIYINKA, Janet Oluwasola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG187",
"name"=>"OMOLEYE, John Ayooluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG188",
"name"=>"OMORIARE, Israel Ese",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG189",
"name"=>"OMOTOSO, Fiyinfoluwa Caleb",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG190",
"name"=>"ORIMOLOYE, Favour Oluwatobi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG191",
"name"=>"OWA, Anuoluwapo Aduragbemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG192",
"name"=>"OWA, Temitope Ifeoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG193",
"name"=>"OWAN, Grace Bose",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG194",
"name"=>"OYEDELE, Oreoluwa Anjolaoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG195",
"name"=>"OYEDOKUN, Mary Tobiloba",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG196",
"name"=>"OYENIYI, Nathaniel Olaoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG197",
"name"=>"OYEWOLE, Babatunde Toyeeb",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG198",
"name"=>"OYEWOLE, David Adeola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG199",
"name"=>"OYEWOLE, Qonitat Oyeronke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG200",
"name"=>"PAMTOK, Similo Deborah",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG201",
"name"=>"PHILLIPS, Godspower Opeoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG202",
"name"=>"QUADRI, Taiwo Rahmon",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG203",
"name"=>"RAUF, Kasim Akorede",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG204",
"name"=>"SAHEED, Muibat Ajoke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG205",
"name"=>"SALAUDEEN, Islamiyat Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG206",
"name"=>"SALAUDEEN, Sarat Adekunbi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG207",
"name"=>"SAMBO, Khairah Adebanke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG208",
"name"=>"SHAZILIYU, Motunrayo Latifat",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG209",
"name"=>"SHOWEMIMO, Suliat Olajumoke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG210",
"name"=>"SHUAIB, Aishat Folashade",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG211",
"name"=>"SOLADOYE, Musteqeem Adeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG212",
"name"=>"SOLIU, Abubakar Sidiq",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG213",
"name"=>"SONEYE, Mojola Solomon",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG214",
"name"=>"SULAIMAN, Wasiu",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG215",
"name"=>"SULAIMON, Faisal Mayowa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG216",
"name"=>"SULEIMAN, Kudirat Opeyemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG217",
"name"=>"SUNDAY, Emmanuel Odunayo",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG218",
"name"=>"SUNDAY, Kayode Peter",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG219",
"name"=>"TAJUDEEN, Sofiyat Oluwakemi",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG220",
"name"=>"TIJANI, Abdulazeez Olalekan",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG221",
"name"=>"TOLUHI, Blessing Adetunmise",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG222",
"name"=>"TOR, Gift Rebekah",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG223",
"name"=>"UDOMA, Stephanie Morolake",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG224",
"name"=>"USMAN, Faidat Olayinka",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG225",
"name"=>"UTHMAN, Ahmad Aluko",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG226",
"name"=>"WASIU, Sherif Bolaji",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG227",
"name"=>"WILLIAMS, Promise Ezinne",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG228",
"name"=>"YAHYA, Moshood Abiola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG229",
"name"=>"YEKINI, Uthman Adeleke",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG230",
"name"=>"YISAU, Taiwo Augustine",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG231",
"name"=>"YUNUSA, Lawal Iyoma",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG232",
"name"=>"YUSUF, Abdulgafar Damilola",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG277",
"name"=>"OLUYEMI, Ayodeji Precious",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG278",
"name"=>"TAIWO, Halima Oreoluwa",
"level"=>"100",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG233",
"name"=>"ADEBISI, Damilola Ayobami",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG234",
"name"=>"ADEDAYO, Mojishola Christianah",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG235",
"name"=>"ADEJUMO, Fatimat Mosunmade",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG236",
"name"=>"ADELEYE, Olayemi Lateefat",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG237",
"name"=>"ADESANYA, Deborah Oluwaseun",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG238",
"name"=>"ADESHINA, Damilola Oluwabukola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG239",
"name"=>"AKERELE, Sulaimon Olamilekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG240",
"name"=>"AKINSANYA, Olanrewaju Dorcas",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG241",
"name"=>"AKINTOLA, Olamilekan Habeeb",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG242",
"name"=>"AKINWALE, Nafeesat Oluwayemisi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG243",
"name"=>"AKINWUNMI, Odunayo Adeola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG244",
"name"=>"ASOGBA, Ezekiel Daniel",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG245",
"name"=>"AYANKUNLE, Babajide Oluwaseun",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG246",
"name"=>"BAMIDELE, Olawale Kelvin",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG247",
"name"=>"BEKI, Hajarah Eniola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG248",
"name"=>"BELLO, Shakirat Abeke",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG249",
"name"=>"EWENIKE, Uju Vivian",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG250",
"name"=>"HANAFI, Muslimat Omotayo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG251",
"name"=>"HASSAN, Raimi Ozovehe",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG252",
"name"=>"JACOB, Faith Oyiza",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG253",
"name"=>"JOHNSON, Ekundayo Christian",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG254",
"name"=>"KALU, Uchechi Idika",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG255",
"name"=>"KAYODE, Babajide David",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG256",
"name"=>"KAZEEM, Babatunde Olamilekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG257",
"name"=>"MBA, Helen",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG258",
"name"=>"MURITALA, Mariam Aderinsola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG259",
"name"=>"NABABA, Abdulmatin Sa'adu",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG260",
"name"=>"NWADIUGUWU, Oluchi Blessing",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG261",
"name"=>"OKIKIOLU, Oluwaseyi Emmanuel",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG262",
"name"=>"OLADOSU, Hammed Ayobami",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG263",
"name"=>"OLATUNJI, Waliy Arisekola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG264",
"name"=>"OLOLADE, Joel Oluwakunmi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG265",
"name"=>"OLUWOLE, Esther Dideoluwa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG266",
"name"=>"ORJI, Prince Chinenye",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG267",
"name"=>"OYEBANJI, Aminat Oladunni",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG268",
"name"=>"OYETUNJI, Ayoola Israel",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG269",
"name"=>"SEIDU, Opeyemi Temitayo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG270",
"name"=>"SHOGBOLA, Hannah Adeola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG271",
"name"=>"SIMISOLA, Kolejo Theresa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG272",
"name"=>"UDOGU-OBIA, Echezona Kingsley",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG273",
"name"=>"UDOH, Mokutima Effiong",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG274",
"name"=>"UGOH, Goodness Ebuka",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG275",
"name"=>"YUSUF, Hawau Olaitan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66MG276",
"name"=>"YUSUF, Misturah Oyinkansola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG001",
"name"=>"ABAH, Wilson Ojoajogwu",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG002",
"name"=>"ABDALLAH, Nadra",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG003",
"name"=>"ABDULAKEEEM, Zulkiyat Oladolapo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG004",
"name"=>"ABDULHAKEEM, Abdulgafar",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG005",
"name"=>"ABDULRAHEEM, Abdulrahman Adekunle",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG007",
"name"=>"ABDULRAHIM, Abdulganiyu",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG008",
"name"=>"ABDULRAHMAN, Adam",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG009",
"name"=>"ABDULRASAQ, Qidamot Boluwatife",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG010",
"name"=>"ABDULRAUF, Alaba Temitope",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG011",
"name"=>"ABDULSALAM, Tajudeen Alaburo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG012",
"name"=>"ABDULWAHAB, Abdulwahab Adebayo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG013",
"name"=>"ABDULWAHAB, Halimat Bola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG014",
"name"=>"ABIKOYE, Taiwo Timothy",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG015",
"name"=>"ABUBAKAR, Hassanah Omayioza",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG018",
"name"=>"ADEBAYO, Ibrahim Bolaji",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG019",
"name"=>"ADEBAYO, Oluwasemilore Eunice",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG020",
"name"=>"ADEBAYO, Taiwo Taofikat",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG021",
"name"=>"ADEBOWALE, Christiana Omoware",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG022",
"name"=>"ADEDEJI, Abosede Oluwaponmile",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG023",
"name"=>"ADEDIRAN, Aduramilere Isaac",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG024",
"name"=>"ADEFILA, Oluwatimilehin Isaac",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG025",
"name"=>"ADEKUNLE, Adenike Alice",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG026",
"name"=>"ADELEKE, Deborah Iretioluwa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG027",
"name"=>"ADEMOKOYA, Tolulope David",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG028",
"name"=>"ADENIYI, Olubunmi Ayomide",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG031",
"name"=>"ADETUWO, Segun",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG032",
"name"=>"ADEWOYIN, Mariam Adeola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG033",
"name"=>"ADEYEMI, Kehinde Oluwakemi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG034",
"name"=>"ADEYEMO, Racheal Abosede",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG036",
"name"=>"AFOLABI, Abiodun Emmanuel",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG037",
"name"=>"AGWUNCHA, Peter Emenike",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG038",
"name"=>"AJAYI, Adeboye Alexander",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG039",
"name"=>"AJAYI, Timilehin John",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG040",
"name"=>"AJETUNMOBI, Rasheed Titilayo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG041",
"name"=>"AJEWOLE, Joshua Oluwafemi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG042",
"name"=>"AJIBOYE, Bisola Victoria",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG043",
"name"=>"AJISAFE, Adizat Adenike",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG044",
"name"=>"AJISAFE, Dorcas Damilola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG045",
"name"=>"AKANBI, Kuburat Arinola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG046",
"name"=>"AKANBI, Muritala Mohammed",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG047",
"name"=>"AKINBOHUN, Joseph Oluwaseyi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG048",
"name"=>"AKINNIRAN, Akinyemi Michael",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG049",
"name"=>"AKINRODEMI, Precious Naomi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG050",
"name"=>"AKINTOYE, Silifat Ronke",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG051",
"name"=>"AKINWALE, Aishat Odunyemi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG052",
"name"=>"ALABI, Remilekun Muritadoh",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG054",
"name"=>"ALAKA, Onaopemipo Adepeju",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG055",
"name"=>"ALIYU, Abdullateef Bolaji",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG056",
"name"=>"ALLI, Toheeb Ishola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG057",
"name"=>"ALLIU, Blessing Semilore",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG058",
"name"=>"ANJORIN, Morenike Simisola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG059",
"name"=>"ANJORIN, Tomilola Olalekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG060",
"name"=>"AREMU, Olayinka Rofiat",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG062",
"name"=>"AWANBOR, Patience Iziegbe",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG063",
"name"=>"AWONIYI, Joy Tobi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG064",
"name"=>"AYINLA, Omotola Blessing",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG066",
"name"=>"AYORINDE, Jamiu Toyin",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG067",
"name"=>"AYUBA, Jonathan Danjuma",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG068",
"name"=>"BABALOLA, Sherifat Omosalewa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG069",
"name"=>"BABATOLA, Oluwaseun David",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG070",
"name"=>"BABATUNDE, Ahmed",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG071",
"name"=>"BABATUNDE, Anuoluwapo Margret",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG072",
"name"=>"BADMUS, Ismail Sukanmi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG073",
"name"=>"BADRU, Blessing Abraham",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG074",
"name"=>"BALOGUN, Abdullahi Ayomide",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG075",
"name"=>"BALOGUN, Fatimah Ayomide",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG076",
"name"=>"BALOGUN, Mariam Bolade",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG077",
"name"=>"BALOGUN, Tobi Soliu",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG078",
"name"=>"BALOGUN, Tosin David",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG079",
"name"=>"BAMGBADE, Fuad Kadejo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG080",
"name"=>"BAMIDELE, Tosin Rhoda",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG082",
"name"=>"BUSARI, Opeyemi Zainab",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG084",
"name"=>"DANIEL, Oluwatosin Aina",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG085",
"name"=>"DAYAS, Hosea Mafeng",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG086",
"name"=>"DISU, Abdulwahab Babatunde",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG087",
"name"=>"DOSUNMU, Jesuleke Joseph",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG088",
"name"=>"EGBEWOLE, Aisha Damilola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG089",
"name"=>"ELEJA, Faridat Boluwatife",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG090",
"name"=>"ENAIBE, Victor Oghoroghene",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG091",
"name"=>"ENIOLORUNDA, Oluwatobi Samuel",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG092",
"name"=>"FERUKE-BELLO, Faridah Oluwatomi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG094",
"name"=>"FOLORUNSO, John Ayomide",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG095",
"name"=>"FOWOMOLA, Saheed Oladimeji",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG096",
"name"=>"GARBA, Habeeb Olamilekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG097",
"name"=>"IBRAHIM, Faruq Olamilekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG098",
"name"=>"IBRAHIM, Jemilat Asabe",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG099",
"name"=>"IBRAHIM, Lawal Ayodeji",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG100",
"name"=>"IDOWU, Christiana Fisayo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG101",
"name"=>"IMONI, Benjamin Ejiro",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG102",
"name"=>"ISAAC, Blessing Atinuke",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG104",
"name"=>"ISHAQ, Muhammad",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG105",
"name"=>"ISIAQ, Abdullateef Olamilekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG106",
"name"=>"JATTO, Taofeeqat Teju",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG108",
"name"=>"JIMOH, Abubakir",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG110",
"name"=>"KOTUN, Simiat Omolabake",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG111",
"name"=>"LAWAL, Christianah Oremeyi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG112",
"name"=>"LAWAL, Habeeb Olasunkanmi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG113",
"name"=>"LAWAL, Kudirat Oluwatosin",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG114",
"name"=>"LAWAL, Olalekan Abimbola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG115",
"name"=>"MADUKA, Samuel Chisom",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG116",
"name"=>"MANFUL, Adeti Isaac",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG118",
"name"=>"MOHAMMED, Rahmat A",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG119",
"name"=>"MUHAMMAD, Ahmad Adio",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG120",
"name"=>"MUHAMMED, Rofiat Bolanle",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG121",
"name"=>"MUHAMMED, Salamatu Eleojo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG122",
"name"=>"ODUBONOJO, Israel Akorede",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG123",
"name"=>"OGU, Violet Titilola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG124",
"name"=>"OJANUGA, Julianah Oluwagbohunmi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG125",
"name"=>"OJEIKERE, Victoria Omolua",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG126",
"name"=>"OJO, Israel Ayomide",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG127",
"name"=>"OKOYE, Ogochukwu Veronica",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG128",
"name"=>"OLABODE, Victory Omodolapo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG129",
"name"=>"OLADAPO, Abimbola Olateju",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG131",
"name"=>"OLADUNMADE, Mercy Oyindamola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG133",
"name"=>"OLANIYI, Azeezat Adenike",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG134",
"name"=>"OLASOJI, Iyanuoluwa Olamide",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG135",
"name"=>"OLATUNJI, Mariam Oyinkansola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG136",
"name"=>"OLORUNLEKE, Ayobami",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG137",
"name"=>"OLOWOGBOYE, Ayomide Matthew",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG138",
"name"=>"OLUDARE, Daniel Oluwaseyi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG139",
"name"=>"OLUGBILE, Jamiu Olawale",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG140",
"name"=>"OLUKOGA, Eniola Aminat",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG141",
"name"=>"OMISANYA, Abdulwahab Adetola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG142",
"name"=>"OMOLAJA, Remilekun Akimot",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG143",
"name"=>"ONUEGBU, Chimdinma Eniola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG144",
"name"=>"ORIOLA, Olawale Quwamdeen",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG145",
"name"=>"OWOLABI, Semiloore Christiana",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG146",
"name"=>"OYEBANJI, Samson Olabode",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG147",
"name"=>"OYEBIYI, Oreoluwa Omotoyosi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG148",
"name"=>"OYEBODE, Micheal Kayode",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG149",
"name"=>"OYELEYE, Gbemisola Elizabeth",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG150",
"name"=>"OYENIYI, Tolulpe Eunice",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG151",
"name"=>"OYEWOPO, Abdulmuiz Opeyemi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG152",
"name"=>"OYINLOYE, Tumininu Mary",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG153",
"name"=>"POPOOLA, Temilola Mayowa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG154",
"name"=>"RAJI, Bilal Muhammad",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG155",
"name"=>"RASHEED, Kudirat Apeke",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG156",
"name"=>"RAZAK, Wahab Ayinde",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG157",
"name"=>"RICHARD, Martins Terna",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG158",
"name"=>"SAAD, Saheed Olamilekan",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG159",
"name"=>"SANNI, Al-janat Toyin",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG160",
"name"=>"SANNI, Suliat Olasunkanmi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG161",
"name"=>"SEHINDE, Newman Temitope",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG162",
"name"=>"SHAKIRU, Taibat Temiloluwa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG163",
"name"=>"SHEHU, Toheer",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG165",
"name"=>"SHUAIB, Babatunde Adebayo",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG166",
"name"=>"SOKOYA, Oluwatobi Ayotomiwa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG167",
"name"=>"SORUNKE, Oreoluwa Aishat",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG168",
"name"=>"SUARAH, Saheed Oluwadamilare",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG169",
"name"=>"SULAIMON, Nosiru Olawale",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG170",
"name"=>"SUNDAY, Joy Oluwatoyin",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG171",
"name"=>"TANIMONURE, Racheal Ifeoluwa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG172",
"name"=>"USMAN, Yusuf Olabisi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG173",
"name"=>"WOJUADE, Remilekun Elizabeth",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG174",
"name"=>"YAHAYA, Aishat Omoshalewa",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG175",
"name"=>"YEKEEN, Adijat Abiodun",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG176",
"name"=>"YISAU, Abdulzarbur Ademola",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG177",
"name"=>"YUSUF, Barakat Omowunmi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG178",
"name"=>"YUSUF, Hawau Adedoyin",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG179",
"name"=>"YUSUF, Toheeb Alabi",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG180",
"name"=>"YUSUF, Zainab Titilope",
"level"=>"200",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG002",
"name"=>"ABDULGANIYU, Nimotallahi Feyisara",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG003",
"name"=>"ABDULKAREEM, Rofiat Ayoka",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG004",
"name"=>"ABDULLATEEF, Mariam Omobolanle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG005",
"name"=>"ABDULMUMEEN, Abdulwaheed Damilare",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG006",
"name"=>"ABDULSALAM, Azeezat Oluwakemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG007",
"name"=>"ABDULSALAM, Halimah Abidemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG181",
"name"=>"ABIKOYE, Aderonke Temitope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG008",
"name"=>"ABODUNRIN, Oluwatobi Olasunkanmi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG009",
"name"=>"ABOGUNDE, Clinton Wale",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG010",
"name"=>"ABOLARIN, Funmilayo Tosin",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG011",
"name"=>"ABOLARIN, Oladapo Peter",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG013",
"name"=>"ABUGU, Ekene ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG014",
"name"=>"ADAMS, Sambo ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG182",
"name"=>"ADEBANJO, Opemipo Osawese",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG015",
"name"=>"ADEBAYO, Hiqmat Omobolanle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG016",
"name"=>"ADEBISI, Aderonke Mary",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG017",
"name"=>"ADEBOYE, Iyanuoluwa Deborah",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG020",
"name"=>"ADEGBITE, Hammed Adeseun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG021",
"name"=>"ADEGBORE, Suliat Adetutu",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG022",
"name"=>"ADEGBOYE, Adetolani Christiana",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG023",
"name"=>"ADELEYE, Oluwasegun Adeboye",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG024",
"name"=>"ADENIYI, Muinat ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG026",
"name"=>"ADETUNJI, Mukhtar Adeyimka",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG184",
"name"=>"ADEWUMI, Covenant Folashade",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG185",
"name"=>"ADEYEMI, Islamiyat Opeyemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG186",
"name"=>"ADEYEMI, Tomisin Aminat",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG028",
"name"=>"ADIGUN, Abdulrasheed Abiodun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG029",
"name"=>"ADIGUN, Okikiola Yussuf",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG031",
"name"=>"AFOLORUNSO, Oreoluwa Paul",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG032",
"name"=>"AHMOD, Zainab Abisola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG033",
"name"=>"AIYELESO, Abdulbasit Opeyemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG187",
"name"=>"AJAGBE, Omolola Tolulope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG188",
"name"=>"AJEWOLE, Sekinat Ajekemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG034",
"name"=>"AJIBADE, Oluwafikemi ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG035",
"name"=>"AJIFERUKE, Uthman Ayodeji",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG037",
"name"=>"AKANBI, Halimat Mobolaji",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG038",
"name"=>"AKANDE, Aminat Olabola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG040",
"name"=>"AKINSANYA, Najat Moyegeola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG189",
"name"=>"AKOLO, Olumide Precious",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG041",
"name"=>"AMINU, Hajarat Bolanle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG043",
"name"=>"ANYAKU, Vivian Chinenye",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG044",
"name"=>"ARAGA, Hamzat Enesi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG045",
"name"=>"ARIMIYAU, Mariam Temitope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG046",
"name"=>"AROYEHUN, Ridwan Bolakale",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG047",
"name"=>"ASEKUNOWO, Aduragbemi Victor",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG048",
"name"=>"AWOEYO, Oluwasetemi Precillar",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG049",
"name"=>"AYINLA, Rukayat Gbemisola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG050",
"name"=>"AYODELE, Olubukola Titilayo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG052",
"name"=>"AZEEZ, Azeezat Oluwakemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG053",
"name"=>"BABALOLA, Oluwatobiloba ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG054",
"name"=>"BADMUS, Sherifat Temitope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG055",
"name"=>"BASHIR, Maryam Olayinka",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG056",
"name"=>"BELLO, Aishat Aramide",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG191",
"name"=>"BELLO, Oluwakemi Sarah",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG059",
"name"=>"BENSON, Excel Oyinloluwa",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG061",
"name"=>"DANJUMA, Ibrahim Amoo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG062",
"name"=>"DARASIN, Ajimoh Oluwafunmilola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG192",
"name"=>"DARE, Funsho Emmanuel",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG063",
"name"=>"DAUDA, Kashif Ayinde",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG193",
"name"=>"DEMOKUN, Opeyemi Olakunle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG194",
"name"=>"EYIOWUAWI, Oluwaseun Elizabeth",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG065",
"name"=>"FATAI, Dauda Olakunle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG066",
"name"=>"FATOKI, Rachael Toluwalope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG196",
"name"=>"GOMPIL, Nenwarng David",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG067",
"name"=>"HABIBULLAHI, Hammed ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG068",
"name"=>"HAMMED, Omolara Kikelomo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG069",
"name"=>"HAMZAT, Ibrahim Abiola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG070",
"name"=>"HASSAN, Ibrahim Pamilerin",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG071",
"name"=>"IBILOYE, John Adebayo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG072",
"name"=>"IBITOMISIN, Mayowa Elizabeth",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG074",
"name"=>"IBRAHIM, Aishat Olajumoke",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG075",
"name"=>"IBRAHIM, Lateef Olayinka",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG076",
"name"=>"IBRAHIM, Rukayat Olubodun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG078",
"name"=>"IBUOWO, Aminat Adebola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG197",
"name"=>"IREYOMI, Akinwale Emmanuel",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG079",
"name"=>"ISHOLA, Peter Akinola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG080",
"name"=>"ISIAKA, Mariam Oyindamola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG081",
"name"=>"ITIOLA, Adedolapo Esther",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG082",
"name"=>"IYOGUN, Mary Ozugua",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG084",
"name"=>"JIMOH, Muinat Titilope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG198",
"name"=>"JIMOH, Oluwatobiloba Sodiq",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG085",
"name"=>"JOLAYEMI, Deborah Morenikeji",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG086",
"name"=>"KAREEM, Ayomide Olayemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG199",
"name"=>"KELLMAN, Samuel Olabode",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG087",
"name"=>"KOLAWOLE, Kadri Oyewale",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG200",
"name"=>"KOLAWOLE, Oluwafunmbi  Titilola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG089",
"name"=>"LARRY-IYOHA, Angel Stephanie",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG090",
"name"=>"LAWAL, Seidat Shafau",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG091",
"name"=>"LAWAL, Toheeb Oluwaremilekun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG201",
"name"=>"LEBI, Funmilayo Grace",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG092",
"name"=>"MUHAMMED, Kehinde Yetunde",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG093",
"name"=>"MUHAMMED, Lukman Reyalekun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG202",
"name"=>"MURITALA, Qudus Ajibola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG094",
"name"=>"NAJEEM, Monsuru Oladipupo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG203",
"name"=>"OBADERO, Matthew Adebayo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG204",
"name"=>"OBALANLEGE, Ibrahim Adekunle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG205",
"name"=>"OGBUEFI, Ogonna Adaugo",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG206",
"name"=>"OGUNLADE, Ifeoluwa Victoria",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG096",
"name"=>"OGUNMEFUN, Daniel Boluwatife",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG207",
"name"=>"OGUNTAYO, Ademola Kunle",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG097",
"name"=>"OKUNUSI, Oluwatomisin Rebecca",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG098",
"name"=>"OKWUNDU, Obinna Kingsley",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG099",
"name"=>"OLADELE, Idris Abiola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG208",
"name"=>"OLADIPUPO, Abimbola Naimah",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG100",
"name"=>"OLAGBAYE, Ruth Temitope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG101",
"name"=>"OLAGOKE, Blessing Temitope",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG102",
"name"=>"OLAGUNJU, Victor Oluwasetitun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG103",
"name"=>"OLASUNKANMI, Olawale Sunday",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG106",
"name"=>"OLONADE, Toheeb Opeyemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG107",
"name"=>"OLORUNTOBA, Eyitayo Oluwaseun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG209",
"name"=>"OLOTU, Olumide Olasoji",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG108",
"name"=>"OLUSANYA, Victor Opeyemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG210",
"name"=>"OLUSEGUN, Yvonne Sharon",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG110",
"name"=>"OMOWARE, Modinat Opeyemi",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG111",
"name"=>"ONIFADE, Zainab Damilola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG112",
"name"=>"ONIGBINDE, O Ebunoluwa",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG211",
"name"=>"ORISAKWE, Precious Nmesoma",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG114",
"name"=>"OROKUNLE, Abisola Theresa",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG115",
"name"=>"OTESHILE, Zainab Ololade",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG117",
"name"=>"OYELOLA, Esther Oyebola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG118",
"name"=>"OYENIYI, Amos Olajesu",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG212",
"name"=>"OYENIYI, Modupe Juliana",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG120",
"name"=>"SALAKO, Oluwadamilola Ayodeji",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG121",
"name"=>"SALAMI, Olawale Lukmon",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG123",
"name"=>"SHITTU, Rofiat Oyindamola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG213",
"name"=>"SHODE, Omotayo Olatunji",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG124",
"name"=>"SHUAIB, Nofisat Ayomide",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG125",
"name"=>"SIJUADE, Taiwo ",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG126",
"name"=>"SODIQ, Aishat Onize",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG127",
"name"=>"SOTIMEHIN, Oluwatobiloba Emmanuel",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"17/66MG214",
"name"=>"TAVI, Armand  Moise",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG128",
"name"=>"TIAMIYU, Mariam Abisola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG130",
"name"=>"UBAH, Julius Charles",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG131",
"name"=>"WAHAB, Kabiru Abiodun",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG132",
"name"=>"WILLIAMS, Kedonojo Precious",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG133",
"name"=>"YUSSUFF, Zaynab Oluwabusola",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG134",
"name"=>"YUSUF, Ridwan Ayinde",
"level"=>"300",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG135",
"name"=>"ABDULKADIR, Abdulbasit Kehinde",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG001",
"name"=>"ABDULKADIR, Hamzat Adisa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG002",
"name"=>"ABDULLAHI, Aishat Damilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG003",
"name"=>"ABDULQUADIR, Ramat Bukola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG004",
"name"=>"ABDULWAHAB, Raheemat Anuoluwapo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG005",
"name"=>"ABDULYEKEEN, Shakirat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG006",
"name"=>"ABDURRAHMON, Zainab Eniola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG007",
"name"=>"ABEGUNDE, Godwin Temidayo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG008",
"name"=>"ABEGUNDE, Oreoluwa Temilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG162",
"name"=>"ABIODUN, Sulaiman Olasunkanmi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG009",
"name"=>"ABOLARIN, David Oluwabukunmi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG012",
"name"=>"ABUBAKAR, Rasheedat Oyiza",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG010",
"name"=>"ADEBAYO, Mariam Anuoluwapo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG011",
"name"=>"ADEBOLA, Moyosoluwa Oluwaseyi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG012",
"name"=>"ADEDEJI, Ronke Lateefat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG017",
"name"=>"ADEDOKUN, Victoria Oyinlade",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG019",
"name"=>"ADEGITE, Oluwatobi Sunday",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG014",
"name"=>"ADEKOYA, Qudus Tunde",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG015",
"name"=>"ADELEKE, Atinuke Anuoluwa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG016",
"name"=>"ADEMILOYE, Ayo Martins",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG017",
"name"=>"ADENIRAN, Deborah Temitope",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG018",
"name"=>"ADENIYI, Adetutu Iyanuoluwa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG019",
"name"=>"ADEOLA, Adedoyin Noah",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG136",
"name"=>"ADEOYE, Mercy Ayomipo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG020",
"name"=>"ADEOYE, Segun Rapheal",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG137",
"name"=>"ADERIBIGBE, Basirat Omotoke",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG022",
"name"=>"ADESINA-MUSA, Damilola Mariam",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG024",
"name"=>"ADEWOYE, Toba Stephen",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG025",
"name"=>"ADIGWU-BARDI, Jennifer Ifeoma",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG026",
"name"=>"ADIKA, Marvellous Oyindamola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG027",
"name"=>"AHMED, Aliyah Bidemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG028",
"name"=>"AIYEDERO, Aminat Oluwatemitope",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG035",
"name"=>"AJIBADE, Habeeb Adeniyi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG029",
"name"=>"AJIBADE, Shamsuldeen Damilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG031",
"name"=>"AKINTAN, Precious Babatunde",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG032",
"name"=>"AKINTUNDE, Bimpe Zubaidat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG033",
"name"=>"AKINTUNDE, Hussein Kehinde",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG140",
"name"=>"AKPORHUERE, Efe",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG040",
"name"=>"ALABI, Omotoyosi Monsurat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG154",
"name"=>"ALAO, Maria Oluwatobi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG035",
"name"=>"ALLI, Mariam Gbeminiyi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG036",
"name"=>"AMINU, Juliana Bukola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG141",
"name"=>"AREGBESOLA, Rokibat Labake",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG037",
"name"=>"AREMU, Nihinlolawa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG046",
"name"=>"ARIYO, Victoria Gbemisola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG038",
"name"=>"AROUNMOLASE, Tosin Abidemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG039",
"name"=>"ATANDA, Olamide Toheeb",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG142",
"name"=>"ATUSERI, Riliwan",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG143",
"name"=>"AWODELE, Grace Jumoke",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG144",
"name"=>"AYENI, Elizabeth Oluwatoyin",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG040",
"name"=>"AYENI, Mojisola Abiodun",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG042",
"name"=>"AYOOLA, Sarah Damilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG043",
"name"=>"AYUBA, Bashir Muhammad",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG052",
"name"=>"AYUBA, Hassanat Modupe",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG044",
"name"=>"BAKARE, Adenike Mistura",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG146",
"name"=>"BALOGUN, Islamiyat Biola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG147",
"name"=>"BALOGUN, Rasheedat Adenike",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG046",
"name"=>"BAMIKOLE, Opeyemi Ifeoluwa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG047",
"name"=>"BASHEER, Mastura Ajoke",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG048",
"name"=>"BASHORUN, Dhikrullah Abayomi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG049",
"name"=>"BELLO, Afeez Bamidele",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG050",
"name"=>"BELLO, Bolaji Abdulgafar",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG051",
"name"=>"BELLO, Oyindamola Omowunmi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG052",
"name"=>"BOLAJI, Islamiat Omolola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG053",
"name"=>"BUARI, Kazeem Ayodele",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG056",
"name"=>"DAIRO, Oyindamola Boluwatife",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG057",
"name"=>"EBOIGBE, Andrew Chukwuka",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG058",
"name"=>"ESABU, Faith Hapiness",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG066",
"name"=>"FALODUN, Idowu Gamaliel",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG059",
"name"=>"FASHANU, Adedoyin Olanrewaju",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG060",
"name"=>"FAWOLE, Opeyemi Sunday",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG061",
"name"=>"GBADAMOSI, Moshood Adeshina",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG063",
"name"=>"HAIDARA, Khadijat Ajibola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG148",
"name"=>"HAMMED, Abubakar Rufai",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG064",
"name"=>"HAMMED, Ayobami Jubril",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG149",
"name"=>"HAMMED, Basirat Opeyemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG150",
"name"=>"HAMZAT, Rokeeb Olaide",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG065",
"name"=>"IBRAHIM, Abubakar Sodiq",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG066",
"name"=>"IBRAHIM, Aminat Oluwakemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG070",
"name"=>"IBRAHIM, Mubarak Bolaji",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG067",
"name"=>"IBRAHIM, Olajumoke Batuli",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG151",
"name"=>"IBRAHIM, Sikiru Olamilekan",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG071",
"name"=>"IDOWU, Adetoun Mabel",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG068",
"name"=>"IDOWU, Clement Ayodele",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG156",
"name"=>"IDRIS, Halima Kofo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG069",
"name"=>"IDRIS, Kafayat Temitope",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG070",
"name"=>"IGEADEDAYO, Temitayo Tomike",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG071",
"name"=>"ILESANMI, Oluwatosin Christiana",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG152",
"name"=>"ILESANMI, Tope",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG153",
"name"=>"ISHOLA, Omodaramola Amidat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG072",
"name"=>"ISIAKA, Toheeb Olanrewaju",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG154",
"name"=>"ISMAIL, Abdulkabir Olorundare",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG073",
"name"=>"ISMAIL, Habeeb Olakunle",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG074",
"name"=>"IYANDA, Muhammedjamiu Olakunle",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG155",
"name"=>"JACOB, Emmanuel Moses",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG075",
"name"=>"JACOBS, Victor Opeyemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG076",
"name"=>"JIMOH, Abdul-basit Adedayo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG078",
"name"=>"JIMOH, Fisayo Rahmat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG156",
"name"=>"JOHNSON, Judith Funmilayo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG080",
"name"=>"JULIUS, Mary Aduragbemisola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG081",
"name"=>"JUNAID, Mardiyat Abiodun",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG083",
"name"=>"KAREEM, Adenike Fathia",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG084",
"name"=>"LAMIDI, Zainab Ajoke",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG085",
"name"=>"LAMINA, Mojeed Babatunde",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG086",
"name"=>"LASISI, Barakat Titilope",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG087",
"name"=>"LATEEF, Riliwan Olawale",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG088",
"name"=>"MAHMOUD, Rafiat Oluwagbemisola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG157",
"name"=>"MAHMUD, Shukurat Damilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG089",
"name"=>"MAJARO-OLA, Elizabeth Oluwaseyi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG090",
"name"=>"MALIK, Quam Olamilekan",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG090",
"name"=>"MORUFF, Rofiyat Oluwatoyin",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG092",
"name"=>"MOSHOOD, Mistura Itunu",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG093",
"name"=>"MUHAMMED, Aishat Omolola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG092",
"name"=>"MUHAMMED, Aminat Olaitan",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG094",
"name"=>"MUNIR, Ibrahim Damilare",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG095",
"name"=>"MUSTAPHA, Yusuf Dimeji",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG096",
"name"=>"NOLLA, Ayoola Islamiat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG097",
"name"=>"NWABA, Emmanuel Obuneme",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG098",
"name"=>"NWOGWUGWU, Victoria Chinanu",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG158",
"name"=>"OBANLA, Yinka David",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG099",
"name"=>"OBASAJU, Praise Oluwafemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG100",
"name"=>"OBINYERE, Chiwendu Euphemia",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG101",
"name"=>"ODEWOLE, Busayo Fatima",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG159",
"name"=>"OJO, Adebola Sarah",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG160",
"name"=>"OJUKANNAIYE, Miracle Tobi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG102",
"name"=>"OKAKA, Chukwuma Anthony",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG103",
"name"=>"OKEZIE, Prince Emmanuel",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG101",
"name"=>"OLADIPO, Rabiat Adenike",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG104",
"name"=>"OLANIYAN, Oladayo Olaoluwa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG161",
"name"=>"OLANIYAN, Olamilekan Toheeb",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG105",
"name"=>"OLANREWAJU, Olatunji Samuel",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG106",
"name"=>"OLASEHINDE, Dorcas Oluwatosin",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG107",
"name"=>"OLASEHINDE, Stephen Ayomide",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG108",
"name"=>"OLAYIWOLA, Adeyemi David",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG109",
"name"=>"OLOJA, Michael Oluwasegun",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG163",
"name"=>"OLORUNSHOLA, Mishael Damilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG111",
"name"=>"OLORUNSOLA, Ilerioluwa Oyindamola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG113",
"name"=>"OLUDIMU, John Oyedele",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG114",
"name"=>"OLUYEMI, Blessing Damilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG116",
"name"=>"ONOJA, Abah Paul",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG159",
"name"=>"ONYEKURU, Micheal Somtochukwu",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG113",
"name"=>"OSENI, Idris Opeyemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG118",
"name"=>"OSIEGBU, Onyinye Cecilia",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG165",
"name"=>"OWOADE, Adedolapo Monsurat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG119",
"name"=>"OWOEYE, Olawunmi Dorcas",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG120",
"name"=>"OWOJUYIGBE, Oluwakemi Abidemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG121",
"name"=>"OWOKONIRAN, Halimat Omobolanle",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG124",
"name"=>"OYEBADE, Folakemi Tomilola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG125",
"name"=>"OYEBODE, Dikrat Oyebisi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG126",
"name"=>"OYELEKAN, Zainab Temitope",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG127",
"name"=>"OYELEKE, Omoshalewa Sekinat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG128",
"name"=>"OYENUGA, Sidiqoh Omonike",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG129",
"name"=>"OYERINDE, Abdulrahman Akintoyese",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG130",
"name"=>"OYETOLA, Omotoke Ibukunoluwa",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG132",
"name"=>"QUADRI, Mariam Agbeke",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG167",
"name"=>"RAHEEM, Bashir",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG168",
"name"=>"RAHEEM, Selimot Konyisola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG169",
"name"=>"RAJI, Ibrahim Adeshola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG133",
"name"=>"RASAQ, Mohammed Kolade",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG134",
"name"=>"REMILEKUN, Abdulmuhsin",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG121",
"name"=>"RICHARD, Oluchi Ginika",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG170",
"name"=>"SALAU, Ayoola Tajudeen",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG135",
"name"=>"SALAU, Jonhson Alaba",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG125",
"name"=>"SALAWUDEEN, Azeez",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG171",
"name"=>"SALIMON, Tunde Afeez",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG136",
"name"=>"SAMUEL, Uduak Ette",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG126",
"name"=>"SANGOSANYA, Adewale Shafiu",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG137",
"name"=>"SHEU, Hussein Oladipo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG139",
"name"=>"SHITTU, Noimot Folake",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG172",
"name"=>"SHOMOYE, Eniola Mariam",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG140",
"name"=>"SHUAIB, Fatimat",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG129",
"name"=>"SOKEFUN,Oluwatobiloba Emmanuel",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG141",
"name"=>"SOLADOYE, Abdulkabir Adebayo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG142",
"name"=>"SOLIU, Ibraheem Dolapo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG173",
"name"=>"SOWEMIMO, Olalekan Bamidele",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG143",
"name"=>"SOWUNMI, Gbemisola Olasumbo",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG144",
"name"=>"SUBAIR, Sodiq Olamilekan",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG145",
"name"=>"SULEIMAN, Abdulganiyu Opeyemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG147",
"name"=>"TIJANI, Halimah Omowunmi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG174",
"name"=>"TUKURA, Sefinatu Muazu",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG148",
"name"=>"UBETA, Wilson Ohonsi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"16/66MG175",
"name"=>"VINCENT, Chukwuma John",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG149",
"name"=>"YAKUB, Barakat Abisola",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"15/66MG150",
"name"=>"YUSSUF, Babjide Maleek",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"14/66MG137",
"name"=>"ZUBAIR, Ahmed Opeyemi",
"level"=>"400",
"department"=>"MARKETING"
],
[
"matric_no"=>"18/66RM001",
"name"=>"ABAH, John",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM002",
"name"=>"ABDULMALIK, Hamidat Kashope",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM003",
"name"=>"ABDULRAHEEM, Aishat Ajoke",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM004",
"name"=>"ABDULWAHAB, Muis Olamilekan",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM005",
"name"=>"ABIFARIN, Oluwatobi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM006",
"name"=>"ADEBANJO, Babajide Joseph",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM007",
"name"=>"ADEBAYO, Henry Israel",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM008",
"name"=>"ADEKOLA, Olamide Comfort",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM009",
"name"=>"ADELEKE, Ibrahim Olayinka",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM010",
"name"=>"ADELEKE, Okanmiyinoluwa Eunice",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM011",
"name"=>"ADETIBA, Solomon Boluwatife",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM012",
"name"=>"ADEWUMI, Olatunbosun Daniel",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM013",
"name"=>"ADEYEMO, Hammed Adewale",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM014",
"name"=>"ADEYEMO, Khafilat Adeola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM015",
"name"=>"AFENI, Ibironke Mary",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM016",
"name"=>"AFOLABI, Ayomikun Rebecca",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM017",
"name"=>"AFOLAYAN, Damilola Oluwatosin",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM018",
"name"=>"AGBAJE, Gideon Eniola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM019",
"name"=>"AGBOLADE, Marvelous Blessing",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM020",
"name"=>"AGBOOLA, Timothy Gbenga",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM021",
"name"=>"AJALA, Oluwaferanmi Samuel",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM022",
"name"=>"AJANI, Amirat Olajumoke",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM023",
"name"=>"AJAYI, Afeez Abiodun",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM024",
"name"=>"AJEWOLE, Justina Temilola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM025",
"name"=>"AJIBADE, Al-ameen Oladeji",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM026",
"name"=>"AJIBOYE, Omotola Precious",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM027",
"name"=>"AKANNI, Abosede Ariyike",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM028",
"name"=>"AKINPELU, Raheemot Motunrayo",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM029",
"name"=>"AKINREMI, Oluwafemi Johnson",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM030",
"name"=>"AKINTADE, Temitope Deborah",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM031",
"name"=>"ALAO, Tunde Yusuf",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM032",
"name"=>"ALESHINLOYE, Raman Iyanda",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM033",
"name"=>"AMISU, Adedolapo Uthman",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM034",
"name"=>"AMOLE, Sabran Patience",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM035",
"name"=>"APEH, Elisha Omachonu",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM036",
"name"=>"ARATUMI, Toheeb Ajibola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM037",
"name"=>"AROSANYIN, Segun Oluwarotimi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM038",
"name"=>"ATOBA, Morufat Motunrayo",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM039",
"name"=>"ATOYEBI, Kehinde Abiola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM040",
"name"=>"AWEDA, Ayodeji Joseph",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM041",
"name"=>"AWOTOYE, Olatomiwa Abike",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM042",
"name"=>"AYINLA, Ahmad Ajibola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM043",
"name"=>"BABALOLA, Ibrahim Olasunkanmi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM044",
"name"=>"BAKARE, Adams Adekunle",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM045",
"name"=>"BALOGUN, Haleemat Oreoluwa",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM046",
"name"=>"DAKAIYE, Moses Damilola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM047",
"name"=>"DAN-TIJANI, Ozavize Rashidat",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM048",
"name"=>"DANIEL, Terkinbi Loho",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM049",
"name"=>"FOWOSERE, Abimbola Olayemi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM050",
"name"=>"FRANKLIN, Temiloluwa Thompson",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM051",
"name"=>"GARBA, Isa Abiodun",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM052",
"name"=>"IBRAHIM, Samiat Temitope",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM053",
"name"=>"IMRAN, Zainab Ifesowapo",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM054",
"name"=>"ISEGEN, Ohioma Emmanuel",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM055",
"name"=>"JONATHAN, Anjolaoluwa Bethel",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM056",
"name"=>"JOSEPH, Benita Chidera",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM057",
"name"=>"KASALI, Rilwan Bamidele",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM058",
"name"=>"KEHINDE, Omotunde Oluwatimilehin",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM059",
"name"=>"LAMBO, Mariam Eniola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM060",
"name"=>"LAMIDI, Mariam Anuoluwapo",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM061",
"name"=>"LASISI, Hasanatu Abake",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM062",
"name"=>"LAWAL, Mariam Folashade",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM063",
"name"=>"LAWAL, Saheed Olamilekan",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM064",
"name"=>"LAWAL, Yusuf Gbemisola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM065",
"name"=>"MUSBAUDEEN, Abdulqudus Olalekan",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM066",
"name"=>"ODUNTAN, Oluwatomisin Christiana",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM067",
"name"=>"OGUNLAJA, Mary Busayo",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM068",
"name"=>"OJO, Precious Abiola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM069",
"name"=>"OKOMAYIN, Abdulsamad Adeiza",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM070",
"name"=>"OKPETU, Benita Omoanono",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM071",
"name"=>"OLADELE, Oladeji Isaac",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM072",
"name"=>"OLADIMEJI, Elijah Timileyin",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM073",
"name"=>"OLAIYA, Joshua Oluwaseun",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM074",
"name"=>"OLATUNDE, Ruth Tolulope",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM075",
"name"=>"OLOMODA, Nofisat Omowumi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM076",
"name"=>"OLUWOLE, Godwin Oluwagbenga",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM077",
"name"=>"OMOBOLA, Damilola Faith",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM078",
"name"=>"OMOTAYO, Wuraola Desire",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM079",
"name"=>"OPADOKUN, Opeyemi Blessing",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM080",
"name"=>"OYEBAMIJI, Blessing Eniola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM081",
"name"=>"OYEBANJI, Omotolani Tamilore",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM082",
"name"=>"OYETORO, Mariam Oyenike",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM083",
"name"=>"OYEWALE, Jelilat Olayemi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM084",
"name"=>"PELEWURA, Kiibati Damilola",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM085",
"name"=>"QOZEEM, Nofisat Olajumoke",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM086",
"name"=>"QUADRY, Habeebat Abosede",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM087",
"name"=>"SAHEED, Fathia Opeyemi",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM088",
"name"=>"SALAMI, Ayodeji Yusuf",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM089",
"name"=>"SALIU, Paul Kehinde",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM090",
"name"=>"SHOBOWALE, Khadijat Aderenle",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM091",
"name"=>"SIKIRU, Ibrahim Adedoyin",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM092",
"name"=>"SOLOMON, Michael Ayomikun",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM093",
"name"=>"SUARAU, Aminat Adenike",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM094",
"name"=>"SULAIMON, Kolawole Selim",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM095",
"name"=>"TAOFEEK, Toheeb Temitope",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM096",
"name"=>"TIJANI, Khadijah Ajoke",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM097",
"name"=>"TIJANI, Sadiq Ayodeji",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM098",
"name"=>"TIMILEYIN, Akanbi Blessing",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM099",
"name"=>"UWAKWE, Irene Chioma",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM100",
"name"=>"YUSUPH, Misturah",
"level"=>"100",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM101",
"name"=>"AGUDA, Alimat Kehinde",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM102",
"name"=>"ALESHINLOYE, Jamiu Omolayo",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM103",
"name"=>"AMAO, Deborah",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM104",
"name"=>"AREMU, Oluwatobi Dennis",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM105",
"name"=>"CHINYERE, Chukwuebuka Samuel",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM106",
"name"=>"CLEMENT, Dorcas",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM107",
"name"=>"EZEORAH, Charles Chiderah",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM108",
"name"=>"KADIKU, Victoria Ololade",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM109",
"name"=>"LIASU, Kaosara Ayomide",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM110",
"name"=>"MAHA, Faith Ojochenemi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM111",
"name"=>"MUHAMMED, Nuhu Abdulwasiu",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM112",
"name"=>"ODESANMI, Anuoluwapo Oluwapelumi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM113",
"name"=>"OGUNYEMI, Kikelomo Oluwakemi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM114",
"name"=>"OLAYIWOLA, Peter Oluwaferanmi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM115",
"name"=>"OMOBORIOWO, Oluwabukunmi Michael",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"18/66RM116",
"name"=>"OYENIYI, Adeyemi Adekunle",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM001",
"name"=>"ABBAS, Rodiat Oluwafadekemi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM002",
"name"=>"ABDULLAZEEZ, Abdullahi Femi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM003",
"name"=>"ABIODUN, Barakat Adeola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM004",
"name"=>"ADEAGA, Ibukunoluwa Isaac",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM005",
"name"=>"ADEBAYO, Adam Ololade",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM006",
"name"=>"ADEDAYO, Muiz Olalekan",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM007",
"name"=>"ADEMOLA, Mojeed Olanrewaju",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM008",
"name"=>"ADENUGA, Oluwatobi Samson",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM009",
"name"=>"ADEYI, Soffiullahi Olamide",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM010",
"name"=>"AFOLABI, Bashirat Mosopeoluwa",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM011",
"name"=>"AFOLABI, Rosemary Ayotomiwa",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM012",
"name"=>"AGBOOLA, Oluwabusayo Esther",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM013",
"name"=>"AGBOOLA, Sodiq Abiodun",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM014",
"name"=>"AHMED, Kafilah Adebimpe",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM016",
"name"=>"AINA, Esther Boluwatife",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM017",
"name"=>"AKHIDENOR, Prestige Ehimen",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM018",
"name"=>"AKINBAMIRE, Abigail Adedamola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM019",
"name"=>"AKINBOYO, Tolulope Ezekiel",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM020",
"name"=>"AKINYEMI, Rhodiyat Oluwadamilola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM021",
"name"=>"ALABI, Lateefat Olamide",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM022",
"name"=>"ALABI, Sodiq Olamilekan",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM023",
"name"=>"ALAYEMOLA, Olayinka Joyce",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM025",
"name"=>"AMAEFUNA, Amaka Euphema",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM026",
"name"=>"AMUDA, Abdulkareem Opeyemi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM027",
"name"=>"AREMU, Deborah Arike",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM028",
"name"=>"AYANDA, Deborah Olamide",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM030",
"name"=>"AZEEZ, Oyindamola Azeezah",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM031",
"name"=>"BABATUNDE, Abdulgafar ",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM032",
"name"=>"BALOGUN, Bolakale Kazeem",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM033",
"name"=>"BAMIDELE, Oluwagbemisola Abosede",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM034",
"name"=>"BELLO, Ibrahim Tijani",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM035",
"name"=>"BETULE, Mary Ileanwa",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM036",
"name"=>"BOLARINWA, Fiyinfoluwa Todimu",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM037",
"name"=>"EMMANUEL-ONYECHE, Edward Mayowa",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM038",
"name"=>"FACHE, Simeon Omeiza",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM039",
"name"=>"FAGBIYE, Oluwadamilola Elizabeth",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM040",
"name"=>"FANIRAN, Oluwatobi Peter",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM041",
"name"=>"FASINA, Ooreoluwa Omolayo",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM042",
"name"=>"GANIYU, Fawaz Abidemi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM043",
"name"=>"GBADAMOSI, Rofiat Adewunmi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM044",
"name"=>"HASSAN, Rofiat Abimbola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM045",
"name"=>"ISAH, Aminat ",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM046",
"name"=>"JOHNSON, Olajuwon Ayobami",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM047",
"name"=>"KAREEM, Soliat Oluwapelumi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM048",
"name"=>"KURANGA, Habeeb Abiodun",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM049",
"name"=>"MAXWELL, Sophia Enemona",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM050",
"name"=>"MOHAMMED, Binta Oziama",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM051",
"name"=>"MOHAMMED, Fatimo Faith",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM052",
"name"=>"MOHAMMED, Mubarak ",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM053",
"name"=>"MUHAMMED, Ismail Opeyemi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM054",
"name"=>"MUSTAPHA, Abdulkabir Atanda",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM055",
"name"=>"OBADEYI, Oluwatobiloba Damilola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM056",
"name"=>"ODUKOYA, Olamide Emmanuel",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM057",
"name"=>"OGUNGBUARO, Joshua Gbenga",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM058",
"name"=>"OGUNWOLU, Ayokunle Sheriph",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM059",
"name"=>"OJO, Temitope Mausede",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM060",
"name"=>"OKANLAWON, Sofiat ",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM061",
"name"=>"OKON, Uyioata Udoh",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM062",
"name"=>"OLAIYA, Opeyemi Bilikis",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM063",
"name"=>"OLASUNKANMI, Anuoluwa Christian",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM064",
"name"=>"OLAYAKI, Mariam Ayinke",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM065",
"name"=>"OLAYINKA, Salam Olasunkanmi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM066",
"name"=>"OLOKO, Saheed Olamilekan",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM067",
"name"=>"OLORUNOJE, Rophiat Temitope",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM068",
"name"=>"OLORUNTOBA, Oluwajuwon Enoch",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM069",
"name"=>"OMOTOSHO, Azeez Olalekan",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM070",
"name"=>"ORILEOLA, Mayowa Ishola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM071",
"name"=>"OROBIYI, Damilare Emmanuel",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM072",
"name"=>"OSENI, Sandra Aderonke",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM073",
"name"=>"OSHIKOYA, Olanrewau Sodiq",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM074",
"name"=>"OYEKUNLE, Christiana Busayo",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM075",
"name"=>"OYETUNJI, Odunayo Oyindamola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM076",
"name"=>"POPOOLA, Aishat Iyabo",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM077",
"name"=>"RUFAI, Simbiat Eniola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM078",
"name"=>"SAHEED, Roheemat Abimbola",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM079",
"name"=>"SAMUEL, Matthew Olasumbo",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM080",
"name"=>"SHEHU, Lukmon Olawale",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM081",
"name"=>"SOETAN, Motunrayo Suliyat",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM082",
"name"=>"SOFOLUWE, Olasubomi Oluwaseyi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM083",
"name"=>"SOLIU, Basheerat Ayomide",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM084",
"name"=>"SULAIMAN, Abdullahi Tope",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM085",
"name"=>"THOMPSON, Adeyemi Omotoyosi",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM086",
"name"=>"UMAR, Habeeb Asipita",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM087",
"name"=>"USHIEKPAN, Benedict Ushie",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM088",
"name"=>"YUSUF, Aishat Iyabo",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM089",
"name"=>"YUSUF, Mistura Morenikeji",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM090",
"name"=>"YUSUF, Shakiru Adedeji",
"level"=>"200",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM003",
"name"=>"ABDULSALAM, Mariam Olabisi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM004",
"name"=>"ABIALA, Oluwabusayo Damilola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM005",
"name"=>"ABOLARIN, Ayobami Jacob",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM006",
"name"=>"ADELAKUN, Iranlowo Joseph",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM007",
"name"=>"ADENIYI, Adedeji Peter",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM091",
"name"=>"ADESIBIKAN, Adetunji Oluwatoyosi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM008",
"name"=>"ADEYERI, Omowumi Azeezat",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM009",
"name"=>"ADEYEYE, Ademola Andrew",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM092",
"name"=>"ADEYEYE, Seyi Joseph",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM010",
"name"=>"AFOLAYAN, Adelodun Oluwanishola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM093",
"name"=>"AFOLAYAN, Lateefat Opeyemi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM011",
"name"=>"AGAJA, Oluwaseyi Florence",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM012",
"name"=>"AGAJUNOH, Agness Maugbe",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM013",
"name"=>"AGBESHIE, Frank Moses",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM015",
"name"=>"AJOLOJA, Mustapha Olamilekan",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM016",
"name"=>"AKANNI, Zainab Omobolanle",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM094",
"name"=>"AKINPELU, Oladimeji Fawaz",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM017",
"name"=>"AKINTARO, Gbolahan Jimmy",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM018",
"name"=>"ALAKALOKO, Olalekan Adeniyi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM095",
"name"=>"ALAO, Zainab Abisola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM096",
"name"=>"ANADU, Chizoba Fanny",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM097",
"name"=>"AROYEWUN, Aishat Olayinka",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM098",
"name"=>"ATIM, Angelica Emmanuella",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM019",
"name"=>"AWOSANYA, Racheal Bukola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM021",
"name"=>"AZEEZ, Islamiat Olajumoke",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM023",
"name"=>"BALOGUN, Mubarak Olawale",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM024",
"name"=>"BELLO, Halimat Oiza",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM099",
"name"=>"BELLO, Nafisat Ahmed",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM025",
"name"=>"BUSARI, Hammed Adeshina",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM027",
"name"=>"CLETUS, Joy Ebi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM028",
"name"=>"EJIOYE, Omolara Deborah",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM029",
"name"=>"EJIWUNMI, Raheemat Oluwatosin",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM030",
"name"=>"ELEGBE, Gregory Temidayo",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM100",
"name"=>"GANYE, Matthew Damilola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM101",
"name"=>"GBADEGESIN, Funmilayo ",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM033",
"name"=>"JAMES, Tayo Temi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM035",
"name"=>"JOEL, Cecilia Abimbola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM037",
"name"=>"LALEYE, Oluwaseun Serah",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM038",
"name"=>"LAWAL, Sherif Taiwo",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM039",
"name"=>"MARUF, Umar Olawale",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM102",
"name"=>"MOSHOOD, Zainab Opeyemi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM040",
"name"=>"MUHAMMED, Ahmed Abiodun",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM041",
"name"=>"ODUNAYO, Abdulsalam Adeyinka",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM042",
"name"=>"OGINNI, Oluwabukola Helen",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM103",
"name"=>"OJO, Remilekun Favour",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM104",
"name"=>"OKANLAWON, Oluwaseun David",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM043",
"name"=>"OKUPEVI, John Nugboyon",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM044",
"name"=>"OLABIYI, Deborah Oluwaferanmi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM105",
"name"=>"OLADELE, Azeezat Olabisi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM106",
"name"=>"OLALEYE, Peter Ibukun",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM045",
"name"=>"OLANREWAJU, Elizabeth Bukolami",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM047",
"name"=>"OLAOYE, Omolola Rebecca",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM048",
"name"=>"OLAYINKA, Zainab Adenike",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM049",
"name"=>"OLUPONA, Emmanuel Oluwatosin",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM050",
"name"=>"OMORO, Ebedaere Oyintarela",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM052",
"name"=>"OYESHIDE, Mary Mayowa",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM053",
"name"=>"OYETUNJI, Stephen Abiodun",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM055",
"name"=>"PETER, Charles Tersoo",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM056",
"name"=>"QUADRI, Zainab Olusola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM057",
"name"=>"RAJI, Awawu Gbemisola",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM060",
"name"=>"SAMUEL, Nneoma Precious",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM061",
"name"=>"SAMUEL, Peace Dupe",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM062",
"name"=>"SOBOGUN, Idowu Omolade",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM063",
"name"=>"UMAR, Aliyu Usman",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"17/66RM107",
"name"=>"USMAN, Hakeem Itopa",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM065",
"name"=>"YUSUF, Khadeejah Omotayo",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM066",
"name"=>"ZAKARIYAH, Rasheedat Oluwakemi",
"level"=>"300",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM001",
"name"=>"ABASS, Ayinla Salaudeen",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM067",
"name"=>"ABDULKADIR, Sadiat Oluwatoyin",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM068",
"name"=>"ABDULLAHI, Samsudeen Oluwasegun",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM069",
"name"=>"ABDULQUADRI, Balqees Opeyemi",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM002",
"name"=>"ABIODUN, Bolaji Lekan",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM070",
"name"=>"ABIOYE, Ayomide Cecilia",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM003",
"name"=>"ADEBAYO, Idris Ismail",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM004",
"name"=>"ADEGBITE, Adeola Aishat",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM005",
"name"=>"ADEGOKE, Oluwaseyi Olaitan",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM005",
"name"=>"ADELAKUN, Yusuf Adeyemi",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM006",
"name"=>"ADEMUWAGUN, Eniola Christopher",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM008",
"name"=>"ADU, Rhoda Temitayo",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM011",
"name"=>"AFOLABI, Racheal Mofoluwake",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM009",
"name"=>"AGBO, Evelyn Onyotse",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM012",
"name"=>"AGBOLADE, Sultan Bolaji",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM010",
"name"=>"AJIBOYE, Morufat Bola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM011",
"name"=>"AKANBI, Zainab Abiola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM071",
"name"=>"AKERELE, Olamide Oluwaseun",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM019",
"name"=>"AKINNIYI, Bikose Juwon",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM072",
"name"=>"ALAO, Michael Olashile",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM013",
"name"=>"AMUJO, Elizabeth Taye",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM073",
"name"=>"AMUSA, Yusuf Olatunji",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM015",
"name"=>"AYENI, Samuel Ayomide",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM033",
"name"=>"AYINLA, Latifa Omotola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM016",
"name"=>"BALOGUN, Mazeedah Olayinka",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM074",
"name"=>"BAMGBOPA, Aishah Temitope",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM018",
"name"=>"BELLO, Hammed Temitope",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM075",
"name"=>"BEYIOKU, Olayemi Imoleayo",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM019",
"name"=>"DUROTOYE, Christianus Seye",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM021",
"name"=>"EKWUEME, Chukwuebuka Godswill",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM022",
"name"=>"ERIAKHA, Obehi Gloria",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM076",
"name"=>"EZENWAKA, Louis Uzochukwu",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM050",
"name"=>"FUNMILAYO, Olumide Davis",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM051",
"name"=>"GIWA, Olamilekan Wakil",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM054",
"name"=>"IBRAHIM, Ibrahim Opemipo",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM058",
"name"=>"JAMIU, Sekeenat Arinola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM024",
"name"=>"JIMOH, Bilikis Omowunmi",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM025",
"name"=>"LA-KADRI, Hameedat Bukola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM026",
"name"=>"LAARO, Yetunde Mariam",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM027",
"name"=>"LAWAL, Aishat Ayomide",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM028",
"name"=>"LAWAL, Azeezat Omobolanle",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM029",
"name"=>"LUKMAN, Kafayat Iyabo",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM066",
"name"=>"MATTHEW, Richard Baggi",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM077",
"name"=>"MBA, Mercy Irukwu",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM030",
"name"=>"ODELEYE, Damilare Isreal",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM079",
"name"=>"ODUNLAMI, Amidat Damilola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM031",
"name"=>"OGUNGBENRO, Oluwatosin Bolu",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM080",
"name"=>"OGUNLUSI, Adenike Damilola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM032",
"name"=>"OJO, Dorcas Tosin",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM081",
"name"=>"OJO, Reuben Oluwatayo",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM034",
"name"=>"OKONI, Anuoluwapo Helen",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM035",
"name"=>"OKONMAH, Fidelia Chinaza",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM082",
"name"=>"OKWE, Cyprian Abang",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM036",
"name"=>"OLUPITAN, Oluwanifemi Ayomide",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM037",
"name"=>"OMONIYI, Muhammed Abiodun",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM038",
"name"=>"OSEJI, Enezike Martyl",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM083",
"name"=>"OWOLABI, Oluwafunmilayo Agnes",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM040",
"name"=>"RAHEEM, Raheemot Adenike",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM084",
"name"=>"RAHEEM, Victoria Fisayo",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM085",
"name"=>"RAJI, Muyideen Olawale",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"15/66RM041",
"name"=>"SANUSI, Abideen Olayinka",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM111",
"name"=>"SAVAGE, Aminat Ayinke",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"16/66RM086",
"name"=>"SHEU, Taiwo Daramola",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM115",
"name"=>"SOYOMBO, Opeyemi Deborah",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
[
"matric_no"=>"14/66RM117",
"name"=>"SULYMAN, Faidat Temitope",
"level"=>"400",
"department"=>"PUBLIC ADMINISTRATION"
],
];
