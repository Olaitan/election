<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('load:voters', function(){
    $students = require(storage_path('students.php'));
    $i = 1;
    foreach ($students as $student){
        //load them into database
        echo $i.'\n';
        $i++;
        $u = new \App\User();
        $u->name= $student['name'];
        $u->matric_no = $student['matric_no'];
        $u->generated_password = rand(1001, 9999);
        $u->level = $student['level'];
        $u->department = $student['department'];
        $u->save();
    }
    $u = new \App\User();
    $u->name= 'Admin';
    $u->matric_no = 'admin';
    $u->generated_password = '1000';
    $u->level ='100';
    $u->department = 'ACCOUNTING';
    $u->admin = true;
    $u->save();

});
