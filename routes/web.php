<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('/', 'PageController@home')->name('home');

    Route::get('login', function(){
        return view('voters.login');
    })->name('login');

    Route::post('vote', 'PageController@vote');

    Route::post('login', 'AuthController@login');


    Route::get('logout', 'Auth\LoginController@logout')->name('logout');


Route::prefix('admonish' )->group(function (){

    Route::get('/','AdminController@home')->name('admin.home');
    Route::get('result', 'AdminController@result')->name('result');
    Route::get('result/{id}', 'AdminController@resultSingle')->name('result');
//    Route::get('login', function(){
//        return view('admin.login');
//    })->name('admin.login');

    Route::post('addpost', 'AdminController@addPost');

    Route::post('addcandidate', 'AdminController@addCandidate');

//    Route::delete('delete/{id}', 'AdminController@candidateDelete');


});
